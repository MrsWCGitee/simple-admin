package simpleadmin.elfinder.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simpleadmin.elfinder.service.ElfinderStorage;

public class AbortCommand extends AbstractCommand {
    @Override
    public void execute(ElfinderStorage elfinderStorage, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        response.setContentType("text/html;charset=utf-8");
        response.setCharacterEncoding("UTF-8");
        response.sendError(204, "No contents！");
    }
}
