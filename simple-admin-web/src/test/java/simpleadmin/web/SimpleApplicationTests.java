package simpleadmin.web;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * 测试
 * 
 * @author wangcheng
 * @since 2022-06-03
 */
@SpringBootTest
class SimpleApplicationTests {

    /**
     * 测试
     */
    @Test
    public void test() {

    }
}
