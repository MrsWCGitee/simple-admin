﻿(function ($) {
    //默认参数
    var defaluts = {
        submitUrl: "",
    };

    //jquery扩展方法

    //【方式1】
    //调用示例  $.setApDiv();
    $.extend({
        /**
         * 获取项目根目录
         * @param {any} hasProjectName 是否有项目名
         */
        getRootPath: function (hasProjectName = false) {
            var curWwwPath = window.document.location.href;
            var pathName = window.document.location.pathname;
            var pos = curWwwPath.indexOf(pathName);
            var localhostPath = curWwwPath.substring(0, pos);
            if (hasProjectName) {
                var projectName = pathName.substring(
                    0,
                    pathName.substr(1).indexOf("/") + 1
                );
                return localhostPath + projectName;
            }
            return localhostPath;
        },

        /**
         * 日期时间的格式化
         * @param {any} options{date:时间对象,format:"yyyy-MM-dd hh:mm"}
         * @return {string} 返回说明
         * @author wangcheng
         * @version 1.0.0
         */
        dateFormat: function (options) {
            var date = options.date;
            var format = options.format;
            return dateFormatInner(date, format);
        },

        /**
         * 日期时间戳的格式化
         * @param {any} options{dateTimeStamp:"/Date(1614908124090)/",format:"yyyy-MM-dd hh:mm"}
         * @return {string} 返回说明
         * @author wangcheng
         * @version 1.0.0
         */
        dateTimeStampFormatfunction(options) {
            var date = eval(
                "new " +
                options.dateTimeStamp.substr(1, options.dateTimeStamp.length - 2)
            );
            var format = options.format;
            return dateFormatInner(date, format);
        },

        /**
         * 获取当前 url 的某个url参数值
         * @param {any} name
         */
        getUrlParam(name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
            var r = window.location.search.substr(1).match(reg);
            if (r != null) return unescape(r[2]);
            return null;
        },
    });

    //【方式1】
    //调用示例  $("#apDiv").setApDiv();
    $.fn.extend({
        /**
         * 清空元素内部所有表单元素值
         * @param {any} options
         */
        clearFormElementValue: function (options) {
            var ele = $(this);
            ele.find("input").each(function () {
                if (
                    $(this).prop("type") == "checkbox" ||
                    $(this).prop("type") == "radio"
                ) {
                    $(this).prop("checked", false);
                } else {
                    $(this).val("");
                }
            });
            ele.find("select").each(function () {
                $(this).val("");
                if ($(this).hasClass("select2")) {
                    $(this).val([""]).trigger("change");
                }
            });
            ele.find("textarea").each(function () {
                $(this).val("");
            });
        },
        /**
         * 禁用元素内部的所有表单元素*/
        disableAllFormElements: function () {
            var ele = $(this);
            ele.find("input").each(function () {
                $(this).attr("disabled", true);
            });
            ele.find("select").each(function () {
                $(this).attr("disabled", true);
            });
            ele.find("textarea").each(function () {
                $(this).attr("disabled", true);
            });
        },
    });

    /**
     * 日期格式化方法（jquery扩展内部使用）
     * @param {date} date 日期时间
     * @param {string} format 格式化格式
     */
    function dateFormatInner(date, format) {
        var o = {
            "M+": date.getMonth() + 1,
            //月份
            "d+": date.getDate(),
            //日
            "h+": date.getHours(),
            //小时
            "m+": date.getMinutes(),
            //分
            "s+": date.getSeconds(),
            //秒
            "q+": Math.floor((date.getMonth() + 3) / 3),
            //季度
            S: date.getMilliseconds(), //毫秒
        };
        if (/(y+)/.test(format)) {
            format = format.replace(
                RegExp.$1,
                (date.getFullYear() + "").substr(4 - RegExp.$1.length)
            );
        }
        for (var k in o) {
            if (new RegExp("(" + k + ")").test(format)) {
                format = format.replace(
                    RegExp.$1,
                    RegExp.$1.length == 1
                        ? o[k]
                        : ("00" + o[k]).substr(("" + o[k]).length)
                );
            }
        }
        return format;
    }
})(window.jQuery);
