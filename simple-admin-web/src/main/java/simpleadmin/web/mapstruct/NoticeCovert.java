package simpleadmin.web.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import simpleadmin.web.entity.Notice;
import simpleadmin.web.entity.dto.notice.AddNoticeInput;

/**
 * 编写转换接口
 */
@Mapper(componentModel = "spring")
public interface NoticeCovert {
    NoticeCovert MAPPER = Mappers.getMapper(NoticeCovert.class);

    /**
     * 字段数量类型相同,只是字段多，可以直接转换
     *
     * @param source
     * @return
     */
    @Mapping(target = "publicUserId", ignore = true) // 忽略id，不进行映射
    @Mapping(target = "publicUserName", ignore = true) // 忽略id，不进行映射
    @Mapping(target = "publicOrgId", ignore = true) // 忽略id，不进行映射
    @Mapping(target = "publicOrgName", ignore = true) // 忽略id，不进行映射
    @Mapping(target = "publicTime", ignore = true) // 忽略id，不进行映射
    @Mapping(target = "cancelTime", ignore = true) // 忽略id，不进行映射
    @Mapping(target = "createdTime", ignore = true) // 忽略id，不进行映射
    @Mapping(target = "updatedTime", ignore = true) // 忽略id，不进行映射
    @Mapping(target = "createdUserId", ignore = true) // 忽略id，不进行映射
    @Mapping(target = "createdUserName", ignore = true) // 忽略id，不进行映射
    @Mapping(target = "updatedUserId", ignore = true) // 忽略id，不进行映射
    @Mapping(target = "updatedUserName", ignore = true) // 忽略id，不进行映射
    @Mapping(target = "isDeleted", ignore = true)
    // 忽略id，不进行映射
    Notice toConvertVO(AddNoticeInput source);

}
