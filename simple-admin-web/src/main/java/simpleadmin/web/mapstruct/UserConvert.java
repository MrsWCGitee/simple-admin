package simpleadmin.web.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import simpleadmin.web.entity.User;
import simpleadmin.web.entity.UserOrg;
import simpleadmin.web.entity.dto.auth.LoginOutput;
import simpleadmin.web.entity.dto.user.AddOrEditUserInput;

/**
 * 编写转换接口
 */
@Mapper(componentModel = "spring")
public interface UserConvert {
    UserConvert MAPPER = Mappers.getMapper(UserConvert.class);

    /**
     * 字段数量类型相同,只是字段多，可以直接转换
     *
     * @param source
     * @return
     */
    @Mapping(target = "lastLoginIp", ignore = true) // 忽略id，不进行映射
    @Mapping(target = "lastLoginTime", ignore = true)
    @Mapping(target = "lastLoginAddress", ignore = true)
    @Mapping(target = "lastLoginBrowser", ignore = true)
    @Mapping(target = "lastLoginOs", ignore = true)
    @Mapping(target = "roles", ignore = true)
    @Mapping(target = "menus", ignore = true)
    LoginOutput toConvertVO(User source);

    @Mapping(target = "lastLoginIp", ignore = true) // 忽略id，不进行映射
    @Mapping(target = "lastLoginTime", ignore = true)
    @Mapping(target = "createdTime", ignore = true)
    @Mapping(target = "updatedTime", ignore = true)
    @Mapping(target = "createdUserId", ignore = true)
    @Mapping(target = "createdUserName", ignore = true)
    @Mapping(target = "updatedUserId", ignore = true)
    @Mapping(target = "updatedUserName", ignore = true)
    User toConvertUser(AddOrEditUserInput source);

    /**
     * 添加修改用户实体转为用户数据范围实体
     *
     * @param source
     * @return
     */
    @Mapping(target = "id", source = "userOrgId")
    @Mapping(target = "sysOrgId", source = "orgId")
    @Mapping(target = "sysUserId", source = "id")
    UserOrg toConvertUserOrg(AddOrEditUserInput source);

}
