package simpleadmin.web.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import simpleadmin.web.entity.DictData;
import simpleadmin.web.entity.map.DictDataVO;

import java.util.List;

/**
 * 编写转换接口
 */
@Mapper(componentModel = "spring")
public interface DictDataCovert {
    DictDataCovert MAPPER = Mappers.getMapper(DictDataCovert.class);

    /**
     * 字段数量类型相同,只是字段多，可以直接转换
     *
     * @param source
     * @return
     */
    DictDataVO toConvertVO(DictData source);

    /**
     * List 转换
     *
     * @param source
     * @return
     */
    List<DictDataVO> toConvertVOs(List<DictData> source);

}
