package simpleadmin.web.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Component;
import simpleadmin.web.common.enums.LoginType;
import simpleadmin.web.common.http.HttpResult;
import simpleadmin.web.common.plugin.logging.async.LoggingFactory;
import simpleadmin.web.common.tools.JsonUtil;
import simpleadmin.web.common.tools.JwtUtil;
import simpleadmin.web.common.tools.ServletUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 登出成功执行的操作
 *
 * @author wangcheng
 * @since 2022-06-24
 */
@Component
public class JwtLogoutSuccessHandler implements LogoutSuccessHandler {

    private final JwtUtil jwtUtil;
    private final LoggingFactory loggingFactory;

    @Autowired
    public JwtLogoutSuccessHandler(JwtUtil jwtUtil, LoggingFactory loggingFactory) {
        this.jwtUtil = jwtUtil;
        this.loggingFactory = loggingFactory;
    }

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
            throws IOException {
        String msg = "登出成功！";
        loggingFactory.saveVisLog(LoginType.LOGOUT, msg);
        if (authentication != null) {
            new SecurityContextLogoutHandler().logout(request, response, authentication);
        }
        response.setHeader(jwtUtil.getTokenResponseHeader(), "");
        response.setHeader(jwtUtil.getRefreshTokenResponseHeader(), "");
        HttpResult result = HttpResult.ok(msg);
        ServletUtil.write(JsonUtil.toString(result));
    }

}
