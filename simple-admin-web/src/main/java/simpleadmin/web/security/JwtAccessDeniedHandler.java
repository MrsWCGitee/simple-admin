package simpleadmin.web.security;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;
import simpleadmin.web.common.http.HttpResult;
import simpleadmin.web.common.http.HttpStatus;
import simpleadmin.web.common.tools.JsonUtil;
import simpleadmin.web.common.tools.ServletUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 拒绝访问执行的操作
 *
 * @author wangcheng
 * @since 2022-06-24
 */
@Component
public class JwtAccessDeniedHandler implements AccessDeniedHandler {

    @Override
    public void handle(HttpServletRequest httpServletRequest, HttpServletResponse response, AccessDeniedException e)
            throws IOException, ServletException {
        HttpResult result = HttpResult.error(HttpStatus.SC_FORBIDDEN, "您没有执行该操作的权限！");
        ServletUtil.write(JsonUtil.toString(result));
    }

}
