package simpleadmin.web.security;

import cn.hutool.jwt.JWTUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import simpleadmin.web.common.enums.LoginType;
import simpleadmin.web.common.http.HttpResult;
import simpleadmin.web.common.plugin.logging.async.LoggingFactory;
import simpleadmin.web.common.tools.JsonUtil;
import simpleadmin.web.common.tools.JwtUtil;
import simpleadmin.web.common.tools.ServletUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 登录成功执行的操作
 *
 * @author wangcheng
 * @since 2022-06-24
 */
@Component
public class JwtLoginSuccessHandler implements AuthenticationSuccessHandler {

    private final LoggingFactory loggingFactory;
    private JwtUtil jwtUtil;

    @Autowired
    public JwtLoginSuccessHandler(LoggingFactory loggingFactory, JwtUtil jwtUtil) {
        this.loggingFactory = loggingFactory;
        this.jwtUtil = jwtUtil;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
        // 生成jwt返回
        String token = jwtUtil.generateToken(authentication.getName());
        // 生成刷新jwt返回
        String refreshToken = jwtUtil.generateRefreshToken(token);
        response.setHeader(jwtUtil.getTokenResponseHeader(), token);
        response.setHeader(jwtUtil.getRefreshTokenResponseHeader(), refreshToken);
        String msg = "登录成功！";
        loggingFactory.saveVisLog(LoginType.LOGIN, msg);
        HttpResult result = HttpResult.ok(msg);
        ServletUtil.write(JsonUtil.toString(result));
    }

}
