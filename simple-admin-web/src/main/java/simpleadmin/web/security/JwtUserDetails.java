package simpleadmin.web.security;

import cn.hutool.core.lang.Assert;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

/**
 * jwt 用户详情
 *
 * @author wangcheng
 * @since 2022-06-24
 */
public class JwtUserDetails implements UserDetails {

    /**
     * 用户名
     */
    private final String username;
    /**
     * 权限
     */
    private final Collection<? extends GrantedAuthority> authorities;
    /**
     * 账号是否过期
     */
    private final boolean accountNonExpired;
    /**
     * 账号是否锁定
     */
    private final boolean accountNonLocked;
    /**
     * 凭证是否过期
     */
    private final boolean credentialsNonExpired;
    /**
     * 是否启用
     */
    private final boolean enabled;
    /**
     * 用户Id
     */
    private Long id;
    /**
     * 密码
     */
    private String password;

    public JwtUserDetails(Long id, String username, String password,
                          Collection<? extends GrantedAuthority> authorities) {
        this(id, username, password, true, true, true, true, authorities);
    }

    public JwtUserDetails(Long id, String username, String password, boolean enabled, boolean accountNonExpired,
                          boolean credentialsNonExpired, boolean accountNonLocked,
                          Collection<? extends GrantedAuthority> authorities) {
        Assert.isTrue(username != null && !"".equals(username) && password != null,
                "不能将空值或空值传递给构造函数 ");
        this.id = id;
        this.username = username;
        this.password = password;
        this.enabled = enabled;
        this.accountNonExpired = accountNonExpired;
        this.credentialsNonExpired = credentialsNonExpired;
        this.accountNonLocked = accountNonLocked;
        this.authorities = authorities;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.authorities;
    }

    public Long getId() {
        return id;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return this.accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return this.credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return this.enabled;
    }

}
