package simpleadmin.web.security;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;
import simpleadmin.web.common.http.HttpResult;
import simpleadmin.web.common.http.HttpStatus;
import simpleadmin.web.common.tools.JsonUtil;
import simpleadmin.web.common.tools.ServletUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Jwt 认证入口点
 *
 * @author wangcheng
 * @since 2022-06-24
 */
@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse response, AuthenticationException e)
            throws IOException, ServletException {
        HttpResult result = HttpResult.error(HttpStatus.SC_UNAUTHORIZED, "您还未登录，请先登录!");
        ServletUtil.write(JsonUtil.toString(result));
    }

}
