package simpleadmin.web.security.model;

import org.springframework.security.core.GrantedAuthority;

/**
 * 权限封装
 *
 * @author wangcheng
 * @date 2022/03/22
 */
public class GrantedAuthorityImpl implements GrantedAuthority {

    private static final long serialVersionUID = 1L;

    private String authority;

    public GrantedAuthorityImpl(String authority) {
        this.authority = authority;
    }

    @Override
    public String getAuthority() {
        return this.authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }
}