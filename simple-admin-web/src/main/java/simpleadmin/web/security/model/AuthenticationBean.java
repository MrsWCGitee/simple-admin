package simpleadmin.web.security.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * 登录接口封装对象
 *
 * @Author wangcheng
 * @date 2022/03/22
 */
@Data
public class AuthenticationBean {

    /**
     * 用户名
     */
    @NotNull(message = "用户名不能为空！")
    @Length(min = 1, max = 20, message = "用户名长度需在10-20之间！")
    @JsonProperty(value = "username")
    private String userName;

    /**
     * 密码
     */
    @NotNull(message = "密码不能为空！")
    @Length(min = 1, max = 20, message = "密码长度需在1-10之间！")
    @JsonProperty(value = "password")
    private String password;

    /**
     * 验证码Key
     */
    @NotNull(message = "验证码Key不能为空！")
    @JsonProperty(value = "captchaCodeKey")
    private String captchaCodeKey;

    /**
     * 验证码
     */
    @NotNull(message = "验证码不能为空！")
    @JsonProperty(value = "code")
    private String code;

}
