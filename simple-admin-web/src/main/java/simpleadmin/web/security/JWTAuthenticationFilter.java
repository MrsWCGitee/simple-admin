package simpleadmin.web.security;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.util.StringUtils;
import simpleadmin.web.common.config.SpringSecurityConfig;
import simpleadmin.web.common.http.HttpStatus;
import simpleadmin.web.common.tools.HttpUtil;
import simpleadmin.web.common.tools.JwtUtil;
import simpleadmin.web.common.tools.ServletUtil;
import simpleadmin.web.entity.User;
import simpleadmin.web.security.model.GrantedAuthorityImpl;
import simpleadmin.web.service.IUserService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Jwt 身份认证过滤器
 *
 * @author wangcheng
 * @since 2022-06-24
 */
public class JWTAuthenticationFilter extends BasicAuthenticationFilter {

    private JwtUtil jwtUtil;
    private IUserService userService;

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager) {
        super(authenticationManager);
    }

    @Autowired
    public void setJwtUtil(JwtUtil jwtUtil) {
        this.jwtUtil = jwtUtil;
    }

    @Autowired
    public void setUserService(IUserService userService) {
        this.userService = userService;
    }

    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        String token = request.getHeader(jwtUtil.getTokenRequestHeader());

        String url = ServletUtil.getRequestURI();
        boolean inWhiteList = ArrayUtil.contains(SpringSecurityConfig.URL_WHITELIST, url);
        if (inWhiteList || !StringUtils.hasLength(token) || !token.startsWith(jwtUtil.getAuthenticationHead())) {
            chain.doFilter(request, response);
            return;
        }
        boolean isValid = jwtUtil.verifyToken(token);
        if (!isValid) {
            throw new RuntimeException("无效的 token！");
        }
        JSONObject payloads = jwtUtil.getPayloads(token);
        if (payloads.isEmpty()) {
            throw new RuntimeException("token格式不正确 ！");
        }
        boolean isExpired = jwtUtil.tokenExpired(payloads);
        if (isExpired) {
            HttpUtil.writeError(response, HttpStatus.SC_EXPIRED_TOKEN_ERROR, "token 已过期！");
            return;
        }
        String userName = jwtUtil.getUserNameByPayloads(payloads);
        if (!StringUtils.hasLength(userName)) {
            throw new RuntimeException("token验证不通过 ！");
        }
        User user = userService.findByUsername(userName);
        Set<String> permissions = userService.findPermissions(user.getId(), user.getAdminType());
        List<GrantedAuthority> userAuthority = permissions.stream().map(GrantedAuthorityImpl::new)
                .collect(Collectors.toList());
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(userName,
                null, userAuthority);
        // 将用户详情放入 PasswordAuthenticationToken
        authenticationToken.setDetails(user);
        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        chain.doFilter(request, response);
    }

}
