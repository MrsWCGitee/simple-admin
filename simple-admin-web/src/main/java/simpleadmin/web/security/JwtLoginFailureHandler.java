package simpleadmin.web.security;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;
import simpleadmin.web.common.http.HttpResult;
import simpleadmin.web.common.tools.JsonUtil;
import simpleadmin.web.common.tools.ServletUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 登录失败执行的操作
 *
 * @author wangcheng
 * @since 2022-06-24
 */
@Component
public class JwtLoginFailureHandler implements AuthenticationFailureHandler {

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
                                        AuthenticationException exception) throws IOException, ServletException {
        String msg = "Bad credentials".equals(exception.getMessage()) ? "用户名或密码不正确" : exception.getMessage();
        HttpResult result = HttpResult.error(msg);
        ServletUtil.write(JsonUtil.toString(result));
    }

}
