package simpleadmin.web.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.validation.Validator;
import simpleadmin.web.common.tools.JsonUtil;
import simpleadmin.web.common.tools.redis.RedisService;
import simpleadmin.web.security.model.AuthenticationBean;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

/**
 * Json 格式的身份认证过滤器
 *
 * @author wangcheng
 * @since 2022-06-24
 */
public class JsonAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private Validator validator;
    private RedisService redisService;

    @Autowired
    public void setValidator(Validator validator) {
        this.validator = validator;
    }

    @Autowired
    public void setRedisService(RedisService redisService) {
        this.redisService = redisService;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {
        if (request.getContentType().startsWith(MediaType.APPLICATION_JSON_VALUE)
                || request.getContentType().equals(MediaType.APPLICATION_JSON_VALUE)) {
            // 如果是 json 格式的数据，执行自定义操作
            UsernamePasswordAuthenticationToken authRequest;
            try (InputStream stream = request.getInputStream()) {
                AuthenticationBean authBean = JsonUtil.toBean(stream, AuthenticationBean.class);
                // 验证码验证
                String captchaCodeKey = Objects.requireNonNull(authBean).getCaptchaCodeKey();
                String requestCaptcha = authBean.getCode();
                if (!StringUtils.hasLength(captchaCodeKey)) {
                    throw new AuthenticationServiceException("验证码Key不能为空!");
                }
                if (!StringUtils.hasLength(requestCaptcha)) {
                    throw new AuthenticationServiceException("验证码不能为空!");
                }
                String code = redisService.get(captchaCodeKey);
                if (!StringUtils.hasLength(code)) {
                    throw new AuthenticationServiceException("验证码已过期，请刷新后再试!");
                }
                if (!code.toLowerCase().equals(requestCaptcha.toLowerCase())) {
                    throw new AuthenticationServiceException("验证码不正确!");
                }
                // 验证
                BindException bindException = new BindException(authBean, authBean.getUserName());
                validator.validate(authBean, bindException);
                if (bindException.hasErrors()) {
                    throw new BindException(bindException.getBindingResult());
                }
                authRequest = new UsernamePasswordAuthenticationToken(authBean.getUserName(),
                        authBean.getPassword());
            } catch (IOException | BindException e) {
                e.printStackTrace();
                authRequest = new UsernamePasswordAuthenticationToken("", "");
            }
            setDetails(request, authRequest);
            return this.getAuthenticationManager().authenticate(authRequest);
        } else {
            // 如果不是 json 格式的数据，就使用原有的处理方式 UsernamePasswordAuthenticationFilter
            return super.attemptAuthentication(request, response);
        }
    }

}
