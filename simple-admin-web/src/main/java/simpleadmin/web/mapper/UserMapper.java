package simpleadmin.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.data.repository.query.Param;
import simpleadmin.web.entity.User;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-03
 */
public interface UserMapper extends BaseMapper<User> {

    /**
     * 获取用户拥有的权限
     *
     * @param status
     * @param userId
     * @return
     */
    @Select("select distinct permission from sys_menu where permission is not null and status = #{status} and type=2 and Id in (select SysMenuId from " +
            "sys_role_menu as A inner join sys_user_role as B on A.SysRoleId=B.SysRoleId where B.SysUserId= #{userId})")
    List<String> selectPermissionsByUserId(@Param("status") Integer status, Long userId);

    /**
     * 获取超级管理员拥有的权限
     * @param status
     * @return
     */
    @Select("select distinct permission from sys_menu where permission is not null and status = #{status} and type=2")
    List<String> selectPermissionsOfSuperAdmin(@Param("status") Integer status);

}
