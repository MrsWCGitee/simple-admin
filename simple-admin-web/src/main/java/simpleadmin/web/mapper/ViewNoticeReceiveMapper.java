package simpleadmin.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import simpleadmin.web.entity.ViewNoticeReceive;

/**
 * <p>
 * VIEW Mapper 接口
 * </p>
 *
 * @author wangcheng
 * @since 2022-08-15
 */
public interface ViewNoticeReceiveMapper extends BaseMapper<ViewNoticeReceive> {

}
