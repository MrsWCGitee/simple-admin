package simpleadmin.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Update;
import simpleadmin.web.entity.LogVis;

/**
 * <p>
 * 访问日志 Mapper 接口
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
public interface LogVisMapper extends BaseMapper<LogVis> {

    /**
     * 清空表数据
     */
    @Update("truncate table sys_log_vis")
    void truncateTable();

}
