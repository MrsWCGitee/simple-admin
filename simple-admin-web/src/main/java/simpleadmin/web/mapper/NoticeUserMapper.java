package simpleadmin.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import simpleadmin.web.entity.NoticeUser;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
public interface NoticeUserMapper extends BaseMapper<NoticeUser> {

}
