package simpleadmin.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Update;
import simpleadmin.web.entity.LogOp;

/**
 * <p>
 * 操作日志 Mapper 接口
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
public interface LogOpMapper extends BaseMapper<LogOp> {

    /**
     * 清空表数据
     */
    @Update("truncate table sys_log_op")
    void truncateTable();

}
