package simpleadmin.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;
import simpleadmin.web.entity.ViewTable;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * VIEW Mapper 接口
 * </p>
 *
 * @author wangcheng
 * @since 2022-08-15
 */
public interface ViewTableMapper extends BaseMapper<ViewTable> {

    /**
     * 获取数据表结构
     *
     * @param tableName 表名
     * @return
     */
    @Select("select * from information_schema.COLUMNS where TABLE_SCHEMA = (select database()) and TABLE_NAME=#{tableName} order by ORDINAL_POSITION")
    List<Map> getTableColumn(String tableName);

}
