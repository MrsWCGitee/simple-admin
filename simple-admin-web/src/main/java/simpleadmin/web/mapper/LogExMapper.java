package simpleadmin.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Update;
import simpleadmin.web.entity.LogEx;

/**
 * <p>
 * 异常日志 Mapper 接口
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
public interface LogExMapper extends BaseMapper<LogEx> {

    /**
     * 清空表数据
     */
    @Update("truncate table sys_log_ex")
    void truncateTable();

}
