package simpleadmin.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;
import simpleadmin.web.entity.Role;
import simpleadmin.web.entity.dto.role.RoleOutput;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-03
 */
public interface RoleMapper extends BaseMapper<Role> {

    /**
     * 获取角色列表（根绝用户Id）
     *
     * @param userId
     * @return
     */
    @Select("select id,code,name from sys_role where Id in (select SysRoleId from sys_user as A right join " +
            "sys_user_role as B on A.Id = B.SysUserId where A.Id = #{userId})")
    List<RoleOutput> getUserRoleList(Long userId);
}
