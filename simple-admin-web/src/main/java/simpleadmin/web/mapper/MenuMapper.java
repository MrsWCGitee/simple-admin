package simpleadmin.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.data.repository.query.Param;
import simpleadmin.web.entity.Menu;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-03
 */
public interface MenuMapper extends BaseMapper<Menu> {

    @Select("select * from sys_menu where status = #{status} and type != #{type} and Id in (select SysMenuId from " +
            "sys_role as A right join sys_role_menu as B on A.Id = B.SysRoleId where A.Id = #{roleId})")
    List<Menu> selectListByRoleId(@Param("status") Integer status, @Param("type") Integer type, @Param("roleId") Long roleId);

    @Select("select * from sys_menu where status = #{status} and type != #{type} and Id in (select SysMenuId from " +
            "sys_role_menu as A inner join sys_user_role as B on A.SysRoleId=B.SysRoleId where B.SysUserId= #{userId})")
    List<Menu> selectListByUserId(@Param("status") Integer status, @Param("type") Integer type, @Param("userId") Long userId);

}
