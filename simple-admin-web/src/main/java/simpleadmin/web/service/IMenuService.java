package simpleadmin.web.service;

import cn.hutool.core.lang.tree.Tree;
import com.baomidou.mybatisplus.extension.service.IService;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.entity.Menu;
import simpleadmin.web.entity.dto.menu.MaterialTreeNode;
import simpleadmin.web.entity.query.MenuPageQuery;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-03
 */
public interface IMenuService extends IService<Menu> {

    /**
     * 根据角色 Id 获取对应菜单数据
     *
     * @param roleId
     * @return
     */
    List<Menu> selectListByRoleId(Long roleId);

    /**
     * 根据用户 Id 获取对应菜单数据
     *
     * @param userId
     * @return
     */
    List<Menu> selectListByUserId(Long userId);

    /**
     * 根据用户 Id 获取登录的菜单信息
     *
     * @param userId
     * @return
     */
    List<MaterialTreeNode> getLoginMenusMaterial(Long userId);

    /**
     * 多条件分页查询
     *
     * @param query 分页参数
     * @return
     */
    PageResult<Menu> pageQuery(MenuPageQuery query);

    /**
     * 获取树形数据
     *
     * @return
     */
    List<Tree<Long>> getMenuTree();

}
