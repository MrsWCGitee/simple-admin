package simpleadmin.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import simpleadmin.web.entity.RoleMenu;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
public interface IRoleMenuService extends IService<RoleMenu> {

}
