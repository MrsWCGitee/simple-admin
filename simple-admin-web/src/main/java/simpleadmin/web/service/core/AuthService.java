package simpleadmin.web.service.core;

import cn.hutool.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import simpleadmin.web.common.http.HttpStatus;
import simpleadmin.web.common.tools.HttpUtil;
import simpleadmin.web.common.tools.JwtUtil;
import simpleadmin.web.entity.dto.auth.RefreshTokenOutput;

/**
 * 用户授权服务相关
 *
 * @author wangcheng
 * @since 2023-03-23
 */
@Service
public class AuthService {

    private final JwtUtil jwtUtil;

    @Autowired
    public AuthService(JwtUtil jwtUtil) {
        this.jwtUtil = jwtUtil;
    }

    /**
     * 根据 refreshToken 生成 token
     *
     * @param refreshToken 刷新 token
     * @return
     */
    public RefreshTokenOutput refreshToken(String token, String refreshToken) {
        try {
            boolean isValid = jwtUtil.verifyToken(token);
            if (!isValid) {
                throw new RuntimeException("无效的 token！");
            }
            JSONObject payloads = jwtUtil.getPayloads(token);
            if (payloads.isEmpty()) {
                throw new RuntimeException("token格式不正确 ！");
            }
            String userName = jwtUtil.getUserNameByPayloads(payloads);
            if (!StringUtils.hasLength(userName)) {
                throw new RuntimeException("token验证不通过 ！");
            }

            boolean isValid2 = jwtUtil.verifyToken(refreshToken);
            if (!isValid2) {
                throw new RuntimeException("无效的 refreshToken！");
            }
            JSONObject payloads2 = jwtUtil.getPayloads(refreshToken);
            if (payloads2.isEmpty()) {
                throw new RuntimeException("refreshToken格式不正确 ！");
            }
            boolean isExpired2 = jwtUtil.tokenExpired(payloads2);
            if (isExpired2) {
                throw new RuntimeException("refreshToken已过期请重新登录 ！");
            }
            String token2 = jwtUtil.getTokenByPayloads(payloads2);
            if (!token.equals(token2)) {
                throw new RuntimeException("token 和 refreshToken 不匹配 ！");
            }

            // 重新签发 token
            String newToken = jwtUtil.generateToken(userName);
            // 重新签发 refreshToken
            String newRefreshToken = jwtUtil.generateRefreshToken(newToken);

            RefreshTokenOutput result = new RefreshTokenOutput(newToken, newRefreshToken);
            return result;
        } catch (Exception ex) {
            throw new RuntimeException("refreshToken过程发生错误，请重新登录 ！");
        }
    }

}
