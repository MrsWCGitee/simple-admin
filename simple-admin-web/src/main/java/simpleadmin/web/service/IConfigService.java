package simpleadmin.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.entity.Config;
import simpleadmin.web.entity.query.ConfigPageQuery;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
public interface IConfigService extends IService<Config> {

    PageResult<Config> pageQuery(ConfigPageQuery query);

}
