package simpleadmin.web.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import simpleadmin.web.common.enums.AdminType;
import simpleadmin.web.common.enums.CommonStatus;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.common.tools.ServletUtil;
import simpleadmin.web.entity.User;
import simpleadmin.web.entity.UserOrg;
import simpleadmin.web.entity.UserRole;
import simpleadmin.web.entity.dto.auth.LoginOutput;
import simpleadmin.web.entity.dto.menu.MaterialTreeNode;
import simpleadmin.web.entity.dto.role.RoleOutput;
import simpleadmin.web.entity.dto.user.AddOrEditUserInput;
import simpleadmin.web.entity.dto.user.GrantUserOrgInput;
import simpleadmin.web.entity.dto.user.GrantUserRoleInput;
import simpleadmin.web.entity.query.UserPageQuery;
import simpleadmin.web.mapper.UserMapper;
import simpleadmin.web.mapstruct.UserConvert;
import simpleadmin.web.service.*;
import simpleadmin.web.service.impl.base.BaseServiceImpl;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-03
 */
@Service
public class UserServiceImpl extends BaseServiceImpl<UserMapper, User> implements IUserService {

    private final UserMapper userMapper;
    private final IRoleService roleService;
    private final IMenuService menuService;
    private final IUserOrgService userOrgService;
    private final IUserRoleService userRoleService;
    private final UserConvert userConvert;

    @Autowired
    public UserServiceImpl(
            UserMapper userMapper, IRoleService roleService, IMenuService menuService, IUserOrgService userOrgService,
            IUserRoleService userRoleService, UserConvert userConvert) {
        this.userMapper = userMapper;
        this.roleService = roleService;
        this.menuService = menuService;
        this.userOrgService = userOrgService;
        this.userRoleService = userRoleService;
        this.userConvert = userConvert;
    }

    /**
     * 多条件分页查询
     *
     * @param query 分页参数
     * @return 结果
     */
    @Override
    public PageResult<User> pageQuery(UserPageQuery query) {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();

        // #region 拼接查询条件
        if (StringUtils.hasLength(query.getAccount())) {
            queryWrapper.like("account", query.getAccount());
        }
        if (StringUtils.hasLength(query.getNickName())) {
            queryWrapper.like("nickName", query.getNickName());
        }
        if (StringUtils.hasLength(query.getName())) {
            queryWrapper.like("name", query.getName());
        }
        if (query.getBirthday() != null && query.getBirthday().length == 2) {
            queryWrapper.ge("birthday", query.getBirthday()[0]);
            queryWrapper.le("birthday", query.getBirthday()[1]);
        }
        if (query.getOrgId() != null && query.getOrgId().length > 0) {
            QueryWrapper<UserOrg> userOrgWrapper = new QueryWrapper<>();
            userOrgWrapper.in("sysOrgId", (Object[]) query.getOrgId());
            List<UserOrg> userRoles = userOrgService.list(userOrgWrapper);
            if (userRoles != null && userRoles.size() > 0) {
                List<Long> userIds = userRoles.stream().map(UserOrg::getSysUserId).collect(Collectors.toList());
                if (userIds.size() > 0) {
                    queryWrapper.in("id", userIds);
                }
            }
        }
        // #endregion

        Page<User> page = pageHelper.initAllPageSetting(query, queryWrapper);
        Page<User> result = userMapper.selectPage(page, queryWrapper);
        return pageMapperToPageResult(result);
    }

    /**
     * 添加或修改
     *
     * @param input 输入参数
     */
    public void addOrEdit(AddOrEditUserInput input) {
        User user = userConvert.toConvertUser(input);
        if (!(user.getId() > 0)) {
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            final String encoderPassword = encoder.encode(user.getPassword());
            user.setPassword(encoderPassword);
        }
        this.saveOrUpdate(user);
    }

    @Override
    public User findByUsername(String username) {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        if (StringUtils.hasLength(username)) {
            queryWrapper.eq("account", username);
        } else {
            return null;
        }
        return userMapper.selectOne(queryWrapper);
    }

    @Override
    public User getById(Long id) {
        return userMapper.selectById(id);
    }

    /**
     * 获取用户的数据
     *
     * @return 结果
     */
    public List<User> getUserSelector(String name) {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();

        // #region 拼接查询条件
        queryWrapper.ne("adminType", AdminType.SuperAdmin.getValue());
        if (StringUtils.hasLength(name)) {
            queryWrapper.eq("name", name);
        }
        // #endregion

        return userMapper.selectList(queryWrapper);
    }

    @Override
    public Set<String> findPermissions(Long userId,Integer adminType) {
        List<String> permissions;
        if (adminType.equals(AdminType.SuperAdmin.getValue())) {
            permissions = userMapper.selectPermissionsOfSuperAdmin(CommonStatus.ENABLE.getValue());
        } else {
            permissions = userMapper.selectPermissionsByUserId(CommonStatus.ENABLE.getValue(), userId);
        }
        return new HashSet<>(permissions);
    }

    /**
     * 获取登录用户信息
     *
     * @param userId 用户Id
     * @return 结果
     */
    public LoginOutput getLoginUser(Long userId) {
        User user = userMapper.selectById(userId);
        LoginOutput result = userConvert.toConvertVO(user);
        result.setLastLoginIp(ServletUtil.getRemoteHost());
        result.setLastLoginBrowser(ServletUtil.getBrowser());
        result.setLastLoginOs(ServletUtil.getOsSystem());
        result.setLastLoginTime(LocalDateTime.now());
        // 角色相关
        List<RoleOutput> roles = roleService.getUserRoleList(userId);
        result.setRoles(roles);
        // 菜单相关
        List<MaterialTreeNode> menus = menuService.getLoginMenusMaterial(userId);
        result.setMenus(menus);
        return result;
    }

    /**
     * 用户授权的角色
     */
    @Override
    public List<Long> ownRole(Long userId) {
        QueryWrapper<UserRole> userRoleWrapper = new QueryWrapper<>();
        userRoleWrapper.eq("sysUserId", userId);
        List<UserRole> userRoles = userRoleService.list(userRoleWrapper);
        return userRoles.stream().map(UserRole::getSysRoleId).collect(Collectors.toList());
    }

    /**
     * 用户角色
     *
     * @param input 请求参数
     */
    @Override
    public void grantRole(GrantUserRoleInput input) {
        QueryWrapper<UserRole> userRoleWrapper = new QueryWrapper<>();
        userRoleWrapper.eq("sysUserId", input.getUserId());
        userRoleService.remove(userRoleWrapper);
        ArrayList<UserRole> roleMenus = new ArrayList<>();
        for (Long grantRoleId : input.getGrantRoleIds()) {
            UserRole roleMenu = new UserRole();
            roleMenu.setSysUserId(input.getUserId());
            roleMenu.setSysRoleId(grantRoleId);
            roleMenus.add(roleMenu);
        }
        userRoleService.saveBatch(roleMenus);
    }

    /**
     * 用户授权数据
     */
    public List<Long> ownData(Long userId) {
        QueryWrapper<UserOrg> userDataWrapper = new QueryWrapper<>();
        userDataWrapper.eq("sysUserId", userId);
        List<UserOrg> userOrgs = userOrgService.list(userDataWrapper);
        return userOrgs.stream().map(UserOrg::getSysOrgId).collect(Collectors.toList());
    }

    /**
     * 用户数据
     *
     * @param input 请求参数
     */
    public void grantData(GrantUserOrgInput input) {
        QueryWrapper<UserOrg> userDataWrapper = new QueryWrapper<>();
        userDataWrapper.eq("sysUserId", input.getUserId());
        userOrgService.remove(userDataWrapper);
        ArrayList<UserOrg> userDatas = new ArrayList<>();
        for (Long grantOrgId : input.getGrantOrgIds()) {
            UserOrg userData = new UserOrg();
            userData.setSysUserId(input.getUserId());
            userData.setSysOrgId(grantOrgId);
            userDatas.add(userData);
        }
        userOrgService.saveBatch(userDatas);
    }

    /**
     * 重置用户密码
     * @param userId
     */
    public void resetPassword(Long userId,String newPassword){
        UpdateWrapper updateWrapper = new UpdateWrapper();
        updateWrapper.eq("id", userId);
        String newPwd = new BCryptPasswordEncoder().encode(newPassword);
        updateWrapper.set("password",  newPwd);
        this.update(null, updateWrapper);
    }

}
