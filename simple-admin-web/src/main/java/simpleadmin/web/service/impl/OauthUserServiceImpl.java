package simpleadmin.web.service.impl;

import org.springframework.stereotype.Service;
import simpleadmin.web.entity.OauthUser;
import simpleadmin.web.mapper.OauthUserMapper;
import simpleadmin.web.service.IOauthUserService;
import simpleadmin.web.service.impl.base.BaseServiceImpl;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
@Service
public class OauthUserServiceImpl extends BaseServiceImpl<OauthUserMapper, OauthUser> implements IOauthUserService {

}
