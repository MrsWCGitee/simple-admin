package simpleadmin.web.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.entity.DictType;
import simpleadmin.web.entity.query.DictTypePageQuery;
import simpleadmin.web.mapper.DictTypeMapper;
import simpleadmin.web.service.IDictTypeService;
import simpleadmin.web.service.impl.base.BaseServiceImpl;

/**
 * <p>
 * 字典类型 服务实现类
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-03
 */
@Service
public class DictTypeServiceImpl extends BaseServiceImpl<DictTypeMapper, DictType> implements IDictTypeService {

    private DictTypeMapper dictTypeMapper;

    @Autowired
    public DictTypeServiceImpl(DictTypeMapper dictTypeMapper) {
        this.dictTypeMapper = dictTypeMapper;
    }

    /**
     * 多条件分页查询
     *
     * @param query 分页参数
     * @return 分页结果
     */
    @Override
    public PageResult<DictType> pageQuery(DictTypePageQuery query) {
        QueryWrapper<DictType> queryWrapper = new QueryWrapper<>();

        // #region 拼接查询条件
        if (StringUtils.hasLength(query.getName())) {
            queryWrapper.like("name", query.getName());
        }
        if (StringUtils.hasLength(query.getCode())) {
            queryWrapper.like("code", query.getCode());
        }
        // #endregion

        Page<DictType> page = pageHelper.initAllPageSetting(query, queryWrapper);
        Page<DictType> result = dictTypeMapper.selectPage(page, queryWrapper);
        return pageMapperToPageResult(result);
    }

    public DictType getDictTypeByCode(String code) {
        QueryWrapper<DictType> dictTypeWrapper = new QueryWrapper<>();
        dictTypeWrapper.eq("code", code);
        return this.getOne(dictTypeWrapper);
    }

}
