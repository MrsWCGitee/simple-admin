package simpleadmin.web.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.entity.LogOp;
import simpleadmin.web.entity.query.LogOpPageQuery;
import simpleadmin.web.mapper.LogOpMapper;
import simpleadmin.web.service.ILogOpService;
import simpleadmin.web.service.impl.base.BaseServiceImpl;

/**
 * <p>
 * 操作日志 服务实现类
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
@Service
public class LogOpServiceImpl extends BaseServiceImpl<LogOpMapper, LogOp> implements ILogOpService {

    private final LogOpMapper logOpMapper;

    @Autowired
    public LogOpServiceImpl(LogOpMapper logOpMapper) {
        this.logOpMapper = logOpMapper;
    }

    /**
     * 多条件分页查询
     *
     * @param query 分页参数
     * @return 分页结果
     */
    @Override
    public PageResult<LogOp> pageQuery(LogOpPageQuery query) {
        QueryWrapper<LogOp> queryWrapper = new QueryWrapper<>();

        // #region 拼接查询条件
        if (StringUtils.hasLength(query.getName())) {
            queryWrapper.like("name", query.getName());
        }
        if (StringUtils.hasLength(query.getReqMethod())) {
            queryWrapper.like("reqMethod", query.getReqMethod());
        }
        if (StringUtils.hasLength(query.getSuccess())) {
            queryWrapper.like("success", query.getSuccess());
        }
        if (query.getOpTime() != null && query.getOpTime().length == 2) {
            queryWrapper.ge("opTime", query.getOpTime()[0]);
            queryWrapper.le("opTime", query.getOpTime()[1]);
        }
        // #endregion

        Page<LogOp> page = pageHelper.initAllPageSetting(query, queryWrapper);
        Page<LogOp> result = logOpMapper.selectPage(page, queryWrapper);
        return pageMapperToPageResult(result);
    }

    /**
     * 清空表数据
     */
    @Override
    public void truncateTable() {
        logOpMapper.truncateTable();
    }

}
