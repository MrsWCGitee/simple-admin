package simpleadmin.web.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.entity.DictData;
import simpleadmin.web.entity.query.DictDataPageQuery;
import simpleadmin.web.mapper.DictDataMapper;
import simpleadmin.web.service.IDictDataService;
import simpleadmin.web.service.impl.base.BaseServiceImpl;

import java.util.List;

/**
 * <p>
 * 字典数据 服务实现类
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-03
 */
@Service
public class DictDataServiceImpl extends BaseServiceImpl<DictDataMapper, DictData> implements IDictDataService {

    private final DictDataMapper dictDataMapper;

    @Autowired
    public DictDataServiceImpl(DictDataMapper dictDataMapper) {
        this.dictDataMapper = dictDataMapper;
    }

    /**
     * 多条件分页查询
     *
     * @param query 分页参数
     * @return 结果
     */
    @Override
    public PageResult<DictData> pageQuery(DictDataPageQuery query) {
        QueryWrapper<DictData> queryWrapper = new QueryWrapper<>();

        // #region 拼接查询条件
        queryWrapper.eq("typeId", query.getTypeId());
        if (StringUtils.hasLength(query.getValue())) {
            queryWrapper.like("value", query.getValue());
        }
        if (StringUtils.hasLength(query.getCode())) {
            queryWrapper.like("code", query.getCode());
        }
        // #endregion

        Page<DictData> page = pageHelper.initAllPageSetting(query, queryWrapper);
        Page<DictData> result = dictDataMapper.selectPage(page, queryWrapper);
        return pageMapperToPageResult(result);
    }

    public List<DictData> getDictDataListByDictTypeId(Long typeId) {
        QueryWrapper<DictData> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("typeId", typeId).orderByAsc("sort").select("code", "value");
        return dictDataMapper.selectList(queryWrapper);
    }

}
