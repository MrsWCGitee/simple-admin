package simpleadmin.web.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.entity.OnlineUser;
import simpleadmin.web.entity.query.OnlineUserPageQuery;
import simpleadmin.web.mapper.OnlineUserMapper;
import simpleadmin.web.service.IOnlineUserService;
import simpleadmin.web.service.impl.base.BaseServiceImpl;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
@Service
public class OnlineUserServiceImpl extends BaseServiceImpl<OnlineUserMapper, OnlineUser> implements IOnlineUserService {

    private final OnlineUserMapper onlineUserMapper;

    @Autowired
    public OnlineUserServiceImpl(OnlineUserMapper onlineUserMapper) {
        this.onlineUserMapper = onlineUserMapper;
    }

    /**
     * 多条件分页查询
     *
     * @param query 分页参数
     * @return 结果
     */
    @Override
    public PageResult<OnlineUser> pageQuery(OnlineUserPageQuery query) {
        QueryWrapper<OnlineUser> queryWrapper = new QueryWrapper<>();

        // #region 拼接查询条件
        if (StringUtils.hasLength(query.getKeyword())) {
            queryWrapper.or().like("account", query.getKeyword());
        }
        if (StringUtils.hasLength(query.getKeyword())) {
            queryWrapper.or().like("name", query.getKeyword());
        }
        // #endregion

        Page<OnlineUser> page = pageHelper.initAllPageSetting(query, queryWrapper);
        Page<OnlineUser> result = onlineUserMapper.selectPage(page, queryWrapper);
        return pageMapperToPageResult(result);
    }

    /**
     * 根据用户Id获取在线用户信息
     *
     * @param userId 用户Id
     * @return 结果
     */
    public OnlineUser getByUserId(Long userId) {
        QueryWrapper<OnlineUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("userId", userId);
        return onlineUserMapper.selectOne(queryWrapper);
    }

    /**
     * 根据用户Id获取在线用户信息
     *
     * @param userId 用户Id
     * @return 结果
     */
    public List<OnlineUser> getListByUserId(Long userId) {
        QueryWrapper<OnlineUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("userId", userId);
        return onlineUserMapper.selectList(queryWrapper);
    }

}
