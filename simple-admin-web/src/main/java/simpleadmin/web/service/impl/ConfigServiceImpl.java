package simpleadmin.web.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.entity.Config;
import simpleadmin.web.entity.query.ConfigPageQuery;
import simpleadmin.web.mapper.ConfigMapper;
import simpleadmin.web.service.IConfigService;
import simpleadmin.web.service.impl.base.BaseServiceImpl;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
@Service
public class ConfigServiceImpl extends BaseServiceImpl<ConfigMapper, Config> implements IConfigService {

    private final ConfigMapper configMapper;

    @Autowired
    public ConfigServiceImpl(ConfigMapper configMapper) {
        this.configMapper = configMapper;
    }

    /**
     * 多条件分页查询
     *
     * @param query 分页参数
     * @return 分页结果
     */
    @Override
    public PageResult<Config> pageQuery(ConfigPageQuery query) {
        QueryWrapper<Config> queryWrapper = new QueryWrapper<>();

        // #region 拼接查询条件
        if (StringUtils.hasLength(query.getName())) {
            queryWrapper.like("name", query.getName());
        }
        if (StringUtils.hasLength(query.getCode())) {
            queryWrapper.like("code", query.getCode());
        }
        if (StringUtils.hasLength(query.getGroupCode())) {
            queryWrapper.like("groupCode", query.getGroupCode());
        }
        // #endregion

        Page<Config> page = pageHelper.initAllPageSetting(query, queryWrapper);
        Page<Config> result = configMapper.selectPage(page, queryWrapper);
        return pageMapperToPageResult(result);
    }

}
