package simpleadmin.web.service.impl;

import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNodeConfig;
import cn.hutool.core.lang.tree.TreeUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import simpleadmin.web.common.enums.*;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.common.tools.SecurityUtil;
import simpleadmin.web.entity.Menu;
import simpleadmin.web.entity.User;
import simpleadmin.web.entity.dto.menu.MaterialTreeNode;
import simpleadmin.web.entity.dto.menu.Meta;
import simpleadmin.web.entity.query.MenuPageQuery;
import simpleadmin.web.mapper.MenuMapper;
import simpleadmin.web.service.IMenuService;
import simpleadmin.web.service.impl.base.BaseServiceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-03
 */
@Service
public class MenuServiceImpl extends BaseServiceImpl<MenuMapper, Menu> implements IMenuService {

    private final MenuMapper menuMapper;

    @Autowired
    public MenuServiceImpl(MenuMapper menuMapper) {
        this.menuMapper = menuMapper;
    }

    @Override
    public List<Menu> selectListByRoleId(Long roleId) {
        return menuMapper.selectListByRoleId(CommonStatus.ENABLE.getValue(), MenuType.Btn.getValue(), roleId);
    }

    @Override
    public List<Menu> selectListByUserId(Long userId) {
        return menuMapper.selectListByUserId(CommonStatus.ENABLE.getValue(), MenuType.Btn.getValue(), userId);
    }

    /**
     * 根据用户 Id 获取登录的菜单信息
     *
     * @param userId 用户Id
     * @return 结果
     */
    @Override
    public List<MaterialTreeNode> getLoginMenusMaterial(Long userId) {
        List<MaterialTreeNode> result = new ArrayList<>();

        List<Menu> menus;
        User userDetails = SecurityUtil.getUserDetails();
        if (Objects.requireNonNull(userDetails).getAdminType().equals(AdminType.SuperAdmin.getValue())) {
            QueryWrapper<Menu> queryWrapper = new QueryWrapper<>();

            // #region 拼接查询条件
            queryWrapper.eq("status", CommonStatus.ENABLE.getValue());
            queryWrapper.ne("type", MenuType.Btn.getValue());
            // 多个排序
            // queryWrapper.orderBy(true, true, "status").orderBy(true, false, "name");
            queryWrapper.orderByAsc("sort");
            // #endregion

            menus = menuMapper.selectList(queryWrapper);
        } else {
            menus = menuMapper.selectListByUserId(CommonStatus.ENABLE.getValue(), MenuType.Btn.getValue(), userId);
        }

        menus.forEach(m -> {
            MaterialTreeNode treeNode = new MaterialTreeNode();

            String redirect = m.getOpenType().equals(MenuOpenType.Outer.getValue()) ? m.getLink() : m.getRedirect();
            String path = m.getOpenType().equals(MenuOpenType.Outer.getValue()) ? m.getLink() : m.getRouter();
            String target = m.getOpenType().equals(MenuOpenType.Outer.getValue()) ? "_blank" : "";

            treeNode.setId(m.getId());
            treeNode.setPid(m.getPid());
            treeNode.setName(m.getName());
            treeNode.setComponent(m.getComponent());
            treeNode.setRedirect(redirect);
            treeNode.setPath(path);

            Meta meta = new Meta();
            meta.setTitle(m.getName());
            meta.setIcon(m.getIcon());
            Boolean visible = m.getVisible().equals(YesOrNot.Y.getName());

            meta.setShow(visible);
            meta.setLink(m.getLink());
            meta.setTarget(target);
            treeNode.setMeta(meta);

            result.add(treeNode);
        });
        return result;
    }

    /**
     * 多条件分页查询
     *
     * @param query 分页参数
     * @return 结果
     */
    @Override
    public PageResult<Menu> pageQuery(MenuPageQuery query) {
        QueryWrapper<Menu> queryWrapper = new QueryWrapper<>();

        // #region 拼接查询条件
        if (StringUtils.hasLength(query.getName())) {
            queryWrapper.like("name", query.getName());
        }
        // 判断是否查询的是全部数据
        if (query.getItemsPerPage().equals(-1)) {
            int sumCount = menuMapper.selectCount(queryWrapper).intValue();
            query.setItemsPerPage(sumCount);
        }
        // #endregion

        Page<Menu> page = pageHelper.initAllPageSetting(query, queryWrapper);
        Page<Menu> result = menuMapper.selectPage(page, queryWrapper);
        return pageMapperToPageResult(result);
    }

    /**
     * 获取树形数据
     *
     * @return 结果
     */
    @Override
    public List<Tree<Long>> getMenuTree() {
        // 1.配置
        TreeNodeConfig config = new TreeNodeConfig();
        // 自定义属性名
        config.setWeightKey("sort"); // 权重排序字段 默认为weight
        config.setIdKey("id"); // 默认为id可以不设置
        config.setNameKey("name"); // 节点名对应名称 默认为name
        config.setParentIdKey("pid"); // 父节点 默认为parentId
        config.setChildrenKey("children"); // 子点 默认为children
        // config.setDeep(3); // 可以配置递归深度 从0开始计算 默认此配置为空,即不限制

        // 2.数据源
        QueryWrapper<Menu> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("status", CommonStatus.ENABLE.getValue());
        queryWrapper.orderByAsc("sort");
        List<Menu> menus = menuMapper.selectList(queryWrapper);

        // 3.转树，Tree<>里面泛型为id的类型
        return TreeUtil.build(menus, 0L, config, (object, tree) -> {
            tree.setId(object.getId());
            tree.setParentId(object.getPid());
            tree.setName(object.getName());
            tree.setWeight(object.getSort());
        });
    }

}
