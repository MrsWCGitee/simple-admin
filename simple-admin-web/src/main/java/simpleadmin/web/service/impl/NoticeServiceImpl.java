package simpleadmin.web.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import simpleadmin.web.common.enums.CommonStatus;
import simpleadmin.web.common.enums.NoticeStatus;
import simpleadmin.web.common.enums.NoticeUserStatus;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.common.tools.SecurityUtil;
import simpleadmin.web.entity.Notice;
import simpleadmin.web.entity.dto.notice.AddNoticeInput;
import simpleadmin.web.entity.query.NoticePageQuery;
import simpleadmin.web.mapper.NoticeMapper;
import simpleadmin.web.mapstruct.NoticeCovert;
import simpleadmin.web.service.INoticeService;
import simpleadmin.web.service.impl.base.BaseServiceImpl;

import java.time.LocalDateTime;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
@Service
public class NoticeServiceImpl extends BaseServiceImpl<NoticeMapper, Notice> implements INoticeService {

    private final NoticeMapper noticeMapper;
    private final NoticeUserServiceImpl noticeUserService;
    private final NoticeCovert noticeCovert;

    @Autowired
    public NoticeServiceImpl(NoticeMapper noticeMapper, NoticeUserServiceImpl noticeUserService,
                             NoticeCovert noticeCovert) {
        this.noticeMapper = noticeMapper;
        this.noticeUserService = noticeUserService;
        this.noticeCovert = noticeCovert;
    }

    /**
     * 多条件分页查询
     *
     * @param query 分页参数
     * @return 结果
     */
    @Override
    public PageResult<Notice> pageQuery(NoticePageQuery query) {
        QueryWrapper<Notice> queryWrapper = new QueryWrapper<>();

        // #region 拼接查询条件
        if (StringUtils.hasLength(query.getKeyword())) {
            queryWrapper.like("title", query.getKeyword());
        }
        if (StringUtils.hasLength(query.getType())) {
            queryWrapper.like("type", query.getType());
        }
        // #endregion

        Page<Notice> page = pageHelper.initAllPageSetting(query, queryWrapper);
        Page<Notice> result = noticeMapper.selectPage(page, queryWrapper);
        return pageMapperToPageResult(result);
    }

    /**
     * 添加通知消息
     *
     * @param input 请求参数
     */
    public void add(AddNoticeInput input) {
        // 设置发布状态
        input.setStatus(NoticeStatus.PUBLIC.getValue());
        Notice notice = noticeCovert.toConvertVO(input);
        Long userId = SecurityUtil.getUserId();
        notice.setPublicUserId(userId);
        notice.setPublicOrgId(0L);
        notice.setPublicTime(LocalDateTime.now());
        notice.setCancelTime(LocalDateTime.now());
        notice.setIsDeleted(CommonStatus.ENABLE.getValue());
        boolean result = this.saveOrUpdate(notice);
        if (result) {
            noticeUserService.add(notice.getId(), input.getNoticeUserIdList(), NoticeUserStatus.UNREAD);
        }
    }

}
