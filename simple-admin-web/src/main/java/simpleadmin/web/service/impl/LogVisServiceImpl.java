package simpleadmin.web.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.entity.LogVis;
import simpleadmin.web.entity.query.LogVisPageQuery;
import simpleadmin.web.mapper.LogVisMapper;
import simpleadmin.web.service.ILogVisService;
import simpleadmin.web.service.impl.base.BaseServiceImpl;

/**
 * <p>
 * 访问日志 服务实现类
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
@Service
public class LogVisServiceImpl extends BaseServiceImpl<LogVisMapper, LogVis> implements ILogVisService {

    private final LogVisMapper logVisMapper;

    @Autowired
    public LogVisServiceImpl(LogVisMapper logVisMapper) {
        this.logVisMapper = logVisMapper;
    }

    /**
     * 多条件分页查询
     *
     * @param query 分页参数
     * @return 分页结果
     */
    @Override
    public PageResult<LogVis> pageQuery(LogVisPageQuery query) {
        QueryWrapper<LogVis> queryWrapper = new QueryWrapper<>();

        // #region 拼接查询条件
        if (StringUtils.hasLength(query.getName())) {
            queryWrapper.like("name", query.getName());
        }
        if (StringUtils.hasLength(query.getVisType())) {
            queryWrapper.like("visType", query.getVisType());
        }
        if (StringUtils.hasLength(query.getSuccess())) {
            queryWrapper.like("success", query.getSuccess());
        }
        if (query.getVisTime() != null && query.getVisTime().length == 2) {
            queryWrapper.ge("visTime", query.getVisTime()[0]);
            queryWrapper.le("visTime", query.getVisTime()[1]);
        }
        // #endregion

        Page<LogVis> page = pageHelper.initAllPageSetting(query, queryWrapper);
        Page<LogVis> result = logVisMapper.selectPage(page, queryWrapper);
        return pageMapperToPageResult(result);
    }

    /**
     * 清空表数据
     */
    @Override
    public void truncateTable() {
        logVisMapper.truncateTable();
    }

}
