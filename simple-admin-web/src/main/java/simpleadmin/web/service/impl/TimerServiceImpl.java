package simpleadmin.web.service.impl;

import org.springframework.stereotype.Service;
import simpleadmin.web.entity.Timer;
import simpleadmin.web.mapper.TimerMapper;
import simpleadmin.web.service.ITimerService;
import simpleadmin.web.service.impl.base.BaseServiceImpl;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
@Service
public class TimerServiceImpl extends BaseServiceImpl<TimerMapper, Timer> implements ITimerService {

}
