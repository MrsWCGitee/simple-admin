package simpleadmin.web.service.impl;

import org.springframework.stereotype.Service;
import simpleadmin.web.entity.LogAudit;
import simpleadmin.web.mapper.LogAuditMapper;
import simpleadmin.web.service.ILogAuditService;
import simpleadmin.web.service.impl.base.BaseServiceImpl;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
@Service
public class LogAuditServiceImpl extends BaseServiceImpl<LogAuditMapper, LogAudit> implements ILogAuditService {

}
