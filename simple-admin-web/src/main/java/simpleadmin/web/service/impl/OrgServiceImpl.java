package simpleadmin.web.service.impl;

import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNodeConfig;
import cn.hutool.core.lang.tree.TreeUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.entity.Org;
import simpleadmin.web.entity.query.OrgPageQuery;
import simpleadmin.web.mapper.OrgMapper;
import simpleadmin.web.service.IOrgService;
import simpleadmin.web.service.impl.base.BaseServiceImpl;

import java.util.List;

/**
 * <p>
 * 组织机构实现类
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-03
 */
@Service
public class OrgServiceImpl extends BaseServiceImpl<OrgMapper, Org> implements IOrgService {

    private final OrgMapper orgMapper;

    @Autowired
    public OrgServiceImpl(OrgMapper orgMapper) {
        this.orgMapper = orgMapper;
    }

    /**
     * 多条件分页查询
     *
     * @param query 分页参数
     * @return 结果
     */
    @Override
    public PageResult<Org> pageQuery(OrgPageQuery query) {
        QueryWrapper<Org> queryWrapper = new QueryWrapper<>();

        // #region 拼接查询条件
        if (StringUtils.hasLength(query.getName())) {
            queryWrapper.like("name", query.getName());
        }
        if (query.getOrgId() != null && query.getOrgId().length > 0) {
            if (query.getOrgId()[0] != 0) {
                String pid = String.format("[%d]", query.getOrgId()[0]);
                queryWrapper.like("pids", pid).or().eq("id", query.getOrgId()[0]);
            }
        }
        // #endregion

        Page<Org> page = pageHelper.initAllPageSetting(query, queryWrapper);
        Page<Org> result = orgMapper.selectPage(page, queryWrapper);
        return pageMapperToPageResult(result);
    }

    /**
     * 获取组织机构树
     *
     * @return 结果
     */
    @Override
    public List<Tree<Long>> getOrgTree() {
        // 1.配置
        TreeNodeConfig config = new TreeNodeConfig();
        // 自定义属性名
        // config.setWeightKey("order"); // 权重排序字段 默认为weight
        config.setIdKey("id"); // 默认为id可以不设置
        config.setNameKey("name"); // 节点名对应名称 默认为name
        config.setParentIdKey("pid"); // 父节点 默认为parentId
        config.setChildrenKey("children"); // 子点 默认为children
        // config.setDeep(3); // 可以配置递归深度 从0开始计算 默认此配置为空,即不限制

        // 2.数据源
        List<Org> orgs = this.list();

        // 3.转树，Tree<>里面泛型为id的类型
        return TreeUtil.build(orgs, 0L, config, (object, tree) -> {
            tree.setId(object.getId());
            tree.setParentId(object.getPid());
            tree.setName(object.getName());
        });
    }

}
