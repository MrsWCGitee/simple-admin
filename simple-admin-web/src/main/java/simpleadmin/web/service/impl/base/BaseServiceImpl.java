package simpleadmin.web.service.impl.base;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import simpleadmin.web.common.page.PageHelper;
import simpleadmin.web.common.page.PageParames;
import simpleadmin.web.common.page.PageResult;

/**
 * 封装一个基类的服务类，做一些公用的操作
 */
public class BaseServiceImpl<M extends com.baomidou.mybatisplus.core.mapper.BaseMapper<T>, T>
        extends ServiceImpl<M, T> {

    /**
     * 分页辅助类
     */
    protected PageHelper<T> pageHelper;

    @Autowired
    public void setPageHelper(PageHelper<T> pageHelper) {
        this.pageHelper = pageHelper;
    }

    /**
     * 将 Mybatis-plus 分页的结果 Page 转化为 自定义的 PageResult
     *
     * @param page 分页参数
     * @return 分页结果
     */
    protected PageResult<T> pageMapperToPageResult(Page<T> page) {
        return new PageResult<>(page.getRecords(), page.getTotal());
    }

    /**
     * 一次生成分页结果，（封装）
     *
     * @param mapper       实体 mapper
     * @param query        分页参数
     * @param queryWrapper 查询添加表达式
     * @return 分页结果
     */
    protected PageResult<T> generatePageResult(M mapper, PageParames query, QueryWrapper<T> queryWrapper) {
        Page<T> page = pageHelper.initAllPageSetting(query, queryWrapper);
        mapper.selectPage(page, queryWrapper);
        return new PageResult<>(page.getRecords(), page.getTotal());
    }

}
