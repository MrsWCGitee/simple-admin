package simpleadmin.web.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import simpleadmin.web.common.enums.NoticeUserStatus;
import simpleadmin.web.entity.NoticeUser;
import simpleadmin.web.mapper.NoticeUserMapper;
import simpleadmin.web.service.INoticeUserService;
import simpleadmin.web.service.impl.base.BaseServiceImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
@Service
public class NoticeUserServiceImpl extends BaseServiceImpl<NoticeUserMapper, NoticeUser> implements INoticeUserService {

    private final NoticeUserMapper noticeUserMapper;

    @Autowired
    public NoticeUserServiceImpl(NoticeUserMapper noticeUserMapper) {
        this.noticeUserMapper = noticeUserMapper;
    }

    public void add(Long noticeId, Long[] noticeUserIds, NoticeUserStatus status) {
        QueryWrapper<NoticeUser> queryWrapper = new QueryWrapper<>();

        // #region 拼接查询条件
        if (noticeId > 0) {
            queryWrapper.eq("noticeId", noticeId);
        }
        // #endregion

        List<NoticeUser> users = this.list(queryWrapper);

        if (users != null) {
            noticeUserMapper.delete(queryWrapper);
        }

        users = new ArrayList<>();
        for (Long userId : noticeUserIds) {
            NoticeUser user = new NoticeUser();
            user.setNoticeId(noticeId);
            user.setUserId(userId);
            user.setReadStatus(status.getValue());
            users.add(user);
        }
        this.saveBatch(users);
    }

}
