package simpleadmin.web.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import simpleadmin.web.common.enums.CommonStatus;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.entity.Role;
import simpleadmin.web.entity.RoleMenu;
import simpleadmin.web.entity.dto.role.GrantRoleMenuInput;
import simpleadmin.web.entity.dto.role.RoleOutput;
import simpleadmin.web.entity.query.RolePageQuery;
import simpleadmin.web.mapper.RoleMapper;
import simpleadmin.web.service.IRoleMenuService;
import simpleadmin.web.service.IRoleService;
import simpleadmin.web.service.impl.base.BaseServiceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-03
 */
@Service
public class RoleServiceImpl extends BaseServiceImpl<RoleMapper, Role> implements IRoleService {

    private final RoleMapper roleMapper;
    private final IRoleMenuService roleMenuService;

    @Autowired
    public RoleServiceImpl(RoleMapper roleMapper, IRoleMenuService roleMenuService) {
        this.roleMapper = roleMapper;
        this.roleMenuService = roleMenuService;
    }

    /**
     * 多条件分页查询
     *
     * @param query 分页参数
     * @return 结果
     */
    @Override
    public PageResult<Role> pageQuery(RolePageQuery query) {
        QueryWrapper<Role> queryWrapper = new QueryWrapper<>();

        // #region 拼接查询条件
        if (StringUtils.hasLength(query.getName())) {
            queryWrapper.like("name", query.getName());
        }
        if (StringUtils.hasLength(query.getCode())) {
            queryWrapper.like("code", query.getCode());
        }
        // #endregion

        queryWrapper.eq("status", CommonStatus.ENABLE);
        Page<Role> page = pageHelper.initAllPageSetting(query, queryWrapper);
        Page<Role> result = roleMapper.selectPage(page, queryWrapper);
        return pageMapperToPageResult(result);
    }

    @Override
    public List<RoleOutput> getUserRoleList(Long userId) {
        return roleMapper.getUserRoleList(userId);
    }

    /**
     * 角色拥有的菜单
     */
    @Override
    public List<Long> ownMenu(Long roleId) {
        QueryWrapper<RoleMenu> roleMenuWrapper = new QueryWrapper<>();
        roleMenuWrapper.eq("sysRoleId", roleId);
        List<RoleMenu> roleMenus = roleMenuService.list(roleMenuWrapper);
        return roleMenus.stream().map(RoleMenu::getSysMenuId).collect(Collectors.toList());
    }

    /**
     * 角色授权菜单
     *
     * @param input 请求参数
     */
    @Override
    public void grantMenu(GrantRoleMenuInput input) {

        QueryWrapper<RoleMenu> roleMenuWrapper = new QueryWrapper<>();
        roleMenuWrapper.eq("sysRoleId", input.getRoleId());
        roleMenuService.remove(roleMenuWrapper);

        ArrayList<RoleMenu> roleMenus = new ArrayList<>();
        for (Long grantMenuId : input.getGrantMenuIds()) {
            RoleMenu roleMenu = new RoleMenu();
            roleMenu.setSysRoleId(input.getRoleId());
            roleMenu.setSysMenuId(grantMenuId);
            roleMenus.add(roleMenu);
        }

        roleMenuService.saveBatch(roleMenus);
    }

}
