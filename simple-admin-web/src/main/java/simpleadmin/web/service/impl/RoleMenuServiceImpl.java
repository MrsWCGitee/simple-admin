package simpleadmin.web.service.impl;

import org.springframework.stereotype.Service;
import simpleadmin.web.entity.RoleMenu;
import simpleadmin.web.mapper.RoleMenuMapper;
import simpleadmin.web.service.IRoleMenuService;
import simpleadmin.web.service.impl.base.BaseServiceImpl;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
@Service
public class RoleMenuServiceImpl extends BaseServiceImpl<RoleMenuMapper, RoleMenu> implements IRoleMenuService {

}
