package simpleadmin.web.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.entity.LogEx;
import simpleadmin.web.entity.query.LogExPageQuery;
import simpleadmin.web.mapper.LogExMapper;
import simpleadmin.web.service.ILogExService;
import simpleadmin.web.service.impl.base.BaseServiceImpl;

/**
 * <p>
 * 异常日志 服务实现类
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
@Service
public class LogExServiceImpl extends BaseServiceImpl<LogExMapper, LogEx> implements ILogExService {

    private final LogExMapper logExMapper;

    @Autowired
    public LogExServiceImpl(LogExMapper logExMapper) {
        this.logExMapper = logExMapper;
    }

    /**
     * 多条件分页查询
     *
     * @param query 分页参数
     * @return 分页结果
     */
    @Override
    public PageResult<LogEx> pageQuery(LogExPageQuery query) {
        QueryWrapper<LogEx> queryWrapper = new QueryWrapper<>();

        // #region 拼接查询条件
        if (StringUtils.hasLength(query.getAccount())) {
            queryWrapper.like("account", query.getAccount());
        }
        if (StringUtils.hasLength(query.getClassName())) {
            queryWrapper.like("className", query.getClassName());
        }
        if (StringUtils.hasLength(query.getMethodName())) {
            queryWrapper.like("methodName", query.getMethodName());
        }
        if (StringUtils.hasLength(query.getExceptionMsg())) {
            queryWrapper.like("exceptionMsg", query.getExceptionMsg());
        }
        if (query.getExceptionTime() != null && query.getExceptionTime().length == 2) {
            queryWrapper.ge("exceptionTime", query.getExceptionTime()[0]);
            queryWrapper.le("exceptionTime", query.getExceptionTime()[1]);
        }
        // #endregion

        Page<LogEx> page = pageHelper.initAllPageSetting(query, queryWrapper);
        Page<LogEx> result = logExMapper.selectPage(page, queryWrapper);
        return pageMapperToPageResult(result);
    }

    /**
     * 清空表数据
     */
    @Override
    public void truncateTable() {
        logExMapper.truncateTable();
    }

}
