package simpleadmin.web.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import simpleadmin.web.common.enums.AdminType;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.common.tools.SecurityUtil;
import simpleadmin.web.entity.User;
import simpleadmin.web.entity.ViewNoticeReceive;
import simpleadmin.web.entity.query.NoticeReceivePageQuery;
import simpleadmin.web.mapper.ViewNoticeReceiveMapper;
import simpleadmin.web.service.IViewNoticeReceiveService;
import simpleadmin.web.service.impl.base.BaseServiceImpl;

/**
 * <p>
 * VIEW 服务实现类
 * </p>
 *
 * @author wangcheng
 * @since 2022-08-15
 */
@Service
public class ViewNoticeReceiveServiceImpl extends BaseServiceImpl<ViewNoticeReceiveMapper, ViewNoticeReceive>
        implements IViewNoticeReceiveService {

    private final ViewNoticeReceiveMapper noticeMapper;

    @Autowired
    public ViewNoticeReceiveServiceImpl(ViewNoticeReceiveMapper noticeMapper) {
        this.noticeMapper = noticeMapper;
    }

    /**
     * 多条件分页查询
     *
     * @param query 分页参数
     * @return 结果
     */
    @Override
    public PageResult<ViewNoticeReceive> pageQuery(NoticeReceivePageQuery query) {
        QueryWrapper<ViewNoticeReceive> queryWrapper = new QueryWrapper<>();

        // #region 拼接查询条件
        User user = SecurityUtil.getUserDetails();
        // 超級管理员可以查看所有
        if (user.getAdminType() != AdminType.SuperAdmin.getValue()) {
            Long userId = SecurityUtil.getUserId();
            queryWrapper.eq("userId", userId);
        }
        if (StringUtils.hasLength(query.getKeyword())) {
            queryWrapper.and(qw -> {
                qw.like("title", query.getKeyword()).or().like("content", query.getKeyword());
            });
        }
        if (query.getType() > 0) {
            queryWrapper.eq("type", query.getType());
        }
        if (query.getStatus() != null && query.getStatus().getValue() > 0) {
            queryWrapper.eq("status", query.getStatus());
        }
        // #endregion

        Page<ViewNoticeReceive> page = pageHelper.initAllPageSetting(query, queryWrapper);
        Page<ViewNoticeReceive> result = noticeMapper.selectPage(page, queryWrapper);
        return pageMapperToPageResult(result);
    }

}
