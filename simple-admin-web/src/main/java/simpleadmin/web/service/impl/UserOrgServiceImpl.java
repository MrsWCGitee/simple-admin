package simpleadmin.web.service.impl;

import org.springframework.stereotype.Service;
import simpleadmin.web.entity.UserOrg;
import simpleadmin.web.mapper.UserOrgMapper;
import simpleadmin.web.service.IUserOrgService;
import simpleadmin.web.service.impl.base.BaseServiceImpl;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
@Service
public class UserOrgServiceImpl extends BaseServiceImpl<UserOrgMapper, UserOrg> implements IUserOrgService {

}
