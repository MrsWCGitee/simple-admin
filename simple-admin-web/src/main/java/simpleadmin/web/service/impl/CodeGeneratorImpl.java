package simpleadmin.web.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.entity.ViewTable;
import simpleadmin.web.entity.query.CodeGeneratorTablePageQuery;
import simpleadmin.web.mapper.ViewTableMapper;
import simpleadmin.web.service.ICodeGeneratorService;
import simpleadmin.web.service.impl.base.BaseServiceImpl;

import java.util.List;
import java.util.Map;

/**
 * 代码生成器相关 - 服务实现
 */
@Service
public class CodeGeneratorImpl extends BaseServiceImpl<ViewTableMapper, ViewTable> implements ICodeGeneratorService {

    private final ViewTableMapper tableMapper;

    @Autowired
    public CodeGeneratorImpl(ViewTableMapper tableMapper) {
        this.tableMapper = tableMapper;
    }

    /**
     * 多条件分页查询
     *
     * @param query 分页参数
     * @return 结果
     */
    @Override
    public PageResult<ViewTable> pageQuery(CodeGeneratorTablePageQuery query) {
        QueryWrapper<ViewTable> queryWrapper = new QueryWrapper<>();

        // #region 拼接查询条件
        if (StringUtils.hasLength(query.getTableName())) {
            queryWrapper.like("tableName", query.getTableName());
        }
        if (StringUtils.hasLength(query.getTableSchema())) {
            queryWrapper.like("tableSchema", query.getTableSchema());
        }
        if (StringUtils.hasLength(query.getTableType())) {
            queryWrapper.eq("tableType", query.getTableType());
        }
        // 判断是否查询的是全部数据
        if (query.getItemsPerPage().equals(-1)) {
            int sumCount = tableMapper.selectCount(queryWrapper).intValue();
            query.setItemsPerPage(sumCount);
        }
        // #endregion

        Page<ViewTable> page = pageHelper.initAllPageSetting(query, queryWrapper);
        Page<ViewTable> result = tableMapper.selectPage(page, queryWrapper);
        return pageMapperToPageResult(result);
    }

    /**
     * 获取数据表结构
     *
     * @param tableName 表名
     * @return 详情
     */
    @Override
    public List<Map> getTableColumn(String tableName) {
        return tableMapper.getTableColumn(tableName);
    }
}
