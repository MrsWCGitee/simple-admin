package simpleadmin.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.entity.DictType;
import simpleadmin.web.entity.query.DictTypePageQuery;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-03
 */
public interface IDictTypeService extends IService<DictType> {

    PageResult<DictType> pageQuery(DictTypePageQuery query);

    DictType getDictTypeByCode(String code);

}
