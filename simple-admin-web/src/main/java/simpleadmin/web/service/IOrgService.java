package simpleadmin.web.service;

import cn.hutool.core.lang.tree.Tree;
import com.baomidou.mybatisplus.extension.service.IService;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.entity.Org;
import simpleadmin.web.entity.query.OrgPageQuery;

import java.util.List;

/**
 * <p>
 * 组织机构服务类
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-03
 */
public interface IOrgService extends IService<Org> {

    /**
     * 多条件分页查询
     *
     * @param query 分页参数
     * @return
     */
    PageResult<Org> pageQuery(OrgPageQuery query);

    /**
     * 获取组织机构树
     *
     * @return
     */
    List<Tree<Long>> getOrgTree();
}
