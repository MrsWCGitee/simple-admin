package simpleadmin.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.entity.ViewTable;
import simpleadmin.web.entity.query.CodeGeneratorTablePageQuery;

import java.util.List;
import java.util.Map;

/**
 * 代码生成器相关 - 服务定义
 */
public interface ICodeGeneratorService extends IService<ViewTable> {

    /**
     * 多条件分页查询
     *
     * @param query 分页参数
     * @return
     */
    PageResult<ViewTable> pageQuery(CodeGeneratorTablePageQuery query);

    /**
     * 获取数据表结构
     *
     * @param tableName 表名
     * @return
     */
    List<Map> getTableColumn(String tableName);

}
