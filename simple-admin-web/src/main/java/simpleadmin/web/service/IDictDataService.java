package simpleadmin.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.entity.DictData;
import simpleadmin.web.entity.query.DictDataPageQuery;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-03
 */
public interface IDictDataService extends IService<DictData> {

    PageResult<DictData> pageQuery(DictDataPageQuery query);

    List<DictData> getDictDataListByDictTypeId(Long typeId);

}
