package simpleadmin.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.entity.LogEx;
import simpleadmin.web.entity.query.LogExPageQuery;

/**
 * <p>
 * 异常日志 服务类
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
public interface ILogExService extends IService<LogEx> {

    /**
     * 多条件分页查询
     *
     * @param query 分页参数
     * @return
     */
    PageResult<LogEx> pageQuery(LogExPageQuery query);

    /**
     * 清空表数据
     */
    void truncateTable();

}
