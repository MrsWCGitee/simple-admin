package simpleadmin.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.entity.LogVis;
import simpleadmin.web.entity.query.LogVisPageQuery;

/**
 * <p>
 * 访问日志 服务类
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
public interface ILogVisService extends IService<LogVis> {

    /**
     * 多条件分页查询
     *
     * @param query 分页参数
     * @return
     */
    PageResult<LogVis> pageQuery(LogVisPageQuery query);

    /**
     * 清空表数据
     */
    void truncateTable();

}
