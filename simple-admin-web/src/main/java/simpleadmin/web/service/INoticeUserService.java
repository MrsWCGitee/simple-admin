package simpleadmin.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import simpleadmin.web.common.enums.NoticeUserStatus;
import simpleadmin.web.entity.NoticeUser;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
public interface INoticeUserService extends IService<NoticeUser> {

    void add(Long noticeId, Long[] noticeUserIds, NoticeUserStatus status);

}
