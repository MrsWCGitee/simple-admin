package simpleadmin.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.entity.Notice;
import simpleadmin.web.entity.dto.notice.AddNoticeInput;
import simpleadmin.web.entity.query.NoticePageQuery;

/**
 * <p>
 * 通知服务类
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
public interface INoticeService extends IService<Notice> {

    /**
     * 多条件分页查询
     *
     * @param query 分页参数
     * @return
     */
    PageResult<Notice> pageQuery(NoticePageQuery query);

    /**
     * 添加通知消息
     *
     * @param input
     */
    void add(AddNoticeInput input);

}
