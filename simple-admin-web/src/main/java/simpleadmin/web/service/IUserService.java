package simpleadmin.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.entity.User;
import simpleadmin.web.entity.dto.auth.LoginOutput;
import simpleadmin.web.entity.dto.user.AddOrEditUserInput;
import simpleadmin.web.entity.dto.user.GrantUserOrgInput;
import simpleadmin.web.entity.dto.user.GrantUserRoleInput;
import simpleadmin.web.entity.query.UserPageQuery;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 用户服务类
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-03
 */
public interface IUserService extends IService<User> {

    /**
     * 多条件分页查询
     *
     * @param query 分页参数
     * @return
     */
    PageResult<User> pageQuery(UserPageQuery query);

    /**
     * 添加或修改
     *
     * @param input
     */
    void addOrEdit(AddOrEditUserInput input);

    /**
     * 根据用户名查找用户
     *
     * @param username
     * @return
     */
    User findByUsername(String username);

    /**
     * 根绝 Id 获取 UserModel
     *
     * @param id
     * @return UserModel
     */
    User getById(Long id);

    /**
     * 获取用户的数据
     *
     * @return
     */
    List<User> getUserSelector(String name);

    /**
     * 查找用户的菜单权限标识集合
     *
     * @return
     */
    Set<String> findPermissions(Long userId,Integer adminType);

    /**
     * 获取登录用户信息
     *
     * @param userId
     * @return
     */
    LoginOutput getLoginUser(Long userId);

    /**
     * 用户授权的角色
     */
    List<Long> ownRole(Long userId);

    /**
     * 用户角色
     *
     * @param input
     */
    void grantRole(GrantUserRoleInput input);

    /**
     * 用户授权数据
     */
    List<Long> ownData(Long userId);

    /**
     * 用户数据
     *
     * @param input
     */
    void grantData(GrantUserOrgInput input);

    /**
     * 重置用户密码
     * @param userId
     */
    void resetPassword(Long userId,String newPassword);

}
