package simpleadmin.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.entity.LogOp;
import simpleadmin.web.entity.query.LogOpPageQuery;

/**
 * <p>
 * 操作日志 服务类
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
public interface ILogOpService extends IService<LogOp> {

    /**
     * 多条件分页查询
     *
     * @param query 分页参数
     * @return
     */
    PageResult<LogOp> pageQuery(LogOpPageQuery query);

    /**
     * 清空表数据
     */
    void truncateTable();

}
