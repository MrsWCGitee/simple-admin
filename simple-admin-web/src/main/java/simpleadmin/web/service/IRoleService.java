package simpleadmin.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.entity.Role;
import simpleadmin.web.entity.dto.role.GrantRoleMenuInput;
import simpleadmin.web.entity.dto.role.RoleOutput;
import simpleadmin.web.entity.query.RolePageQuery;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-03
 */
public interface IRoleService extends IService<Role> {

    PageResult<Role> pageQuery(RolePageQuery query);

    List<RoleOutput> getUserRoleList(Long userId);

    /**
     * 角色拥有的菜单
     */
    List<Long> ownMenu(Long roleId);

    /**
     * 角色授权菜单
     *
     * @param input
     */
    void grantMenu(GrantRoleMenuInput input);

}
