package simpleadmin.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.entity.OnlineUser;
import simpleadmin.web.entity.query.OnlineUserPageQuery;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
public interface IOnlineUserService extends IService<OnlineUser> {

    /**
     * 多条件分页查询
     *
     * @param query 分页参数
     * @return
     */
    PageResult<OnlineUser> pageQuery(OnlineUserPageQuery query);

    /**
     * 根据用户Id获取在线用户信息
     *
     * @param userId
     * @return
     */
    OnlineUser getByUserId(Long userId);

    /**
     * 根据用户Id获取在线用户信息
     *
     * @param userId
     * @return
     */
    List<OnlineUser> getListByUserId(Long userId);

}
