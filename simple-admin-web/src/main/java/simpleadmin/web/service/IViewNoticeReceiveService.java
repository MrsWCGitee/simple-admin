package simpleadmin.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.entity.ViewNoticeReceive;
import simpleadmin.web.entity.query.NoticeReceivePageQuery;

/**
 * <p>
 * VIEW 服务类
 * </p>
 *
 * @author wangcheng
 * @since 2022-08-15
 */
public interface IViewNoticeReceiveService extends IService<ViewNoticeReceive> {

    /**
     * 多条件分页查询
     *
     * @param query 分页参数
     * @return
     */
    PageResult<ViewNoticeReceive> pageQuery(NoticeReceivePageQuery query);

}
