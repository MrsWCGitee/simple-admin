package simpleadmin.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import simpleadmin.web.common.http.HttpResult;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.entity.Notice;
import simpleadmin.web.entity.ViewNoticeReceive;
import simpleadmin.web.entity.dto.notice.AddNoticeInput;
import simpleadmin.web.entity.query.NoticePageQuery;
import simpleadmin.web.entity.query.NoticeReceivePageQuery;
import simpleadmin.web.service.INoticeService;
import simpleadmin.web.service.IViewNoticeReceiveService;

import java.util.List;

/**
 * <p>
 * 通知控制器
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
@RestController
@RequestMapping("/api/notice")
public class NoticeController extends BaseController {

    private final INoticeService noticeService;
    private final IViewNoticeReceiveService noticeReceiveService;

    @Autowired
    public NoticeController(INoticeService noticeService, IViewNoticeReceiveService noticeReceiveService) {
        this.noticeService = noticeService;
        this.noticeReceiveService = noticeReceiveService;
    }

    /**
     * 分页查询
     */
    @PostMapping(value = "/page")
    public HttpResult page(@RequestBody NoticePageQuery query) {
        PageResult<Notice> result = noticeService.pageQuery(query);
        return HttpResult.ok(result);
    }

    /**
     * 添加 或 修改
     */
    @PostMapping(value = "/addOrEdit")
    public HttpResult addOrEdit(@RequestBody AddNoticeInput input) {
        noticeService.add(input);
        return HttpResult.ok(true);
    }

    /**
     * 根据 Id 获取详情
     *
     * @param id 数据 id
     * @return 结果
     */
    @GetMapping(value = "/detail")
    public HttpResult detail(Long id) {
        Notice result = noticeService.getById(id);
        return HttpResult.ok(result);
    }

    /**
     * 根据 Id 批量删除
     *
     * @param ids 数据 id 集合
     * @return 结果
     */
    @PostMapping(value = "/removeBatchByIds")
    public HttpResult removeBatchByIds(@RequestBody List<Long> ids) {
        Boolean result = noticeService.removeBatchByIds(ids);
        return HttpResult.ok(result);
    }

    /**
     * 已读通知公告的分页查询
     *
     * @param query 请求参数
     * @return 结果
     */
    @PostMapping(value = "/receivedPage")
    public HttpResult receivedPage(@RequestBody NoticeReceivePageQuery query) {
        PageResult<ViewNoticeReceive> result = noticeReceiveService.pageQuery(query);
        return HttpResult.ok(result);
    }

    /**
     * 根据 Id 获取已读通知详情
     *
     * @param id 数据 id
     * @return 结果
     */
    @GetMapping(value = "/receivedDetail")
    public HttpResult receivedDetail(Long id) {
        ViewNoticeReceive result = noticeReceiveService.getById(id);
        return HttpResult.ok(result);
    }

}
