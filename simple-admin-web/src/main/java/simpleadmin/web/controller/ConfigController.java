package simpleadmin.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import simpleadmin.web.common.http.HttpResult;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.entity.Config;
import simpleadmin.web.entity.Menu;
import simpleadmin.web.entity.query.ConfigPageQuery;
import simpleadmin.web.service.IConfigService;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
@RestController
@RequestMapping("/api/config")
public class ConfigController extends BaseController {

    private final IConfigService configService;

    @Autowired
    public ConfigController(IConfigService configService) {
        this.configService = configService;
    }

    /**
     * 分页查询
     */
    @PostMapping(value = "/page")
    public HttpResult page(@RequestBody ConfigPageQuery query) {
        PageResult<Config> result = configService.pageQuery(query);
        return HttpResult.ok(result);
    }

    /**
     * 添加 或 修改
     */
    @PostMapping(value = "/addOrEdit")
    public HttpResult addOrEdit(@RequestBody Config input) {
        // 临时这样处理随后需要进行优化
        input.setStatus(0);// 设置默认状态
        input.setIsDeleted(0);// 设置默认状态
        Boolean result = configService.saveOrUpdate(input);
        return HttpResult.ok(result);
    }

    /**
     * 根据 Id 获取详情
     *
     * @param id 数据 id
     * @return 结果
     */
    @GetMapping(value = "/detail")
    public HttpResult detail(Long id) {
        Config result = configService.getById(id);
        return HttpResult.ok(result);
    }

    /**
     * 根据 Id 批量删除
     *
     * @param ids 数据 id 集合
     * @return 结果
     */
    @PostMapping(value = "/removeBatchByIds")
    public HttpResult removeBatchByIds(@RequestBody List<Long> ids) {
        Boolean result = configService.removeBatchByIds(ids);
        return HttpResult.ok(result);
    }

}
