package simpleadmin.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import simpleadmin.web.common.http.HttpResult;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.entity.LogEx;
import simpleadmin.web.entity.LogOp;
import simpleadmin.web.entity.query.LogExPageQuery;
import simpleadmin.web.service.ILogExService;

import java.util.List;

/**
 * <p>
 * 异常日志
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
@RestController
@RequestMapping("/api/logEx")
public class LogExController extends BaseController {

    private final ILogExService logExService;

    @Autowired
    public LogExController(ILogExService logExService) {
        this.logExService = logExService;
    }

    /**
     * 分页查询
     */
    @PostMapping(value = "/page")
    public HttpResult page(@RequestBody LogExPageQuery query) {
        PageResult<LogEx> result = logExService.pageQuery(query);
        return HttpResult.ok(result);
    }

    /**
     * 清空所有日志
     *
     * @return 结果
     */
    @PostMapping(value = "/clearAll")
    public HttpResult clearAll() {
        logExService.truncateTable();
        return HttpResult.ok("已清空所有异常日志数据！");
    }

    /**
     * 根据 Id 批量删除
     *
     * @param ids 数据 id 集合
     * @return 结果
     */
    @PostMapping(value = "/removeBatchByIds")
    public HttpResult removeBatchByIds(@RequestBody List<Long> ids) {
        Boolean result = logExService.removeBatchByIds(ids);
        return HttpResult.ok(result);
    }

    /**
     * 根据 Id 获取详情
     *
     * @param id 数据 id
     * @return 结果
     */
    @GetMapping(value = "/detail")
    public HttpResult detail(Long id) {
        LogEx result = logExService.getById(id);
        return HttpResult.ok(result);
    }

}
