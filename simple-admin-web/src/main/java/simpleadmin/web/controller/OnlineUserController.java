package simpleadmin.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import simpleadmin.web.common.http.HttpResult;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.common.webScoket.WebSocketServer;
import simpleadmin.web.entity.OnlineUser;
import simpleadmin.web.entity.query.OnlineUserPageQuery;
import simpleadmin.web.service.IOnlineUserService;

import java.io.IOException;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
@RestController
@RequestMapping("/api/onlineUser")
public class OnlineUserController extends BaseController {

    private final WebSocketServer webSocketServer;
    private final IOnlineUserService onlineUserService;

    @Autowired
    public OnlineUserController(WebSocketServer webSocketServer, IOnlineUserService onlineUserService) {
        this.webSocketServer = webSocketServer;
        this.onlineUserService = onlineUserService;
    }

    /**
     * 分页查询
     */
    @PostMapping(value = "/page")
    public HttpResult page(@RequestBody OnlineUserPageQuery query) {
        PageResult<OnlineUser> result = onlineUserService.pageQuery(query);
        return HttpResult.ok(result);
    }

    /**
     * 强制离线
     *
     * @param ids 数据 id 集合
     * @return 结果
     */
    @PostMapping(value = "/forceExist")
    public HttpResult forceExist(@RequestBody List<Long> ids) throws IOException {
        webSocketServer.forceExistByConnectIds(ids);
        return HttpResult.ok("离线成功！");
    }

}
