package simpleadmin.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import simpleadmin.web.common.http.HttpResult;
import simpleadmin.web.entity.dto.email.EmailBaseInput;
import simpleadmin.web.entity.dto.email.EmailHtmlInput;
import simpleadmin.web.service.core.MailService;

import javax.mail.MessagingException;

/**
 * <p>
 * 异常日志
 * </p>
 *
 * @author wangcheng
 * @since 2022-07-08
 */
@RestController
@RequestMapping("/api/email")
public class EmailController extends BaseController {

    private final MailService mailService;

    @Autowired
    public EmailController(MailService mailService) {
        this.mailService = mailService;
    }

    /**
     * 发送简单邮件
     *
     * @param model 请求参数
     * @return 结果
     */
    @PostMapping(value = "/sendSimpleMail")
    public HttpResult sendSimpleMail(@RequestBody EmailBaseInput model) {
        mailService.sendSimpleMail(model.getToEmail(), model.getSubject(), model.getContent());
        return HttpResult.ok("邮件发送完成！");
    }

    /**
     * 发送 HTML 邮件
     *
     * @param model 请求参数
     * @return 响应结果
     * @throws MessagingException 异常消息
     */
    @PostMapping(value = "/sendHtmlMail")
    public HttpResult sendHtmlMail(@RequestBody EmailHtmlInput model) throws MessagingException {
        mailService.sendHtmlEmail(model.getToEmail(), model.getSubject(), model.getContent());
        return HttpResult.ok("邮件发送完成！");
    }

}
