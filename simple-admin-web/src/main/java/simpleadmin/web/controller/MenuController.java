package simpleadmin.web.controller;

import cn.hutool.core.lang.tree.Tree;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import simpleadmin.web.common.http.HttpResult;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.entity.Menu;
import simpleadmin.web.entity.Notice;
import simpleadmin.web.entity.query.MenuPageQuery;
import simpleadmin.web.service.IMenuService;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 通知控制器
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
@RestController
@RequestMapping("/api/menu")
public class MenuController extends BaseController {

    private final IMenuService menuService;

    @Autowired
    public MenuController(IMenuService menuService) {
        this.menuService = menuService;
    }

    /**
     * 分页查询
     */
    @PreAuthorize("hasAuthority('sysMenu:list')")
    @PostMapping(value = "/page")
    public HttpResult page(@RequestBody MenuPageQuery query) {
        PageResult<Menu> result = menuService.pageQuery(query);
        return HttpResult.ok(result);
    }

    /**
     * 添加 或 修改
     */
    @PreAuthorize("hasAnyAuthority('sysMenu:add','sysMenu:edit')")
    @PostMapping(value = "/addOrEdit")
    public HttpResult addOrEdit(@RequestBody Menu input) {
        if (input.getId() > 0L) {
            input.setUpdatedTime(LocalDateTime.now());
        } else {
            // 设置发布状态
            input.setStatus(1);
            input.setCreatedTime(LocalDateTime.now());
            input.setIsDeleted(0);
        }
        Boolean result = menuService.saveOrUpdate(input);
        return HttpResult.ok(result);
    }

    /**
     * 根据 Id 获取详情
     *
     * @param id 数据 id
     * @return 结果
     */
    @GetMapping(value = "/detail")
    public HttpResult detail(Long id) {
        Menu result = menuService.getById(id);
        return HttpResult.ok(result);
    }

    /**
     * 根据 Id 批量删除
     *
     * @param ids 数据 id 集合
     * @return 结果
     */
    @PreAuthorize("hasAuthority('sysMenu:delete')")
    @PostMapping(value = "/removeBatchByIds")
    public HttpResult removeBatchByIds(@RequestBody List<Long> ids) {
        Boolean result = menuService.removeBatchByIds(ids);
        return HttpResult.ok(result);
    }

    /**
     * 获取组织机构树
     *
     * @return 结果
     */
    @GetMapping(value = "/tree")
    @ApiOperation(value = "获取组织机构树")
    public HttpResult tree() {
        List<Tree<Long>> result = menuService.getMenuTree();
        return HttpResult.ok(result);
    }

}
