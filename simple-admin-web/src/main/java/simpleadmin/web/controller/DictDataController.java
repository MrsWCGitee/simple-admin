package simpleadmin.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import simpleadmin.web.common.http.HttpResult;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.entity.DictData;
import simpleadmin.web.entity.DictType;
import simpleadmin.web.entity.query.DictDataPageQuery;
import simpleadmin.web.service.IDictDataService;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
@RestController
@RequestMapping("/api/dictData")
public class DictDataController extends BaseController {

    private final IDictDataService dictDataService;

    @Autowired
    public DictDataController(IDictDataService dictDataService) {
        this.dictDataService = dictDataService;
    }

    /**
     * 分页查询
     */
    @PostMapping(value = "/page")
    public HttpResult page(@RequestBody DictDataPageQuery query) {
        PageResult<DictData> result = dictDataService.pageQuery(query);
        return HttpResult.ok(result);
    }

    /**
     * 添加 或 修改
     */
    @PostMapping(value = "/addOrEdit")
    public HttpResult addOrEdit(@RequestBody DictData input) {
        // 临时这样处理随后需要进行优化
        input.setStatus(0);// 设置默认状态
        input.setIsDeleted(0);// 设置默认状态
        Boolean result = dictDataService.saveOrUpdate(input);
        return HttpResult.ok(result);
    }

    /**
     * 根据 Id 获取详情
     *
     * @param id 数据 id
     * @return 结果
     */
    @GetMapping(value = "/detail")
    public HttpResult detail(Long id) {
        DictData result = dictDataService.getById(id);
        return HttpResult.ok(result);
    }

    /**
     * 根据 Id 批量删除
     *
     * @param ids 数据 id 集合
     * @return 结果
     */
    @PostMapping(value = "/removeBatchByIds")
    public HttpResult removeBatchByIds(@RequestBody List<Long> ids) {
        Boolean result = dictDataService.removeBatchByIds(ids);
        return HttpResult.ok(result);
    }

}
