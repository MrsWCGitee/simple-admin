package simpleadmin.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import simpleadmin.web.common.http.HttpResult;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.entity.DictData;
import simpleadmin.web.entity.Role;
import simpleadmin.web.entity.dto.role.GrantRoleMenuInput;
import simpleadmin.web.entity.query.RolePageQuery;
import simpleadmin.web.service.IRoleService;

import java.util.List;

/**
 * <p>
 * 角色控制器
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-03
 */
@RestController
@RequestMapping("/api/role")
public class RoleController extends BaseController {

    private final IRoleService roleService;

    @Autowired
    public RoleController(IRoleService roleService) {
        this.roleService = roleService;
    }

    /**
     * 分页查询
     */
    @PreAuthorize("hasAuthority('sysRole:page')")
    @PostMapping(value = "/page")
    public HttpResult page(@RequestBody RolePageQuery query) {
        PageResult<Role> result = roleService.pageQuery(query);
        return HttpResult.ok(result);
    }

    /**
     * 获取角色选择器数据
     */
    @PreAuthorize("hasAuthority('sysRole:dropDown')")
    @GetMapping(value = "/selector")
    public HttpResult getRoleSelector() {
        List<Role> result = roleService.list();
        return HttpResult.ok(result);
    }

    /**
     * 添加 或 修改
     */
    @PreAuthorize("hasAnyAuthority('sysRole:add','sysRole:edit')")
    @PostMapping(value = "/addOrEdit")
    public HttpResult addOrEdit(@RequestBody Role input) {
        // 临时这样处理随后需要进行优化
        input.setStatus(0);// 设置默认状态
        input.setIsDeleted(0);// 设置默认状态
        Boolean result = roleService.saveOrUpdate(input);
        return HttpResult.ok(result);
    }

    /**
     * 根据 Id 获取详情
     *
     * @param id 数据 id
     * @return 结果
     */
    @GetMapping(value = "/detail")
    public HttpResult detail(Long id) {
        Role result = roleService.getById(id);
        return HttpResult.ok(result);
    }

    /**
     * 根据 Id 批量删除
     *
     * @param ids 数据 id 集合
     * @return 结果
     */
    @PreAuthorize("hasAuthority('sysRole:delete')")
    @PostMapping(value = "/removeBatchByIds")
    public HttpResult removeBatchByIds(@RequestBody List<Long> ids) {
        Boolean result = roleService.removeBatchByIds(ids);
        return HttpResult.ok(result);
    }

    /**
     * 获取角色拥有菜单Id集合
     *
     * @return 结果
     */
    @PreAuthorize("hasAuthority('sysRole:ownMenu')")
    @GetMapping(value = "/ownMenu")
    public HttpResult ownMenu(Long roleId) {
        List<Long> menuIds = roleService.ownMenu(roleId);
        return HttpResult.ok(menuIds);
    }

    /**
     * 授权角色菜单
     *
     * @param input 请求参数
     * @return 结果
     */
    @PreAuthorize("hasAuthority('sysRole:grantMenu')")
    @PostMapping(value = "/grantMenu")
    public HttpResult grantMenu(@RequestBody GrantRoleMenuInput input) {
        roleService.grantMenu(input);
        return HttpResult.ok("授权成功");
    }

}
