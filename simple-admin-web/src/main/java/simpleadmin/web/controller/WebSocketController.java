package simpleadmin.web.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import simpleadmin.web.common.http.HttpResult;
import simpleadmin.web.common.webScoket.WebSocketServer;

import javax.annotation.Resource;

/**
 * <p>
 * 登录授权相关
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
@RestController
@RequestMapping("/api/socket")
public class WebSocketController extends BaseController {

    @Resource
    private WebSocketServer webSocket;

    @GetMapping(value = "/sendMessage")
    public HttpResult getLoginUser(String msg) {
        webSocket.sendOneMessage("1", msg);
        return HttpResult.ok("成功！");
    }

}
