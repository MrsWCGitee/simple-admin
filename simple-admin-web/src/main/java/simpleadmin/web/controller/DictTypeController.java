package simpleadmin.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import simpleadmin.web.common.http.HttpResult;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.entity.Config;
import simpleadmin.web.entity.DictData;
import simpleadmin.web.entity.DictType;
import simpleadmin.web.entity.map.DictDataVO;
import simpleadmin.web.entity.query.DictTypePageQuery;
import simpleadmin.web.mapstruct.DictDataCovert;
import simpleadmin.web.service.IDictDataService;
import simpleadmin.web.service.IDictTypeService;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-03
 */
@RestController
@RequestMapping("/api/dictType")
public class DictTypeController extends BaseController {

    private final IDictTypeService dictTypeService;
    private final IDictDataService dictDataService;
    private final DictDataCovert dictDataCovert;

    @Autowired
    public DictTypeController(IDictTypeService dictTypeService, IDictDataService dictDataService,
                              DictDataCovert dictDataCovert) {
        this.dictTypeService = dictTypeService;
        this.dictDataService = dictDataService;
        this.dictDataCovert = dictDataCovert;
    }

    /**
     * 分页查询
     */
    @PostMapping(value = "/page")
    public HttpResult page(@RequestBody DictTypePageQuery query) {
        PageResult<DictType> result = dictTypeService.pageQuery(query);
        return HttpResult.ok(result);
    }

    /**
     * 添加 或 修改
     */
    @PostMapping(value = "/addOrEdit")
    public HttpResult addOrEdit(@RequestBody DictType input) {
        // 临时这样处理随后需要进行优化
        input.setStatus(0);// 设置默认状态
        input.setIsDeleted(0);// 设置默认状态
        Boolean result = dictTypeService.saveOrUpdate(input);
        return HttpResult.ok(result);
    }

    /**
     * 根据 Id 获取详情
     *
     * @param id 数据 id
     * @return 结果
     */
    @GetMapping(value = "/detail")
    public HttpResult detail(Long id) {
        DictType result = dictTypeService.getById(id);
        return HttpResult.ok(result);
    }

    /**
     * 根据 Id 批量删除
     *
     * @param ids 数据 id 集合
     * @return 结果
     */
    @PostMapping(value = "/removeBatchByIds")
    public HttpResult removeBatchByIds(@RequestBody List<Long> ids) {
        Boolean result = dictTypeService.removeBatchByIds(ids);
        return HttpResult.ok(result);
    }

    /**
     * 获取字典类型下所有字典值
     *
     * @param code 字典类型代码
     */
    @GetMapping(value = "/dropDown")
    public HttpResult dropDown(String code) {
        DictType dictType = dictTypeService.getDictTypeByCode(code);
        if (dictType == null) {
            return HttpResult.error("字典类型不存在！");
        }
        List<DictData> dictDatas = dictDataService.getDictDataListByDictTypeId(dictType.getId());
        List<DictDataVO> result = dictDataCovert.toConvertVOs(dictDatas);
        return HttpResult.ok(result);
    }
}
