package simpleadmin.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import simpleadmin.web.common.http.HttpResult;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.entity.Org;
import simpleadmin.web.entity.User;
import simpleadmin.web.entity.dto.user.AddOrEditUserInput;
import simpleadmin.web.entity.dto.user.GrantUserOrgInput;
import simpleadmin.web.entity.dto.user.GrantUserRoleInput;
import simpleadmin.web.entity.dto.user.ResetPasswordInput;
import simpleadmin.web.entity.query.UserPageQuery;
import simpleadmin.web.service.IUserService;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 用户控制器
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-03
 */
@RestController
@RequestMapping("/api/user")
public class UserController extends BaseController {

    private final IUserService userService;

    @Autowired
    public UserController(IUserService userService) {
        this.userService = userService;
    }

    /**
     * 分页查询
     *
     * @param query 查询条件
     * @return 结果
     */
    @PreAuthorize("hasAuthority('sysUser:page')")
    @PostMapping(value = "/page")
    public HttpResult page(@RequestBody UserPageQuery query) {
        PageResult<User> result = userService.pageQuery(query);
        return HttpResult.ok(result);
    }

    /**
     * 添加 或 修改
     */
    @PreAuthorize("hasAnyAuthority('sysUser:add','sysUser:edit')")
    @PostMapping(value = "/addOrEdit")
    public HttpResult addOrEdit(@RequestBody AddOrEditUserInput input) {
        userService.addOrEdit(input);
        return HttpResult.ok("保存成功！");
    }

    /**
     * 根据 Id 获取详情
     *
     * @param id 数据 id
     * @return 结果
     */
    @GetMapping(value = "/detail")
    public HttpResult detail(Long id) {
        User result = userService.getById(id);
        return HttpResult.ok(result);
    }

    /**
     * 根据 Id 批量删除
     *
     * @param ids 数据 id 集合
     * @return 结果
     */
    @PreAuthorize("hasAuthority('sysUser:delete')")
    @PostMapping(value = "/removeBatchByIds")
    public HttpResult removeBatchByIds(@RequestBody List<Long> ids) {
        Boolean result = userService.removeBatchByIds(ids);
        return HttpResult.ok(result);
    }

    /**
     * 根据 Id 获取用户详情
     * @param id
     * @return
     */
    @PreAuthorize("hasAuthority('sysUser:detail')")
    @GetMapping(value = "/getById")
    public HttpResult getById(Long id) {
        User result = userService.getById(id);
        return HttpResult.ok(result);
    }

    /**
     * 获取用户拥有角色Id集合
     *
     * @return 结果
     */
    @PreAuthorize("hasAuthority('sysUser:ownRole')")
    @GetMapping(value = "/ownRole")
    public HttpResult ownRole(Long userId) {
        List<Long> roleIds = userService.ownRole(userId);
        return HttpResult.ok(roleIds);
    }

    /**
     * 用户角色
     *
     * @param input 输入参数
     * @return 结果
     */
    @PreAuthorize("hasAuthority('sysUser:grantRole')")
    @PostMapping(value = "/grantRole")
    public HttpResult grantRole(@RequestBody GrantUserRoleInput input) {
        userService.grantRole(input);
        return HttpResult.ok("授权成功");
    }

    /**
     * 获取用户数据集合
     *
     * @return 结果
     */
    @PreAuthorize("hasAuthority('sysUser:ownData')")
    @GetMapping(value = "/ownData")
    public HttpResult ownData(Long userId) {
        List<Long> dataIds = userService.ownData(userId);
        return HttpResult.ok(dataIds);
    }

    /**
     * 用户数据
     *
     * @param input 输入参数
     * @return 结果
     */
    @PreAuthorize("hasAuthority('sysUser:grantData')")
    @PostMapping(value = "/grantData")
    public HttpResult grantData(@RequestBody GrantUserOrgInput input) {
        userService.grantData(input);
        return HttpResult.ok("授权成功");
    }

    /**
     * 获取用户选择器数据
     */
    @PreAuthorize("hasAuthority('sysUser:selector')")
    @GetMapping(value = "/selector")
    public HttpResult getUserSelector(String name) {
        List<User> result = userService.getUserSelector(name);
        return HttpResult.ok(result);
    }

    /**
     * 重置用户密码
     *
     * @param input 重置密码实体
     * @return
     */
    //@PreAuthorize("hasAuthority('sysUser:grantData')")
    @PostMapping(value = "/resetPassword")
    public HttpResult resetPassword(@RequestBody @Valid ResetPasswordInput input) {
        userService.resetPassword(input.getUserId(), input.getNewPassword());
        return HttpResult.ok("授权成功");
    }

}
