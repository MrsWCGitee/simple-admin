package simpleadmin.web.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import simpleadmin.web.common.http.HttpResult;
import simpleadmin.web.entity.dto.machine.ServerOutput;

/**
 * <p>
 * 服务监控相关
 * </p>
 *
 * @author wangcheng
 * @since 2022-08-05
 */
@RestController
@RequestMapping("/api/machine")
public class MachineController extends BaseController {

    /**
     * 获取系统信息
     *
     * @return
     */
    @GetMapping(value = "/getAllSysInfo")
    public HttpResult getAllSysInfo() {
        ServerOutput result = new ServerOutput();
        try {
            result.copyTo(); //就这个代码有用
        } catch (Exception e) {
            e.printStackTrace();
        }
        return HttpResult.ok(result);
    }

}
