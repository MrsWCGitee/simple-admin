package simpleadmin.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import simpleadmin.web.common.http.HttpResult;
import simpleadmin.web.entity.LogAudit;
import simpleadmin.web.service.ILogAuditService;

/**
 * <p>
 * 日志
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
@RestController
@RequestMapping("/api/logAudit")
public class LogAuditController extends BaseController {

    private final ILogAuditService logAuditService;

    @Autowired
    public LogAuditController(ILogAuditService logAuditService) {
        this.logAuditService = logAuditService;
    }

    @GetMapping(value = "/getById")
    public HttpResult getById(Long id) {
        LogAudit result = logAuditService.getById(id);
        return HttpResult.ok(result);
    }

}
