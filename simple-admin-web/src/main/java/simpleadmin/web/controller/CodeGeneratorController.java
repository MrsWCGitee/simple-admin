package simpleadmin.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import simpleadmin.web.common.http.HttpResult;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.common.tools.MybatisPlusGeneratorUtil;
import simpleadmin.web.entity.ViewTable;
import simpleadmin.web.entity.query.CodeGeneratorTablePageQuery;
import simpleadmin.web.service.ICodeGeneratorService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-03
 */
@RestController
@RequestMapping("/api/codeGenerator")
public class CodeGeneratorController extends BaseController {

    private final ICodeGeneratorService codeGeneratorService;

    @Autowired
    public CodeGeneratorController(ICodeGeneratorService codeGeneratorService) {
        this.codeGeneratorService = codeGeneratorService;
    }

    /**
     * 分页查询数据库表信息
     */
    //@PreAuthorize("hasAuthority('sysMenu:list')")
    @PostMapping(value = "/page")
    public HttpResult page(@RequestBody CodeGeneratorTablePageQuery query) {
        PageResult<ViewTable> result = codeGeneratorService.pageQuery(query);
        return HttpResult.ok(result);
    }

    /**
     * 获取数据表结构
     *
     * @param tableName 表名
     * @return
     */
    @GetMapping(value = "/getTableColumn")
    public HttpResult getTableColumn(String tableName) {
        List<Map> result = codeGeneratorService.getTableColumn(tableName);
        return HttpResult.ok(result);
    }

    /**
     * 代码生成
     *
     * @return
     */
    @GetMapping(value = "/fast")
    public HttpResult fast() {
        MybatisPlusGeneratorUtil.CodeFastCreate();
        return HttpResult.ok("代码生成成功！");
    }



}
