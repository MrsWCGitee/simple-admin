package simpleadmin.web.controller;

import cn.hutool.core.lang.tree.Tree;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import simpleadmin.web.common.http.HttpResult;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.entity.Org;
import simpleadmin.web.entity.Role;
import simpleadmin.web.entity.query.OrgPageQuery;
import simpleadmin.web.service.IOrgService;

import java.util.List;

/**
 * <p>
 * 组织机构控制器
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-03
 */
@RestController
@RequestMapping("/api/org")
public class OrgController extends BaseController {

    private final IOrgService orgService;

    @Autowired
    public OrgController(IOrgService orgService) {
        this.orgService = orgService;
    }

    /**
     * 分页查询
     */
    @PreAuthorize("hasAuthority('sysOrg:page')")
    @PostMapping(value = "/page")
    public HttpResult page(@RequestBody OrgPageQuery query) {
        PageResult<Org> result = orgService.pageQuery(query);
        return HttpResult.ok(result);
    }

    /**
     * 添加 或 修改
     */
    @PreAuthorize("hasAnyAuthority('sysOrg:add','sysOrg:edit')")
    @PostMapping(value = "/addOrEdit")
    public HttpResult addOrEdit(@RequestBody Org input) {
        // 临时这样处理随后需要进行优化
        input.setStatus(0);// 设置默认状态
        input.setIsDeleted(0);// 设置默认状态
        // 这里还需要根据 pid ，设置 pids 不然会有问题
        Boolean result = orgService.saveOrUpdate(input);
        return HttpResult.ok(result);
    }

    /**
     * 根据 Id 获取详情
     *
     * @param id 数据 id
     * @return 结果
     */
    @GetMapping(value = "/detail")
    public HttpResult detail(Long id) {
        Org result = orgService.getById(id);
        return HttpResult.ok(result);
    }

    /**
     * 根据 Id 批量删除
     *
     * @param ids 数据 id 集合
     * @return 结果
     */
    @PreAuthorize("hasAuthority('sysOrg:delete')")
    @PostMapping(value = "/removeBatchByIds")
    public HttpResult removeBatchByIds(@RequestBody List<Long> ids) {
        Boolean result = orgService.removeBatchByIds(ids);
        return HttpResult.ok(result);
    }

    /**
     * 获取组织机构树
     *
     * @return 结果
     */
    @GetMapping(value = "/tree")
    @ApiOperation(value = "获取组织机构树")
    public HttpResult tree() {
        List<Tree<Long>> result = orgService.getOrgTree();
        return HttpResult.ok(result);
    }

}
