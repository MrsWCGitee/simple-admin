package simpleadmin.web.controller;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.LineCaptcha;
import cn.hutool.core.util.IdUtil;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import simpleadmin.web.common.http.HttpResult;
import simpleadmin.web.common.tools.JwtUtil;
import simpleadmin.web.common.tools.SecurityUtil;
import simpleadmin.web.common.tools.redis.RedisService;
import simpleadmin.web.entity.dto.auth.CaptchaOutput;
import simpleadmin.web.entity.dto.auth.LoginOutput;
import simpleadmin.web.entity.dto.auth.RefreshTokenInput;
import simpleadmin.web.entity.dto.auth.RefreshTokenOutput;
import simpleadmin.web.entity.dto.user.ResetPasswordInput;
import simpleadmin.web.service.IUserService;
import simpleadmin.web.service.core.AuthService;

import javax.validation.Valid;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 登录授权相关
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
@RestController
@RequestMapping("/api/auth")
public class AuthController extends BaseController {

    private final IUserService userService;
    private final RedisService redisService;
    private final AuthService authService;
    private JwtUtil jwtUtil;

    @Autowired
    public AuthController(IUserService userService, RedisService redisService, AuthService authService, JwtUtil jwtUtil) {
        this.userService = userService;
        this.redisService = redisService;
        this.authService = authService;
        this.jwtUtil = jwtUtil;
    }

    /**
     * 获取当前登录用户信息
     */
    @ApiOperation(value = "获取当前登录用户信息")
    @GetMapping(value = "/getLoginUser")
    public HttpResult getLoginUser() {
        Long userId = SecurityUtil.getUserId();
        LoginOutput result = userService.getLoginUser(userId);
        return HttpResult.ok(result);
    }

    /**
     * 刷新 token
     *
     * @param input 刷新 token 输入实体
     * @return
     */
    @ApiOperation(value = "刷新 token")
    @PostMapping(value = "/refreshToken")
    public HttpResult refreshToken(@RequestBody RefreshTokenInput input) {
        RefreshTokenOutput result = authService.refreshToken(input.getToken(), input.getRefreshToken());
        return HttpResult.ok(result);
    }

    /**
     * 重置用户密码
     *
     * @param input 重置密码实体
     * @return
     */
    @PostMapping(value = "/resetPassword")
    public HttpResult resetPassword(@RequestBody @Valid ResetPasswordInput input) {
        userService.resetPassword(input.getUserId(), input.getNewPassword());
        return HttpResult.ok("授权成功");
    }


    /**
     * 获取验证码
     *
     * @param captchaCodeKey 服务端存储验证码的key（当不为空时说明是刷新验证码）
     * @return 结果
     */
    @ApiOperation(value = "获取验证码")
    @GetMapping(value = "/getCaptcha")
    public HttpResult getCaptcha(String captchaCodeKey) {
        // captchaCodeKey 不为空，说明是刷新验证码
        if (StringUtils.hasLength(captchaCodeKey)) {
            String code = redisService.get(captchaCodeKey);
            // 判断验证码是否存在或者过期
            if (StringUtils.hasLength(code)) {
                redisService.delete(captchaCodeKey);
            }
        }
        LineCaptcha lineCaptcha = CaptchaUtil.createLineCaptcha(200, 40, 4, 6);
        String code = lineCaptcha.getCode();
        captchaCodeKey = "CaptchaCode_" + IdUtil.randomUUID();
        redisService.add(captchaCodeKey, code, 15L, TimeUnit.MINUTES);
        String img = lineCaptcha.getImageBase64();
        CaptchaOutput captcha = new CaptchaOutput(captchaCodeKey, "data:image/png;base64," + img);
        return HttpResult.ok(captcha);
    }

}
