package simpleadmin.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import simpleadmin.web.common.http.HttpResult;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.entity.LogOp;
import simpleadmin.web.entity.Notice;
import simpleadmin.web.entity.query.LogOpPageQuery;
import simpleadmin.web.service.ILogOpService;

import java.util.List;

/**
 * <p>
 * 操作日志
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
@RestController
@RequestMapping("/api/logOp")
public class LogOpController extends BaseController {

    private final ILogOpService logOpService;

    @Autowired
    public LogOpController(ILogOpService logOpService) {
        this.logOpService = logOpService;
    }

    /**
     * 分页查询
     */
    @PreAuthorize("hasAuthority('sysOpLog:page')")
    @PostMapping(value = "/page")
    public HttpResult page(@RequestBody LogOpPageQuery query) {
        PageResult<LogOp> result = logOpService.pageQuery(query);
        return HttpResult.ok(result);
    }

    /**
     * 清空所有日志
     *
     * @return 结果
     */
    @PreAuthorize("hasAuthority('sysOpLog:delete')")
    @PostMapping(value = "/clearAll")
    public HttpResult clearAll() {
        logOpService.truncateTable();
        return HttpResult.ok("已清空所有操作日志数据！");
    }

    /**
     * 根据 Id 批量删除
     *
     * @param ids 数据 id 集合
     * @return 结果
     */
    @PreAuthorize("hasAuthority('sysOpLog:delete')")
    @PostMapping(value = "/removeBatchByIds")
    public HttpResult removeBatchByIds(@RequestBody List<Long> ids) {
        Boolean result = logOpService.removeBatchByIds(ids);
        return HttpResult.ok(result);
    }

    /**
     * 根据 Id 获取详情
     *
     * @param id 数据 id
     * @return 结果
     */
    @GetMapping(value = "/detail")
    public HttpResult detail(Long id) {
        LogOp result = logOpService.getById(id);
        return HttpResult.ok(result);
    }

}
