package simpleadmin.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import simpleadmin.web.common.http.HttpResult;
import simpleadmin.web.service.INoticeUserService;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
@RestController
@RequestMapping("/api/noticeUser")
public class NoticeUserController extends BaseController {

    private final INoticeUserService noticeUserService;

    @Autowired
    public NoticeUserController(INoticeUserService noticeUserService) {
        this.noticeUserService = noticeUserService;
    }

    /**
     * 根据 Id 批量删除
     *
     * @param ids 数据 id 集合
     * @return 结果
     */
    @PostMapping(value = "/removeBatchByIds")
    public HttpResult removeBatchByIds(@RequestBody List<Long> ids) {
        Boolean result = noticeUserService.removeBatchByIds(ids);
        return HttpResult.ok(result);
    }

}
