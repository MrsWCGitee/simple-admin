package simpleadmin.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import simpleadmin.web.common.http.HttpResult;
import simpleadmin.web.common.page.PageResult;
import simpleadmin.web.entity.LogEx;
import simpleadmin.web.entity.LogVis;
import simpleadmin.web.entity.query.LogVisPageQuery;
import simpleadmin.web.service.ILogVisService;

import java.util.List;

/**
 * <p>
 * 访问日志
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
@RestController
@RequestMapping("/api/logVis")
public class LogVisController extends BaseController {

    private final ILogVisService logVisService;

    @Autowired
    public LogVisController(ILogVisService logVisService) {
        this.logVisService = logVisService;
    }

    /**
     * 分页查询
     */
    @PreAuthorize("hasAuthority('sysVisLog:page')")
    @PostMapping(value = "/page")
    public HttpResult page(@RequestBody LogVisPageQuery query) {
        PageResult<LogVis> result = logVisService.pageQuery(query);
        return HttpResult.ok(result);
    }

    /**
     * 清空所有日志
     *
     * @return 结果
     */
    @PreAuthorize("hasAuthority('sysVisLog:delete')")
    @PostMapping(value = "/clearAll")
    public HttpResult clearAll() {
        // 暂时有问题，需要调试看看具体原因
        logVisService.truncateTable();
        return HttpResult.ok("已清空所有访问日志数据！");
    }

    /**
     * 根据 Id 批量删除
     *
     * @param ids 数据 id 集合
     * @return 结果
     */
    @PreAuthorize("hasAuthority('sysVisLog:delete')")
    @PostMapping(value = "/removeBatchByIds")
    public HttpResult removeBatchByIds(@RequestBody List<Long> ids) {
        Boolean result = logVisService.removeBatchByIds(ids);
        return HttpResult.ok(result);
    }

    /**
     * 根据 Id 获取详情
     *
     * @param id 数据 id
     * @return 结果
     */
    @GetMapping(value = "/detail")
    public HttpResult detail(Long id) {
        LogVis result = logVisService.getById(id);
        return HttpResult.ok(result);
    }

}
