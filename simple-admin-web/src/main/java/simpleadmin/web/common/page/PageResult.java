package simpleadmin.web.common.page;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * 分页查询结果
 */
@Data
public class PageResult<T> {

    /**
     * 分页查询结果
     */
    @JsonProperty(value = "records")
    private List<T> records;
    /**
     * 数据总条数
     */
    @JsonProperty(value = "total")
    private Long total;

    public PageResult() {
    }

    /**
     * 初始化分页查询结果
     *
     * @param records 分页查询结果
     * @param total   数据总条数
     */
    public PageResult(List<T> records, Long total) {
        this.records = records;
        this.total = total;
    }

}
