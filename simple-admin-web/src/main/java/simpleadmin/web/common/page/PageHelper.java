package simpleadmin.web.common.page;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Component;

@Component
public class PageHelper<T> {

    /**
     * 设置排序
     *
     * @param query        分页参数
     * @param queryWrapper 查询添加表达式
     */
    public void setOrders(PageParames query, QueryWrapper<T> queryWrapper) {
        String[] sortBys = query.getSortBy();
        Boolean[] sortDescs = query.getSortDesc();
        if (sortBys == null || sortBys.length == 0 || sortDescs == null || sortDescs.length == 0) {
            return;
        }
        int length = sortBys.length < sortDescs.length ? sortBys.length : sortDescs.length;
        for (int i = 0; i < length; i++) {
            // 是否降序
            if (sortDescs[i]) {
                queryWrapper.orderByDesc(sortBys[i]);
            } else {
                queryWrapper.orderByAsc(sortBys[i]);
            }
        }
    }

    /**
     * 初始化分页实体
     *
     * @param query 分页参数
     * @return
     */
    public Page<T> initPageSetting(PageParames query) {
        Page<T> page = new Page<T>(query.getPage(), query.getItemsPerPage());
        return page;
    }

    /**
     * 初始化所有配置（就是把 setOrders 和 initPageSetting 一起执行）
     *
     * @param query        分页参数
     * @param queryWrapper 查询添加表达式
     * @return
     */
    public Page<T> initAllPageSetting(PageParames query, QueryWrapper<T> queryWrapper) {
        setOrders(query, queryWrapper);
        Page<T> page = initPageSetting(query);
        return page;
    }

}
