package simpleadmin.web.common.page;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 分 页 参 数 封 装
 */
@Data
public class PageParames {

    /**
     * 当前页
     */
    @JsonProperty(value = "page")
    private Integer page;

    /**
     * 每页数量
     */
    @JsonProperty(value = "itemsPerPage")
    private Integer itemsPerPage;

    /**
     * 指定那列排序
     */
    @JsonProperty(value = "sortBy")
    private String[] sortBy;

    /**
     * 对应的列排序是否降序,默认false
     */
    @JsonProperty(value = "sortDesc")
    private Boolean[] sortDesc;

    /**
     * 获取开始的数据行
     */
    public Integer start() {
        return (this.page - 1) * this.itemsPerPage;
    }

    /**
     * 获取结束的数据行
     */
    public Integer end() {
        return this.page * this.itemsPerPage;
    }

}
