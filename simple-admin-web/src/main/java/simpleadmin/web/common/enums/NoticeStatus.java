package simpleadmin.web.common.enums;

/**
 * 通知公告状态
 */
public enum NoticeStatus {

    /**
     * 草稿
     */
    DRAFT(0, "草稿"),

    /**
     * 发布
     */
    PUBLIC(1, "发布"),

    /**
     * 撤回
     */
    CANCEL(2, "撤回"),

    /**
     * 删除
     */
    DELETED(3, "删除");

    private final Integer value;
    private final String name;

    NoticeStatus(Integer value, String name) {
        this.value = value;
        this.name = name;
    }

    public Integer getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

}
