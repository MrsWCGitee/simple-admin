package simpleadmin.web.common.enums;

/**
 * 通知公告用户状态
 */
public enum NoticeUserStatus {

    /**
     * 未读
     */
    UNREAD(0, "未读"),

    /**
     * 发布
     */
    READ(1, "已读");

    private final Integer value;
    private final String name;

    NoticeUserStatus(Integer value, String name) {
        this.value = value;
        this.name = name;
    }

    public Integer getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

}
