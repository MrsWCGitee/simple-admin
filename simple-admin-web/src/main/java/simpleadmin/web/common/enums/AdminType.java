package simpleadmin.web.common.enums;

/**
 * 管理员类型
 */
public enum AdminType {

    /**
     * 超级管理员
     */
    SuperAdmin(1, "超级管理员"),

    /**
     * 管理员
     */
    Admin(2, "管理员"),

    /**
     * 普通账号
     */
    None(3, "普通账号");

    private final Integer value;
    private final String name;

    AdminType(Integer value, String name) {
        this.value = value;
        this.name = name;
    }

    public Integer getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

}
