package simpleadmin.web.common.enums;

/**
 * 系统菜单类型
 */
public enum MenuType {

    /**
     * 目录
     */
    Dir(0, "目录"),

    /**
     * 菜单
     */
    Menu(1, "菜单"),

    /**
     * 按钮
     */
    Btn(2, "按钮");

    private final Integer value;
    private final String name;

    MenuType(Integer value, String name) {
        this.value = value;
        this.name = name;
    }

    public Integer getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

}
