package simpleadmin.web.common.enums;

/**
 * 菜单激活类型
 */
public enum YesOrNot {

    /**
     * 是
     */
    Y(0, "Y"),

    /**
     * 否
     */
    N(1, "N");

    private final Integer value;
    private final String name;

    YesOrNot(Integer value, String name) {
        this.value = value;
        this.name = name;
    }

    public Integer getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

}
