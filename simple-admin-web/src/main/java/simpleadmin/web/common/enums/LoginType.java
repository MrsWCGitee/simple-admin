package simpleadmin.web.common.enums;

/**
 * 登陆类型
 */
public enum LoginType {

    /**
     * 登录
     */
    LOGIN(1, "登录"),

    /**
     * 登出
     */
    LOGOUT(2, "登出"),

    /**
     * 注册
     */
    REGISTER(3, "注册"),

    /**
     * 修改密码
     */
    CHANGEPASSWORD(4, "修改密码"),

    /**
     * 第三方授权登陆
     */
    AUTHORIZEDLOGIN(5, "第三方授权登陆");

    private final Integer value;
    private final String name;

    LoginType(Integer value, String name) {
        this.value = value;
        this.name = name;
    }

    public Integer getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

}
