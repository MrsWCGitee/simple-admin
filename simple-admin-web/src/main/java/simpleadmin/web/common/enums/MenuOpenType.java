package simpleadmin.web.common.enums;

/**
 * 系统菜单打开类型
 */
public enum MenuOpenType {

    /**
     * 无
     */
    None(0, "无"),

    /**
     * 组件
     */
    Component(1, "组件"),

    /**
     * 内链
     */
    Inner(2, "内链"),

    /**
     * 外链
     */
    Outer(3, "外链");

    private final Integer value;
    private final String name;

    MenuOpenType(Integer value, String name) {
        this.value = value;
        this.name = name;
    }

    public Integer getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

}
