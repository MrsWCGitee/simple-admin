package simpleadmin.web.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseBuilder;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.*;

/*
 * Swagger 配置
 */
@EnableOpenApi
@Configuration
public class SwaggerConfig implements WebMvcConfigurer {

    @Bean
    public Docket createRestApi() {
        Docket docket = new Docket(DocumentationType.OAS_30)
                .apiInfo(this.apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("simpleadmin.web.controller"))
                .paths(PathSelectors.any())
                .build()
                .globalResponses(HttpMethod.GET, getGlobalResonseMessage())
                .globalResponses(HttpMethod.POST, getGlobalResonseMessage())
                .protocols(new LinkedHashSet<>(Arrays.asList("HTTPS", "HTTP")))
                .securitySchemes(securitySchemes())// 授权信息设置，必要的header token等认证信息
                .securityContexts(securityContexts());// 授权信息全局应用
        return docket;
    }

    /**
     * 设置授权信息
     *
     * @return
     */
    private List<SecurityScheme> securitySchemes() {
        ArrayList<SecurityScheme> securitySchemes = new ArrayList<SecurityScheme>();
        String header = io.swagger.v3.oas.models.security.SecurityScheme.In.HEADER.toString();
        securitySchemes.add(new ApiKey("Authorization", "access-token", header));
        securitySchemes.add(new ApiKey("X-Authorization", "x-access-token", header));
        return securitySchemes;
    }

    /**
     * 授权信息全局应用
     *
     * @return
     */
    private List<SecurityContext> securityContexts() {
        ArrayList<SecurityReference> securityReferences = new ArrayList<SecurityReference>();
        securityReferences.add(new SecurityReference("Authorization",
                new AuthorizationScope[]{new AuthorizationScope("global", "")}));
        securityReferences.add(new SecurityReference("X-Authorization2",
                new AuthorizationScope[]{new AuthorizationScope("global", "")}));
        return Collections.singletonList(SecurityContext.builder()
                .securityReferences(securityReferences)
                .build());
    }

    /**
     * 生成通用响应信息
     *
     * @return
     */
    private List<Response> getGlobalResonseMessage() {
        List<Response> responseList = new ArrayList<>();
        responseList.add(new ResponseBuilder().code("404").description("找不到资源").build());
        return responseList;
    }

    /**
     * api 信息
     *
     * @return
     */
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("SimpleAdmin")
                .description("简易管理系统接口文档")
                .version("1.0.0")
                .build();
    }
}
