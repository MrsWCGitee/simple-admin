package simpleadmin.web.common.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import simpleadmin.web.security.*;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * 白名单
     */
    public static final String[] URL_WHITELIST = {
            "/favicon.ico", // 网站图标
            "/swagger**/**", "/webjars/**", "/v3/**", "/doc.html", // swagger 相关
            "/static/**", "/content/**", "/elfinder/**", "/image/**", "/js/**", // 静态文件相关
            "/fm.html", "/fmsel.html", "/fm.html?mimeType=image", "/fmsel.html?mimeType=image", // elfinder 文件管理器相关
            "/api/login", "/logout", "/api/auth/refreshToken", "/api/auth/getCaptcha", "/api/codeGenerator/fast", // 白名单接口
            "/websocket/**", // websocket 相关
    };
    /**
     * 用户详情服务实现
     */
    private final UserDetailsServiceImpl userDetailsService;
    /**
     * 登录失败执行的操作
     */
    private final JwtLoginFailureHandler loginFailureHandler;
    /**
     * 登录成功执行的操作
     */
    private final JwtLoginSuccessHandler loginSuccessHandler;
    /**
     * Jwt 认证入口点
     */
    private final JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;
    /**
     * 拒绝访问执行的操作
     */
    private final JwtAccessDeniedHandler jwtAccessDeniedHandler;
    /**
     * 登出成功执行的操作
     */
    private final JwtLogoutSuccessHandler jwtLogoutSuccessHandler;

    @Autowired
    public SpringSecurityConfig(UserDetailsServiceImpl userDetailsService,
                                JwtLoginFailureHandler loginFailureHandler,
                                JwtLoginSuccessHandler loginSuccessHandler,
                                JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint,
                                JwtAccessDeniedHandler jwtAccessDeniedHandler,
                                JwtLogoutSuccessHandler jwtLogoutSuccessHandler) {
        this.userDetailsService = userDetailsService;
        this.loginFailureHandler = loginFailureHandler;
        this.loginSuccessHandler = loginSuccessHandler;
        this.jwtAuthenticationEntryPoint = jwtAuthenticationEntryPoint;
        this.jwtAccessDeniedHandler = jwtAccessDeniedHandler;
        this.jwtLogoutSuccessHandler = jwtLogoutSuccessHandler;
    }

    /**
     * Jwt 身份认证过滤器
     *
     * @return 结果
     */
    @Bean
    JWTAuthenticationFilter jwtAuthenticationFilter() throws Exception {
        return new JWTAuthenticationFilter(authenticationManager());
    }

    /**
     * 注册自定义的 UsernamePasswordAuthenticationFilter
     * 用户名，密码验证（原有的 UsernamePasswordAuthenticationFilter 仅支持表单数据的登录）
     * 这里为了兼容 json 格式的数据 需要重写
     */
    @Bean
    JsonAuthenticationFilter jsonAuthenticationFilter() throws Exception {
        JsonAuthenticationFilter filter = new JsonAuthenticationFilter();
        filter.setAuthenticationSuccessHandler(loginSuccessHandler);
        filter.setAuthenticationFailureHandler(loginFailureHandler);
        filter.setFilterProcessesUrl("/api/login");
        // 这句很关键，重用WebSecurityConfigurerAdapter配置的AuthenticationManager，不然要自己组装AuthenticationManager
        filter.setAuthenticationManager(authenticationManagerBean());
        return filter;
    }

    /**
     * 采用的加密方式
     */
    @Bean
    BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * 配置
     */
    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        // 禁用 csrf, 由于使用的是JWT，我们这里不需要csrf
        httpSecurity.cors().and().csrf().disable()
                // 关闭session
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)

                // 设置允许页面被 Iframe 嵌套
                .and().headers().frameOptions().disable()

                // 登录
                .and()
                .formLogin()
                // .loginPage("/")
                // .successHandler(loginSuccessHandler)
                // .failureHandler(loginFailureHandler)

                // 退出
                .and()
                .logout()
                .logoutUrl("/api/logout")
                .logoutSuccessHandler(jwtLogoutSuccessHandler)

                // 资源访问控制
                .and()
                .authorizeRequests()

                // 跨域预检请求
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()

                // 白名单
                .antMatchers(URL_WHITELIST).permitAll()

                // 其他所有请求需要身份认证
                .anyRequest().authenticated()

                // 异常处理器
                .and()
                .exceptionHandling()
                .authenticationEntryPoint(jwtAuthenticationEntryPoint)
                .accessDeniedHandler(jwtAccessDeniedHandler)

                .and()
                .addFilter(jwtAuthenticationFilter());

        // 用重写的Filter替换掉原有的UsernamePasswordAuthenticationFilter
        httpSecurity.addFilterAt(jsonAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);

    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }

}
