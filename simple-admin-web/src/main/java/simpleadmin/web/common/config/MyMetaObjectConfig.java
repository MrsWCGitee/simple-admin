package simpleadmin.web.common.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;
import simpleadmin.web.common.tools.SecurityUtil;
import simpleadmin.web.entity.User;

import java.time.LocalDateTime;

/**
 * mybatis-plus 特定字段默认赋值
 */
@Configuration
public class MyMetaObjectConfig implements MetaObjectHandler {

    // 自动插入、更新
    @Override
    public void insertFill(MetaObject metaObject) {
        setCommonFileds(metaObject, true);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        setCommonFileds(metaObject, false);
    }

    /**
     * 设置通用字段的默认值
     *
     * @param metaObject
     * @param isInsert   是否是新增
     */
    private void setCommonFileds(MetaObject metaObject, Boolean isInsert) {
        User user = SecurityUtil.getUserDetails();
        if (user != null) {
            if (user.getId() > 0L) {
                if (isInsert) {
                    this.setFieldValByName("createdUserId", user.getId(), metaObject);
                }
                this.setFieldValByName("updatedUserId", user.getId(), metaObject);
            }
            if (StringUtils.hasLength(user.getName())) {
                if (isInsert) {
                    this.setFieldValByName("createdUserName", user.getName(), metaObject);
                }
                this.setFieldValByName("updatedUserName", user.getName(), metaObject);
            }
        }
        LocalDateTime nowTime = LocalDateTime.now();
        if (isInsert) {
            this.setFieldValByName("createdTime", nowTime, metaObject);
        }
        this.setFieldValByName("updatedTime", nowTime, metaObject);
    }

}
