package simpleadmin.web.common.plugin.logging.aop.enums;

/**
 * 日 志 类 型
 *
 * @Author:wangcheng
 * @Date: 2022/03/07
 * @Time 10:23
 */
public enum LoggingType {

    /**
     * 操 作 日 志
     */
    OPERATE,

    /**
     * 登 录 日 志
     */
    LOGIN
}
