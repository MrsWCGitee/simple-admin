package simpleadmin.web.common.plugin.logging.aop.enums;

/**
 * 日 志 业 务 类 型
 *
 * @Author:wangcheng
 * @Date: 2022/03/07
 * @Time 10:23
 */
public enum BusinessType {

    /**
     * 新增
     */
    ADD,

    /**
     * 修改
     */
    EDIT,

    /**
     * 删除
     */
    REMOVE,

    /**
     * 查询
     */
    QUERY,

    /**
     * 导出
     */
    EXPORT,

    /**
     * 其他
     */
    OTHER
}
