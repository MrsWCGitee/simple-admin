package simpleadmin.web.common.plugin.logging.aop.enums;

/**
 * 请 求 方 式
 *
 * @Author:wangcheng
 * @Date: 2022/03/07
 * @Time 10:23
 */
public enum RequestMethod {

    /**
     * Get 请求
     */
    GET,

    /**
     * Post 请求
     */
    POST,

    /**
     * Put 请求
     */
    PUT,

    /**
     * Delete 请求
     */
    DELETE

}
