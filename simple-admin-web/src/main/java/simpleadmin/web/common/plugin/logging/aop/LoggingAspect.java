package simpleadmin.web.common.plugin.logging.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import simpleadmin.web.common.plugin.logging.async.LoggingFactory;

import java.lang.reflect.Method;

/**
 * 日志切面
 *
 * @Author:wangcheng
 * @Date: 2022/03/07
 * @Time 10:23
 */
@Aspect
@Component
public class LoggingAspect {

    private final LoggingFactory loggingFactory;

    @Autowired
    public LoggingAspect(LoggingFactory loggingFactory) {
        this.loggingFactory = loggingFactory;
    }

    /**
     * 切 面 编 程
     */
    // @Pointcut("@annotation(simpleadmin.web.common.plugin.logging.aop.annotation.Logging)
    // ||
    // @within(simpleadmin.web.common.plugin.logging.aop.annotation.Logging)")
    @Pointcut("execution(public * simpleadmin.web.controller.*.*(..))")
    public void dsPointCut() {
    }

    @Before("dsPointCut()")
    public void deBefore(JoinPoint joinPoint) throws Throwable {
        // 接收到请求，记录请求内容
        System.out.println("接收到请求，记录请求内容 : ");
    }

    @AfterReturning(returning = "ret", pointcut = "dsPointCut()")
    public void doAfterReturning(Object ret) throws Throwable {
        // 处理完请求，返回内容
        System.out.println("方法的返回值 : " + ret);
    }

    /**
     * 后置异常通知（方法异常时执行.....）
     *
     * @param jp JoinPoint对象封装了SpringAop中切面方法的信息
     * @param e  异常对象
     * @throws Exception 异常类型
     */
    @AfterThrowing(value = "dsPointCut()", throwing = "e")
    public void throwss(JoinPoint jp, Exception e) throws Exception {
        // 先记录错误日志，然后该异常抛出，统一让 GloablExceptionHandler 处理
        loggingFactory.saveExLog(jp, e);
    }

    // 后置最终通知,final增强，不管是抛出异常或者正常退出都会执行
    @After("dsPointCut()")
    public void after(JoinPoint jp) {
        System.out.println("方法最后执行.....");
    }

    /**
     * 处 理 系 统 日 志（这里可以记录操作日志）
     */
    @Around("dsPointCut()")
    private Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        // 为了测试 elfinder 这里暂时注释掉
        return loggingFactory.saveOpLog(joinPoint);
        // return null;
    }

    /**
     * 获 取 注 解
     */
    public simpleadmin.web.common.plugin.logging.aop.annotation.Logging getLogging(ProceedingJoinPoint point) {
        MethodSignature signature = (MethodSignature) point.getSignature();
        Class<?> targetClass = point.getTarget().getClass();
        simpleadmin.web.common.plugin.logging.aop.annotation.Logging targetLogging = targetClass
                .getAnnotation(simpleadmin.web.common.plugin.logging.aop.annotation.Logging.class);
        if (targetLogging != null) {
            return targetLogging;
        } else {
            Method method = signature.getMethod();
            simpleadmin.web.common.plugin.logging.aop.annotation.Logging logging = method
                    .getAnnotation(simpleadmin.web.common.plugin.logging.aop.annotation.Logging.class);
            return logging;
        }
    }
}
