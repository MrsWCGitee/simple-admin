package simpleadmin.web.common.plugin.logging.async;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import simpleadmin.web.common.enums.LoginType;
import simpleadmin.web.common.enums.YesOrNot;
import simpleadmin.web.common.tools.JsonUtil;
import simpleadmin.web.common.tools.SecurityUtil;
import simpleadmin.web.common.tools.ServletUtil;
import simpleadmin.web.entity.LogEx;
import simpleadmin.web.entity.LogOp;
import simpleadmin.web.entity.LogVis;
import simpleadmin.web.entity.User;
import simpleadmin.web.service.ILogExService;
import simpleadmin.web.service.ILogOpService;
import simpleadmin.web.service.ILogVisService;

import java.time.LocalDateTime;

/**
 * 日志异步工厂
 *
 * @author wangcheng
 * @since 2022-06-08
 */
@Component
public class LoggingFactory {

    private final ILogExService logExService;
    private final ILogOpService logOpService;
    private final ILogVisService logVisService;

    @Autowired
    public LoggingFactory(ILogExService logExService, ILogOpService logOpService, ILogVisService logVisService) {
        this.logExService = logExService;
        this.logOpService = logOpService;
        this.logVisService = logVisService;
    }

    /**
     * 异常日志入库
     */
    @Async
    public void saveExLog(JoinPoint joinPoint, final Exception e) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        // 请求的 类名、方法名
        String className = joinPoint.getTarget().getClass().getName();
        String methodName = signature.getName();
        // 请求的参数
        String paramsObjs = JsonUtil.toString(joinPoint.getArgs());
        // 异常名
        String exName = e.getClass().getName();
        // 异常来源
        String exSource = e.toString();
        // 异常消息
        String exMsg = e.getMessage();
        // 堆栈跟踪详情
        String stackTrace = "";
        for (StackTraceElement stackTraceElement : e.getStackTrace()) {
            stackTrace += stackTraceElement.toString();
        }
        LogEx log = new LogEx();
        // 用户信息
        User user = SecurityUtil.getUserDetails();
        if (user != null) {
            log.setAccount(user.getAccount());
            log.setName(user.getName());
        } else {
            log.setAccount(SecurityUtil.currentUserName());
        }
        log.setClassName(className);
        log.setMethodName(methodName);
        log.setExceptionName(exName);
        log.setExceptionMsg(exMsg);
        log.setExceptionSource(exSource);
        log.setStackTrace(stackTrace);
        log.setParamsObj(paramsObjs);
        log.setExceptionTime(LocalDateTime.now());
        logExService.save(log);
    }

    /**
     * 操作日志入库
     */
    @Async
    public Object saveOpLog(ProceedingJoinPoint joinPoint) throws Throwable {
        Object result;
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        // 请求的 类名、方法名
        String className = joinPoint.getTarget().getClass().getName();
        String methodName = signature.getName();
        // 请求的参数
        String paramsObjs = JsonUtil.toString(joinPoint.getArgs());

        LogOp log = new LogOp();
        // 用户信息
        User user = SecurityUtil.getUserDetails();
        if (user != null) {
            log.setAccount(user.getAccount());
            log.setName(user.getName());
        }
        log.setSuccess(YesOrNot.Y.getValue());
        log.setIp(ServletUtil.getRemoteHost());
        log.setLocation(ServletUtil.getRequestURL());
        log.setBrowser(ServletUtil.getBrowser());
        log.setOs(ServletUtil.getOsSystem());
        log.setUrl(ServletUtil.getRequestURI());
        log.setClassName(className);
        log.setMethodName(methodName);
        log.setReqMethod(ServletUtil.getMethod());
        log.setParam(paramsObjs);
        log.setOpTime(LocalDateTime.now());

        Long startTime = System.currentTimeMillis(); // 获取开始时间
        try {
            result = joinPoint.proceed();
            // 操作结果入库，有时这个结果字符串过大导致数据库服务器有较大的压力，暂时注释掉了
            // log.setResult(JsonUtil.toString(result));
        } catch (Exception exception) {
            log.setSuccess(YesOrNot.N.getValue());
            log.setMessage(exception.getMessage());
            throw exception;
        } finally {
            Long endTime = System.currentTimeMillis(); // 获取结束时间
            Long elapsedTime = endTime - startTime;
            log.setElapsedTime(elapsedTime);
            logOpService.save(log);
        }
        return result;
    }

    /**
     * 访问日志入库(用户登入，用户登出等操作)
     */
    @Async
    public void saveVisLog(LoginType loginType, String message) {
        LogVis log = new LogVis();
        try {
            // 用户名
            log.setAccount(SecurityUtil.currentUserName());
            log.setSuccess(YesOrNot.Y.getValue());
            log.setIp(ServletUtil.getRemoteHost());
            log.setLocation(ServletUtil.getRequestURL());
            log.setBrowser(ServletUtil.getBrowser());
            log.setOs(ServletUtil.getOsSystem());
            log.setVisType(loginType.getValue());
            log.setMessage(message);
        } catch (Exception exception) {
            log.setSuccess(YesOrNot.N.getValue());
            log.setMessage(exception.getMessage());
            throw exception;
        } finally {
            log.setVisTime(LocalDateTime.now());
            logVisService.save(log);
        }
    }

}
