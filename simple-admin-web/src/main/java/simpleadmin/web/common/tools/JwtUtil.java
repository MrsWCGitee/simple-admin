package simpleadmin.web.common.tools;

import cn.hutool.core.date.DateUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.jwt.JWT;
import cn.hutool.jwt.JWTUtil;
import cn.hutool.jwt.JWTValidator;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Data
@Component
@ConfigurationProperties(prefix = "admin.jwt")
public class JwtUtil {

    /**
     * 加密秘钥
     */
    private String secret;

    /**
     * token 请求头
     */
    private String tokenRequestHeader;

    /**
     * token 响应头
     */
    private String tokenResponseHeader;

    /**
     * token 过期时间
     */
    private long tokenExpire;

    /**
     * refreshToken 请求头
     */
    private String refreshTokenRequestHeader;

    /**
     * refreshToken 响应头
     */
    private String refreshTokenResponseHeader;

    /**
     * refreshToken 过期时间
     */
    private long refreshTokenExpire;

    /**
     * 认证头
     */
    private String authenticationHead;

    private final String tokenKey = "user_name";
    private final String refreshTokenKey = "token";
    private final String expireTime = "expire_time";

    /**
     * 生成 token
     *
     * @param username
     * @return
     */
    public String generateToken(String username) {
        Map<String, Object> map = new HashMap<String, Object>() {
            private static final long serialVersionUID = 1L;
            {
                put(tokenKey, username);
                put(expireTime, System.currentTimeMillis() + 1000 * tokenExpire);
            }
        };
        return JWTUtil.createToken(map, secret.getBytes());
    }

    /**
     * 生成刷新 token
     *
     * @return
     */
    public String generateRefreshToken(String token) {
        Map<String, Object> map = new HashMap<String, Object>() {
            private static final long serialVersionUID = 2L;
            {
                put(refreshTokenKey, token);
                put(expireTime, System.currentTimeMillis() + 1000 * refreshTokenExpire);
            }
        };
        return JWTUtil.createToken(map, secret.getBytes());
    }

    /**
     * 获取 jwt 的承载信息
     *
     * @param token
     * @return
     */
    public JSONObject getPayloads(String token) {
        token = removeExpire(token);
        final JWT jwt = JWTUtil.parseToken(token);
        JSONObject payloads = jwt.getPayloads();
        return payloads;
    }

    /**
     * 获取 UserName 根据 token 的承载内容
     *
     * @param payloads token的承载内容
     * @return
     */
    public String getUserNameByPayloads(JSONObject payloads) {
        return payloads.get(tokenKey).toString();
    }

    /**
     * 获取 UserName 根据 token
     *
     * @param token
     * @return
     */
    public String getUserNameByToken(String token) {
        JSONObject payloads = getPayloads(token);
        return payloads.get(tokenKey).toString();
    }


    /**
     * 获取 token 根据 token 的承载内容
     *
     * @param payloads refreshToken的承载内容
     * @return
     */
    public String getTokenByPayloads(JSONObject payloads) {
        return payloads.get(refreshTokenKey).toString();
    }

    /**
     * 验证 token、refreshToken 是否过期
     *
     * @param payloads refreshToken的承载内容
     * @return
     */
    public boolean tokenExpired(JSONObject payloads) {
        try {
            Long expireTimeMillis = Long.valueOf(String.valueOf(payloads.get(expireTime)));
            return System.currentTimeMillis() > expireTimeMillis;
        } catch (Exception ex) {
            return true;
        }
    }

    /**
     * 验证 token
     *
     * @param token
     * @return
     */
    public boolean verifyToken(String token) {
        token = removeExpire(token);
        return JWTUtil.verify(token, secret.getBytes());
    }

    /**
     * 去除 token 或 refreshToken 的前缀
     *
     * @param jwt 传入携带前缀的的 token 或 refreshToken
     * @return
     */
    private String removeExpire(String jwt) {
        if (jwt.startsWith(authenticationHead + " ")) {
            return jwt.substring((authenticationHead + " ").length());
        }
        if (jwt.startsWith(refreshTokenRequestHeader + " ")) {
            return jwt.substring((refreshTokenRequestHeader + " ").length());
        }
        return jwt;
    }


}
