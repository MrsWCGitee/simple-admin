package simpleadmin.web.common.tools;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import simpleadmin.web.entity.User;

public class SecurityUtil {

    /**
     * 获取当前登录用户的信息，如果未登录返回null
     *
     * @return Object 当前登录用户名
     */
    public static String currentUserName() {
        return null != getAuthentication() ? getAuthentication().getName() : null;
    }

    /**
     * 获取当前用户详情
     *
     * @return
     */
    public static User getUserDetails() {
        Authentication auth = getAuthentication();
        if (auth != null) {
            Object details = getAuthentication().getDetails();
            if (details != null && details instanceof User) {
                return (User) details;
            }
        }
        return null;
    }

    /**
     * 获取当前用户详情
     *
     * @return
     */
    public static Long getUserId() {
        Authentication auth = getAuthentication();
        if (auth != null) {
            Object details = getAuthentication().getDetails();
            if (details != null && details instanceof User) {
                User user = (User) details;
                return user.getId();
            }
        }
        return null;
    }

    /**
     * 获取当前登录用户的信息
     *
     * @return Authentication 权鉴对象
     */
    public static Authentication getAuthentication() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return !(auth instanceof AnonymousAuthenticationToken) ? auth : null;
    }

    /**
     * 验证当前用户是否登录
     *
     * @return boolean 是否登录
     */
    public static boolean isAuthentication() {
        // if security session eq s-id is not null to index
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return !(auth instanceof AnonymousAuthenticationToken);
    }

}
