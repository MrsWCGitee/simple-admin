package simpleadmin.web.common.tools;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import simpleadmin.web.common.http.HttpResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * HTTP工具类
 *
 * @author wangcheng
 * @since 2022-06-03
 */
public class HttpUtil {

    /**
     * 获取HttpServletRequest对象
     *
     * @return
     */
    public static HttpServletRequest getHttpServletRequest() {
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    }

    /**
     * 输出成功的信息到浏览器
     *
     * @param response HttpServletResponse
     * @param data     返回结果
     * @throws IOException
     */
    public static void writeOk(HttpServletResponse response, Object data) throws IOException {
        response.setContentType("application/json; charset=utf-8");
        HttpResult result = HttpResult.ok(data);
        String json = JsonUtil.toString(result);
        response.getWriter().print(json);
        response.getWriter().flush();
        response.getWriter().close();
    }

    /**
     * 输出异常信息到浏览器
     *
     * @param response
     * @param code
     * @param msg
     * @throws IOException
     */
    public static void writeError(HttpServletResponse response, Integer code, String msg) throws IOException {
        response.setContentType("application/json; charset=utf-8");
        HttpResult result = HttpResult.error(code, msg);
        String json = JsonUtil.toString(result);
        response.getWriter().print(json);
        response.getWriter().flush();
        response.getWriter().close();
    }

}
