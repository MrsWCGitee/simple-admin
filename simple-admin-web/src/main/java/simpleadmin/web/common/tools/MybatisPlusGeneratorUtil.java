package simpleadmin.web.common.tools;

import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.converts.MySqlTypeConvert;
import com.baomidou.mybatisplus.generator.config.querys.MySqlQuery;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.engine.VelocityTemplateEngine;
import com.baomidou.mybatisplus.generator.keywords.MySqlKeyWordsHandler;
import org.springframework.beans.factory.annotation.Value;
import simpleadmin.web.service.impl.base.BaseServiceImpl;

import java.util.Arrays;
import java.util.Collections;

/**
 * MybatisPlus 代码生成器配置
 *
 * @Author:wangcheng
 * @Date: 2022/03/10
 * @Time 13:41
 */
public class MybatisPlusGeneratorUtil {

    /**
     * 数据库链接串
     */
    @Value("${spring.datasource.url}")
    private static String dataUrl;

    /**
     * 数据库用户名
     */
    @Value("${spring.datasource.username}")
    private static String userName;

    /**
     * 数据库密码
     */
    @Value("${spring.datasource.password}")
    private static String password;

    /**
     * 包名
     */
    private static final String packageName = "simpleadmin.web";

    /**
     * 表前缀
     */
    private static final String tablePrefix = "sys_";

    /**
     * 项目路径
     */
    private static final String projectPath = System.getProperty("user.dir");

    /**
     * 代码生成（简单模式）
     */
    public static void CodeFastCreate() {
        FastAutoGenerator.create(dataUrl, userName, password)
                // 全局配置
                .globalConfig(builder -> {
                    builder.author("wangcheng") // 设置作者
                            .enableSwagger() // 开启 swagger 模式
                            .fileOverride() // 覆盖已生成文件
                            .disableOpenDir() // 禁止打开输出目录
                            .outputDir(projectPath + "/src/main/java"); // 指定输出目录
                })
                // 包配置
                .packageConfig(builder -> {
                    builder.parent(packageName) // 设置父包名
                            // .moduleName(moduleName) // 设置父包模块名
                            .entity("entity") // Entity 包名 默认值:entity
                            .service("service") // Service 包名 默认值:service
                            .serviceImpl("service.impl") // Service Impl 包名 默认值:service.impl
                            .mapper("mapper") // Mapper 包名 默认值:mapper
                            .xml("mapper.xml") // Mapper XML 包名 默认值:mapper.xml
                            .controller("controller") // Controller 包名 默认值:controller
                            // .other("domain.dto") // 自定义文件包名 输出自定义文件时所用到的包名
                            .pathInfo(Collections.singletonMap(OutputFile.mapperXml,
                                    projectPath + "/src/main/resources/mapping")); // 设置mapperXml生成路径
                })
                // 实体策略配置
                .strategyConfig(builder -> {
                    builder.entityBuilder()
                            // 开启Lombok，默认生成@Get，@Set，可以手动换成@Data
                            .enableLombok();
                })
                // 控制类策略配置
                .strategyConfig(builder -> {
                    builder.controllerBuilder()
                            // 如果没有父类请注释掉
                            // .superClass(BaseController.class)
                            .enableRestStyle();
                })
                // 服务类策略配置
                .strategyConfig(builder -> {
                    builder.serviceBuilder()
                            // .superServiceClass(BaseService.class)
                            .superServiceImplClass(BaseServiceImpl.class)
                            .formatServiceFileName("I%sService")
                            .formatServiceImplFileName("%sServiceImpl");
                })
                .strategyConfig(builder -> {
                    // 设置需要生成的表名
                    // builder.addInclude("sys_app").addTablePrefix("sys_", "c_"); // 设置过滤表前缀
                    String[] blackItems = new String[]{
                            "sys_menu", "sys_role", "sys_user",
                            "sys_app", "sys_config",
                            "sys_dict_data",
                            "sys_dict_type", "sys_emp", "sys_emp_ext_org_pos", "sys_emp_pos", "sys_file",
                            "sys_log_audit", "sys_log_ex", "sys_log_op", "sys_log_vis", "sys_menu",
                            "sys_notice", "sys_notice_user", "sys_oauth_user", "sys_online_user",
                            "sys_org",
                            "sys_pos", "sys_role", "sys_role_data_scope", "sys_role_menu", "sys_timer",
                            "sys_user", "sys_user_data_scope", "sys_user_role"
                    };
                    // String[] tableItems = new String[] { "sys_app", "sys_config" };

                    String[] tableItems = new String[]{
                            "sys_view_notice_receive"
                    };

                    for (String item : tableItems) {
                        if (!Arrays.asList(blackItems).contains(item)) {
                            builder.addInclude(item).addTablePrefix("sys_", "c_");
                        }
                    }
                })
                .templateConfig(builder -> {
                    builder.entity("/templates/entity.java");
                    builder.service("/templates/service.java");
                    builder.serviceImpl("/templates/serviceImpl.java");
                    builder.mapper("/templates/mapper.java");
                    builder.mapperXml("/templates/mapper.xml");
                    builder.controller("/templates/controller.java");
                })
                // .templateEngine(new BeetlTemplateEngine()) // 使用Beetl引擎模板，默认的是Velocity引擎模板
                // .templateEngine(new FreemarkerTemplateEngine()) //
                // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .templateEngine(new VelocityTemplateEngine()) // 使用Velocity引擎模板
                .execute();
    }

    /**
     * 代码生成（正常模式）
     */
    public static void CodeCreate() {
        /**
         * 数据源配置
         */
        DataSourceConfig dsc = new DataSourceConfig.Builder(dataUrl, userName, password)
                .dbQuery(new MySqlQuery())
                .typeConvert(new MySqlTypeConvert())
                .keyWordsHandler(new MySqlKeyWordsHandler())
                .build();
        AutoGenerator mpg = new AutoGenerator(dsc);

        /**
         * 全局配置
         */
        GlobalConfig gc = new GlobalConfig.Builder()
                .fileOverride()
                .outputDir(projectPath + "/src/main/java")
                .author("wangcheng")
                .enableKotlin()
                .enableSwagger()// 实体属性 Swagger 注解
                .dateType(DateType.TIME_PACK)
                .commentDate("yyyy-MM-dd")
                .build();
        mpg.global(gc);

        /**
         * 包配置
         */
        PackageConfig pc = new PackageConfig.Builder()
                .parent(packageName)
                // .moduleName(moduleName)
                .entity("entity")
                .service("service")
                .serviceImpl("service.impl")
                .mapper("mapper")
                .xml("mapper.xml")
                .controller("controller")
                // .other("other")
                .pathInfo(Collections.singletonMap(OutputFile.mapperXml, projectPath + "/src/main/resources/mapping")) // 设置mapperXml生成路径
                .build();
        mpg.packageInfo(pc);

        /**
         * 输出模板，如果按照官方原生的可以不配置，也可以配置自定义的模板
         */
        TemplateConfig tc = new TemplateConfig.Builder()
                .disable(TemplateType.ENTITY)
                .entity("/templates/entity.java") // 设置实体模板路径(JAVA) /templates/entity.java
                .service("/templates/service.java")
                .serviceImpl("/templates/serviceImpl.java")
                .mapper("/templates/mapper.java")
                .mapperXml("/templates/mapper.xml")
                .controller("/templates/controller.java")
                .build();
        mpg.template(tc);

        /**
         * 自定义配置，可以自定义参数在模板中使用，还可以已定义输出的文件，
         * 如果除了上面的几个模板之外还有其他的文件需要输出可以在这里设置
         */
        InjectionConfig ic = new InjectionConfig.Builder()
                .beforeOutputFile((tableInfo, objectMap) -> {
                    System.out.println("tableInfo: " + tableInfo.getEntityName() + " objectMap: "
                            + objectMap.size());
                })
                // .customMap(Collections.singletonMap("test", "baomidou"))
                // .customFile(Collections.singletonMap("test.txt", "/templates/test.vm"))
                .build();
        mpg.injection(ic);

        /**
         * 生成策略配置，常用
         * 1.指定生成的表名
         * 2.表名前缀过滤
         * 3.实体名、字段名的命名方式
         * 4.指定继承的父类、父字段
         */
        StrategyConfig sc = new StrategyConfig.Builder()
                .enableCapitalMode()
                .enableSkipView()
                .disableSqlFilter()
                // .likeTable(new LikeTable("USER"))
                .addInclude("sys_app")
                .addTablePrefix(tablePrefix) // 过滤表前缀
                // .addFieldSuffix("_flag") // 过滤表后缀
                .build();
        mpg.strategy(sc);

        // 执行生成
        mpg.execute();
    }

}
