package simpleadmin.web.common.exception;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.internal.engine.path.PathImpl;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import simpleadmin.web.common.http.HttpResult;
import simpleadmin.web.common.http.HttpStatus;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 全局异常处理
 *
 * @Author wangcheng
 * @date 2022-03-15 15:55
 */
// @RestControllerAdvice 增强的 Controller
// 可以实现以下几种功能
// 全局异常处理
// 全局数据绑定
// 全局数据预处理
@Slf4j
@RestControllerAdvice
public class GloablExceptionHandler {

    /**
     * 不 支 持 的 请 求 类 型
     */
    @ExceptionHandler({HttpRequestMethodNotSupportedException.class})
    public HttpResult handleException(HttpRequestMethodNotSupportedException e) {
        log.error(e.getMessage(), e);
        return HttpResult.error("不支持' " + e.getMethod() + "'请求");
    }

    /**
     * 拦 截 未 知 的 运 行 时 异 常
     */
    @ExceptionHandler({RuntimeException.class})
    public HttpResult notFount(RuntimeException e) {
        log.error("运行时异常:", e);
        return HttpResult.error("运行时异常:" + e.getMessage());
    }

    /**
     * 拦 截 Spring Security 的 用 户 登 录 授 权 时 异 常
     */
    @ExceptionHandler({AuthenticationException.class})
    public HttpResult authenticationException(AuthenticationException e) {
        log.error("用户登录授权异常:", e);
        return HttpResult.error(e.getMessage());
    }

    /**
     * 拦 截 Spring Security 的 用 户 登 录 服 务 的 异 常
     */
    @ExceptionHandler({AuthenticationServiceException.class})
    public HttpResult authenticationServiceException(AuthenticationServiceException e) {
        log.error("用户登录服务异常:", e);
        return HttpResult.error(e.getMessage());
    }

    /**
     * 系 统 异 常
     */
    @ExceptionHandler(Exception.class)
    public HttpResult handleException(Exception e) {
        log.error(e.getMessage(), e);
        return HttpResult.error(HttpStatus.SC_INTERNAL_SERVER_ERROR, "服务器内部出现异常，请联系管理员！");
    }

    @ExceptionHandler(BindException.class)
    public HttpResult sendErrorResponse_Validator(BindException exception) {
        Map<String, String> errors = new HashMap<>(16);
        List<FieldError> errs = exception.getFieldErrors();
        for (FieldError err : errs) {
            String paramName = err.getField();
            String message = err.getDefaultMessage();
            errors.put(paramName, message);
        }
        return HttpResult.error(HttpStatus.SC_DATA_VALIDATION_ERROR, "数据验证不通过！", errors);
    }

    /**
     * 统一捕获验证注解异常方法
     */
    @ExceptionHandler(ConstraintViolationException.class)
    public HttpResult sendErrorResponse_ConstraintViolation(ConstraintViolationException exception) {
        Map<String, String> errors = new HashMap<>(16);
        Set<ConstraintViolation<?>> constraintViolations = exception.getConstraintViolations();
        for (ConstraintViolation<?> constraintViolation : constraintViolations) {
            PathImpl pathImpl = (PathImpl) constraintViolation.getPropertyPath();
            // 读取参数字段，constraintViolation.getMessage() 读取验证注解中的message值
            String paramName = pathImpl.getLeafNode().getName();
            String message = "参数{".concat(paramName).concat("}").concat(constraintViolation.getMessage());
            errors.put(paramName, message);
        }
        return HttpResult.error(HttpStatus.SC_DATA_VALIDATION_ERROR, "数据验证不通过！", errors);
    }

}
