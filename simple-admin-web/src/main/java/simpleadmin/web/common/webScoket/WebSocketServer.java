package simpleadmin.web.common.webScoket;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import simpleadmin.web.common.config.webSocket.WebSocketConfigurator;
import simpleadmin.web.common.tools.JwtUtil;
import simpleadmin.web.entity.OnlineUser;
import simpleadmin.web.entity.User;
import simpleadmin.web.service.IOnlineUserService;
import simpleadmin.web.service.IUserService;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * WebSocket 相关（ws://localhost:8087/websocket/token）
 */
@Component
@Slf4j
@ServerEndpoint(value = "/websocket/{token}", configurator = WebSocketConfigurator.class)
public class WebSocketServer {

    // 用来存在线连接
    private static final Map<String, Session> onlineUserMap = new HashMap<String, Session>();
    // 用来存要离线的连接
    private static final Map<String, Session> offlineUserMap = new HashMap<String, Session>();
    private static ApplicationContext applicationContext;
    // 在线人数
    private static int onlineCount = 0;
    private IOnlineUserService onlineUserService;
    private IUserService userService;
    private JwtUtil jwtUtil;

    public static void setApplicationContext(ApplicationContext context) {
        applicationContext = context;
    }

    /**
     * 链接成功调用的方法
     */
    @OnOpen
    public void onOpen(Session session, @PathParam(value = "token") String token) {
        try {
            onlineUserService = applicationContext.getBean(IOnlineUserService.class);
            // 根据 token 获取 用户信息
            jwtUtil = applicationContext.getBean(JwtUtil.class);
            String userName = jwtUtil.getUserNameByToken(token);
            userService = applicationContext.getBean(IUserService.class);
            User user = userService.findByUsername(userName);
            String userId = user.getId().toString();
            if (!onlineUserMap.containsKey(userId)) {
                onlineUserMap.put(userId, session);
                onlineCount = onlineUserMap.size();
            }

            OnlineUser onlineUser = onlineUserService.getByUserId(user.getId());
            if (onlineUser == null) {
                Map<String, Object> userProperties = session.getUserProperties();
                String ipAddr = (String) userProperties.get(WebSocketConfigurator.IP_ADDR);

                // 用户连接成功
                onlineUser = new OnlineUser();
                onlineUser.setAccount(user.getAccount());
                onlineUser.setConnectionId(user.getId().toString());
                onlineUser.setLastLoginIp(ipAddr);
                onlineUser.setLastTime(LocalDateTime.now());
                onlineUser.setName(user.getName());
                onlineUser.setUserId(user.getId());
                onlineUserService.save(onlineUser);
            }

            log.info("【websocket消息】有新的连接，总数为:" + onlineCount);
        } catch (Exception e) {
            log.error("链接成功发生异常,原因:" + e.getMessage());
        }
    }

    /**
     * 链接关闭调用的方法
     */
    @OnClose
    public void onClose(@PathParam(value = "token") String token) {
        try {
            onlineUserService = applicationContext.getBean(IOnlineUserService.class);
            // 根据 token 获取 用户信息
            jwtUtil = applicationContext.getBean(JwtUtil.class);
            String userName = jwtUtil.getUserNameByToken(token);
            userService = applicationContext.getBean(IUserService.class);
            User user = userService.findByUsername(userName);
            if (onlineUserMap.containsKey(user.getId().toString())) {
                onlineUserMap.remove(user.getId().toString());
                onlineCount = onlineUserMap.size();
            }
            List<OnlineUser> onlineUsers = onlineUserService.getListByUserId(user.getId());
            if (onlineUsers != null && onlineUsers.size() > 0) {
                onlineUserService.removeByIds(onlineUsers);
            }
            log.info("【websocket消息】连接断开，总数为:" + onlineCount);
        } catch (Exception e) {
            log.error("链接关闭发生异常,原因:" + e.getMessage());
        }
    }

    /**
     * 收到客户端消息后调用的方法
     *
     * @param message
     */
    @OnMessage
    public void onMessage(@PathParam("token") String token, String message) {
        log.info("【websocket消息】收到客户端消息:" + message);
        onlineUserService = applicationContext.getBean(IOnlineUserService.class);
        // 根据 token 获取 用户信息
        jwtUtil = applicationContext.getBean(JwtUtil.class);
        String userName = jwtUtil.getUserNameByToken(token);
        userService = applicationContext.getBean(IUserService.class);
        User user = userService.findByUsername(userName);
        String userId = user.getId().toString();
        try {
            Session offlineSession = offlineUserMap.get(userId);
            if (offlineSession != null) {
                Session onlineSession = onlineUserMap.get(userId);
                if (onlineSession != null && onlineSession.isOpen()) {
                    // onlineUserMap.get(userId.toString()).close();
                    // onlineUserMap.remove(userId.toString());
                    offlineUserMap.remove(userId);
                    onlineUserMap.get(userId).getBasicRemote().sendText("{\"kind\":110}");
                }
            }
            // 向指定用户发送消息
            onlineUserMap.get(userId).getBasicRemote().sendText(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 发送错误时的处理
     *
     * @param session
     * @param error
     */
    @OnError
    public void onError(Session session, Throwable error) {
        log.error("用户错误,原因:" + error.getMessage());
        error.printStackTrace();
    }

    /**
     * 强制离线（根据 socket session 的id）
     *
     * @param ids
     * @throws IOException
     */
    public void forceExistByConnectIds(List<Long> ids) throws IOException {
        for (Long id : ids) {
            Session session = onlineUserMap.get(id.toString());
            if (session != null && session.isOpen()) {
                offlineUserMap.put(id.toString(), session);
            }
        }
    }

    // 此为广播消息
    public void sendAllMessage(String message) {
        log.info("【websocket消息】广播消息:" + message);
        for (String key : onlineUserMap.keySet()) {
            Session session = onlineUserMap.get(key);
            if (session != null && session.isOpen()) {
                try {
                    session.getAsyncRemote().sendText(message);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    // 此为单点消息
    public void sendOneMessage(String userId, String message) {
        Session session = onlineUserMap.get(userId);
        if (session != null && session.isOpen()) {
            try {
                log.info("【websocket消息】 单点消息:" + message);
                session.getAsyncRemote().sendText(message);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // 此为单点消息(多人)
    public void sendMoreMessage(String[] userIds, String message) {
        for (String userId : userIds) {
            Session session = onlineUserMap.get(userId);
            if (session != null && session.isOpen()) {
                try {
                    log.info("【websocket消息】 单点消息:" + message);
                    session.getAsyncRemote().sendText(message);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }

}
