package simpleadmin.web.common.constant;

/*
 * 系统常量
 *
 * @Author wangcheng
 * @date 2022-06-22 14:38
 */
public class SystemConstant {

    /**
     * Windows 系统常量标识
     */
    public static String WINDOWS = "windows";

    /**
     * Linux 系统常量标识
     */
    public static String Linux = "linux";

    /**
     * UTF-8 编码格式
     */
    public static String UTF8 = "UTF-8";

    /**
     * GBK 编码格式
     */
    public static String GBK = "GBK";

    /**
     * 空串
     */
    public static String EMPTY = "";

    /**
     * Redis 用户登录的 Key 前缀
     */
    public static String RedisLoginKeyPrefix = "UserLogin_";
}
