package simpleadmin.web.common.constant;

/**
 * 日期时间格式化相关
 */
public class DateTimeFormatConstant {

    /**
     * 日期时间
     */
    public static final String DATETIME = "yyyy-MM-dd HH:mm:ss";

    /**
     * 日期
     */
    public static final String DATE = "yyyy-MM-dd";

    /**
     * 时间
     */
    public static final String TIME = "HH:mm:ss";

}
