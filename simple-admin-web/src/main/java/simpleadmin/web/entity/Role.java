package simpleadmin.web.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import simpleadmin.web.common.constant.DateTimeFormatConstant;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @Author wangchneg
 * @since 2022-03-10
 */
@Getter
@Setter
@TableName("sys_role")
@ApiModel(value = "Role对象")
public class Role implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "Id", type = IdType.AUTO) // 设置主键，并指定自增策略
    private Long id;

    private String name;

    private String code;

    private Integer sort;

    private String remark;

    private Integer status;

    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(shape = Shape.STRING, pattern = DateTimeFormatConstant.DATETIME)
    private LocalDateTime createdTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(shape = Shape.STRING, pattern = DateTimeFormatConstant.DATETIME)
    private LocalDateTime updatedTime;

    @TableField(fill = FieldFill.INSERT)
    private Long createdUserId;

    @TableField(fill = FieldFill.INSERT)
    private String createdUserName;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updatedUserId;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updatedUserName;

    private Integer isDeleted;

}
