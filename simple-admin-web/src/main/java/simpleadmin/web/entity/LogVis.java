package simpleadmin.web.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import simpleadmin.web.common.constant.DateTimeFormatConstant;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
@Getter
@Setter
@TableName("sys_log_vis")
@ApiModel(value = "LogVis对象")
public class LogVis implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "Id", type = IdType.AUTO)
    private Integer id;

    private String account;

    private String name;

    private Integer success;

    private String message;

    private String ip;

    private String location;

    private String browser;

    private String os;

    private Integer visType;

    @JsonFormat(pattern = DateTimeFormatConstant.DATETIME)
    private LocalDateTime visTime;

}
