package simpleadmin.web.entity.query;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import simpleadmin.web.common.page.PageParames;

/**
 * <p>
 * 访问日志 查询实体
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-03
 */
@Getter
@Setter
@ApiModel(value = "访问日志 查询实体")
public class LogVisPageQuery extends PageParames {

    /**
     * 名称
     */
    private String name;

    /**
     * 访问类型
     */
    private String visType;

    /**
     * 是否成功
     */
    private String success;

    /**
     * 访问时间
     */
    private String[] visTime;

}
