package simpleadmin.web.entity.query;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import simpleadmin.web.common.page.PageParames;

/**
 * <p>
 * 机构 查询实体
 * </p>
 *
 * @author wangcheng
 * @since 2022-07-11
 */
@Getter
@Setter
@ApiModel(value = "机构 查询实体")
public class OrgPageQuery extends PageParames {

    /**
     * 机构名称
     */
    private String name;

    /**
     * 组织机构Id
     */
    private Long[] orgId;

}
