package simpleadmin.web.entity.query;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import simpleadmin.web.common.page.PageParames;

/**
 * 在线用户查询实体
 *
 * @author wangcheng
 * @since 2022-08-07
 */
@Getter
@Setter
@ApiModel(value = "在线用户查询实体")
public class OnlineUserPageQuery extends PageParames {

    /**
     * 账户，姓名关键字
     */
    private String keyword;

}
