package simpleadmin.web.entity.query;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import simpleadmin.web.common.page.PageParames;

/**
 * <p>
 * 系统配置查询实体
 * </p>
 *
 * @author wangcheng
 * @since 2022-08-06
 */
@Getter
@Setter
@ApiModel(value = "系统配置查询实体")
public class ConfigPageQuery extends PageParames {

    /**
     * 参数名称
     */
    private String name;

    /**
     * 唯一编码
     */
    private String code;

    /**
     * 所属分类
     */
    private String groupCode;

}
