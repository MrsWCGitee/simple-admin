package simpleadmin.web.entity.query;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import simpleadmin.web.common.enums.NoticeStatus;
import simpleadmin.web.common.page.PageParames;

import java.util.List;

/**
 * 通知查询实体
 *
 * @author wangcheng
 * @since 2022-06-29
 */
@Getter
@Setter
@ApiModel(value = "通知查询实体")
public class NoticeReceivePageQuery extends PageParames {

    /**
     * 标题，内容关键字
     */
    private String keyword;

    /**
     * 类型
     */
    private Integer type;

    /**
     * 状态（字典 0草稿 1发布 2撤回 3删除）
     */
    private NoticeStatus status;

    /**
     * 通知到的人
     */
    private List<Long> noticeUserIds;

}
