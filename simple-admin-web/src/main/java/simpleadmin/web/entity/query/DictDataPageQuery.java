package simpleadmin.web.entity.query;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import simpleadmin.web.common.page.PageParames;

/**
 * <p>
 * 字典值查询实体
 * </p>
 *
 * @author wangcheng
 * @since 2022-08-15
 */
@Getter
@Setter
@ApiModel(value = "字典值查询实体")
public class DictDataPageQuery extends PageParames {

    /**
     * 字典类型 Id
     */
    private Long typeId;

    /**
     * 唯一编码
     */
    private String value;

    /**
     * 字典值
     */
    private String code;

}
