package simpleadmin.web.entity.query;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import simpleadmin.web.common.page.PageParames;

/**
 * <p>
 * 字典类型查询实体
 * </p>
 *
 * @author wangcheng
 * @since 2022-08-15
 */
@Getter
@Setter
@ApiModel(value = "字典类型查询实体")
public class DictTypePageQuery extends PageParames {

    /**
     * 类型名称
     */
    private String name;

    /**
     * 唯一编码
     */
    private String code;

}
