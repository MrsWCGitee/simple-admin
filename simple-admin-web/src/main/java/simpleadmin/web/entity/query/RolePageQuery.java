package simpleadmin.web.entity.query;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import simpleadmin.web.common.page.PageParames;

/**
 * <p>
 * 角色查询实体
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-03
 */
@Getter
@Setter
@ApiModel(value = "角色查询实体")
public class RolePageQuery extends PageParames {

    private String name;

    private String code;

    private Integer isDeleted;

}
