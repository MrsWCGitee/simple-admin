package simpleadmin.web.entity.query;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import simpleadmin.web.common.page.PageParames;

/**
 * 菜单查询实体
 *
 * @author wangcheng
 * @since 2022-07-15
 */
@Getter
@Setter
@ApiModel(value = "菜单查询实体")
public class MenuPageQuery extends PageParames {

    /**
     * 名称
     */
    private String name;

}
