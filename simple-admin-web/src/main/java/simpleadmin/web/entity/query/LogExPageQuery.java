package simpleadmin.web.entity.query;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import simpleadmin.web.common.page.PageParames;

/**
 * <p>
 * 异常日志 查询实体
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-03
 */
@Getter
@Setter
@ApiModel(value = "异常日志 查询实体")
public class LogExPageQuery extends PageParames {

    /**
     * 操作账号
     */
    private String account;

    /**
     * 类名
     */
    private String className;

    /**
     * 方法名
     */
    private String methodName;

    /**
     * 异常消息
     */
    private String exceptionMsg;

    /**
     * 异常发生时间
     */
    private String[] exceptionTime;

}
