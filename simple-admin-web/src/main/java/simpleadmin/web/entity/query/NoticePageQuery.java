package simpleadmin.web.entity.query;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import simpleadmin.web.common.page.PageParames;

/**
 * 通知查询实体
 *
 * @author wangcheng
 * @since 2022-06-29
 */
@Getter
@Setter
@ApiModel(value = "通知查询实体")
public class NoticePageQuery extends PageParames {

    /**
     * 标题，内容关键字
     */
    private String keyword;

    /**
     * 类型
     */
    private String type;

}
