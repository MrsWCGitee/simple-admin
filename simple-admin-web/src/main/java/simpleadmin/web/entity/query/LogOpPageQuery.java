package simpleadmin.web.entity.query;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import simpleadmin.web.common.page.PageParames;

/**
 * <p>
 * 操作日志 查询实体
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-03
 */
@Getter
@Setter
@ApiModel(value = "操作日志 查询实体")
public class LogOpPageQuery extends PageParames {

    /**
     * 名称
     */
    private String name;

    /**
     * 请求方法
     */
    private String reqMethod;

    /**
     * 是否成功
     */
    private String success;

    /**
     * 操作时间
     */
    private String[] opTime;

}
