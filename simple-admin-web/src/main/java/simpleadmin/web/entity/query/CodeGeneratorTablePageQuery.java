package simpleadmin.web.entity.query;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import simpleadmin.web.common.page.PageParames;

/**
 * 代码生成器数据表查询实体
 *
 * @author wangcheng
 * @since 2023-04-28
 */
@Getter
@Setter
@ApiModel(value = "代码生成器数据表查询实体")
public class CodeGeneratorTablePageQuery extends PageParames {

    /**
     * 表名
     */
    private String tableName;

    /**
     * 表类型（"BASE TABLE、VIEW"）
     */
    private String tableType;

    /**
     * 表所在数据库名
     */
    private String tableSchema;

}
