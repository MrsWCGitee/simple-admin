package simpleadmin.web.entity.query;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import simpleadmin.web.common.page.PageParames;

/**
 * 用户查询实体
 *
 * @author wangcheng
 * @since 2022-06-03
 */
@Getter
@Setter
@ApiModel(value = "用户查询实体")
public class UserPageQuery extends PageParames {

    /**
     * 组织机构Id
     */
    private Long[] orgId;

    /**
     * 账号名
     */
    private String account;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 姓名
     */
    private String name;

    /**
     * 出生日期
     */
    private String[] birthday;

    /**
     * 是否删除
     */
    private Boolean isDeleted;

}
