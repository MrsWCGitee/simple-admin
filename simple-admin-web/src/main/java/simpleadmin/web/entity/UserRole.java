package simpleadmin.web.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
@Getter
@Setter
@TableName("sys_user_role")
@ApiModel(value = "UserRole对象")
public class UserRole implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "Id", type = IdType.AUTO)
    private Long id;

    private Long sysUserId;

    private Long sysRoleId;


}
