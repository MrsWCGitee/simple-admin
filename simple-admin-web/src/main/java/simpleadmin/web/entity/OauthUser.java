package simpleadmin.web.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import simpleadmin.web.common.constant.DateTimeFormatConstant;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
@Getter
@Setter
@TableName("sys_oauth_user")
@ApiModel(value = "OauthUser对象")
public class OauthUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "Id", type = IdType.AUTO)
    private Long id;

    private String uuid;

    private String accessToken;

    private String nickName;

    private String avatar;

    private String gender;

    private String phone;

    private String email;

    private String location;

    private String blog;

    private String company;

    private String source;

    private String remark;

    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(shape = Shape.STRING, pattern = DateTimeFormatConstant.DATETIME)
    private LocalDateTime createdTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(shape = Shape.STRING, pattern = DateTimeFormatConstant.DATETIME)
    private LocalDateTime updatedTime;

    @TableField(fill = FieldFill.INSERT)
    private Long createdUserId;

    @TableField(fill = FieldFill.INSERT)
    private String createdUserName;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updatedUserId;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updatedUserName;

    private Integer isDeleted;

}
