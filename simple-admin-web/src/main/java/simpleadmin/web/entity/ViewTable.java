package simpleadmin.web.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import lombok.Getter;
import lombok.Setter;
import simpleadmin.web.common.constant.DateTimeFormatConstant;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 代码生成器用来查询表的视图
 *
 * @author wangcheng
 * @since 2023-04-28
 */
@Getter
@Setter
@TableName("sys_view_table")
public class ViewTable implements Serializable {

    private static final long serialVersionUID = 1L;

    private String tableName;

    private String tableType;

    private String tableSchema;

    private String tableCollation;

    private String tableComment;

    private String tableRows;

    @JsonFormat(shape = Shape.STRING, pattern = DateTimeFormatConstant.DATETIME)
    private LocalDateTime createTime;


}
