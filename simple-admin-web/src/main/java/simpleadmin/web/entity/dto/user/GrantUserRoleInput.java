package simpleadmin.web.entity.dto.user;

import lombok.Getter;
import lombok.Setter;

/**
 * 角色授权菜单
 */
@Getter
@Setter
public class GrantUserRoleInput {

    /**
     * 用户Id
     */
    private Long userId;

    /**
     * 用户角色Id的数组
     */
    private Long[] grantRoleIds;
}
