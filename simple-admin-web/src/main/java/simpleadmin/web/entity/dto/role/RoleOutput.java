package simpleadmin.web.entity.dto.role;

import lombok.Getter;
import lombok.Setter;

/**
 * 登录用户角色参数
 */
@Getter
@Setter
public class RoleOutput {

    /**
     * Id
     */
    private Long id;

    /**
     * 编码
     */
    private String code;

    /**
     * 名称
     */
    private String name;

}
