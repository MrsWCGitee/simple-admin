package simpleadmin.web.entity.dto.role;

import lombok.Getter;
import lombok.Setter;

/**
 * 角色授权菜单
 */
@Getter
@Setter
public class GrantRoleMenuInput {

    /**
     * 角色Id
     */
    private Long roleId;

    /**
     * 授权菜单Id的数组
     */
    private Long[] grantMenuIds;
}
