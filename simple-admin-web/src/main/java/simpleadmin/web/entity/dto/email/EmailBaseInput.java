package simpleadmin.web.entity.dto.email;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

/**
 * 邮件实体
 */
@Getter
@Setter
public class EmailBaseInput {
    /**
     * 邮件内容
     */
    @NotNull(message = "邮件内容不能为空！")
    public String content;
    /**
     * 收件人邮箱
     */
    @NotNull(message = "收件人邮箱不能为空！")
    @Email
    private String toEmail;
    /**
     * 邮件标题
     */
    @NotNull(message = "邮件标题不能为空！")
    private String subject;
}
