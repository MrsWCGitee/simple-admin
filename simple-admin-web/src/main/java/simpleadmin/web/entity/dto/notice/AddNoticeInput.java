package simpleadmin.web.entity.dto.notice;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 添加通知的实体
 */
@Data
public class AddNoticeInput {

    /**
     * 通知到的人
     */
    @NotNull(message = "通知到的人不能为空！")
    public Long[] noticeUserIdList;
    /**
     * Id
     */
    private Long id;
    /**
     * 标题
     */
    @NotNull(message = "标题不能为空！")
    private String title;
    /**
     * 内容
     */
    @NotNull(message = "内容不能为空！")
    private String content;
    /**
     * 类型
     */
    @NotNull(message = "类型不能为空！")
    private Integer type;
    /**
     * 状态
     */
    @NotNull(message = "状态不能为空！")
    private Integer status;

}
