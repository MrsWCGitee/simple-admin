package simpleadmin.web.entity.dto.menu;

import lombok.Getter;
import lombok.Setter;

/**
 * 登录菜单-Material 菜单类型
 */
@Getter
@Setter
public class MaterialTreeNode {

    /**
     * id
     */
    private Long id;

    /**
     * 父id
     */
    private Long pid;

    /**
     * 路由名称, 必须设置,且不能重名
     */
    private String name;

    /**
     * 组件
     */
    private String component;

    /**
     * 重定向地址, 访问这个路由时, 自定进行重定向
     */
    private String redirect;

    /**
     * 路由元信息（路由附带扩展信息）
     */
    private Meta meta;

    /**
     * 路径
     */
    private String path;

    /**
     * 控制路由和子路由是否显示在 sidebar
     */
    private Boolean hidden;

}
