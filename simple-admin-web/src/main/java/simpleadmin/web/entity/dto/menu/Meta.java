package simpleadmin.web.entity.dto.menu;

import lombok.Getter;
import lombok.Setter;

/**
 * 路由元信息内部类
 */
@Getter
@Setter
public class Meta {

    /**
     * 路由标题, 用于显示面包屑, 页面标题 *推荐设置
     */
    private String title;

    /**
     * 图标
     */
    private String icon;

    /**
     * 是否可见
     */
    private Boolean show;

    /**
     * 如需外部打开，增加：_blank
     */
    private String target;

    /**
     * 内链打开http链接
     */
    private String link;

}
