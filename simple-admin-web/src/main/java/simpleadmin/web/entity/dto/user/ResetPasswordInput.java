package simpleadmin.web.entity.dto.user;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;


/**
 * 重置用户密码实体
 */
@Getter
@Setter
public class ResetPasswordInput {

    /**
     * 用户Id
     */
    @NotNull(message = "用户Id不能为空")
    private Long userId;

    /**
     * 新的用户密码
     */
    @NotNull(message = "密码不能为空")
    @Pattern(regexp = "^[A-Z][A-Za-z0-9_]{5,19}$", message = "密码以大写英文字母开头，只包含英文字母、数字、下划线，长度在6到20之间")
    private String newPassword;

}
