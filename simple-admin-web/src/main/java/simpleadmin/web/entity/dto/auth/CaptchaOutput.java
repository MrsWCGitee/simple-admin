package simpleadmin.web.entity.dto.auth;

import lombok.Getter;
import lombok.Setter;

/**
 * 验证码实体
 */
@Getter
@Setter
public class CaptchaOutput {

    /**
     * 验证码存储的 Key
     */
    private String captchaCodeKey;
    /**
     * 验证码(Base64格式的图片)
     */
    private String captchaCodeImg;

    public CaptchaOutput() {
    }

    public CaptchaOutput(String captchaCodeKey, String captchaCodeImg) {
        this.captchaCodeKey = captchaCodeKey;
        this.captchaCodeImg = captchaCodeImg;
    }

}
