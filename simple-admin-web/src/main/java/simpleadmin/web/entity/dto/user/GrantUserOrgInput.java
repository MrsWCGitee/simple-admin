package simpleadmin.web.entity.dto.user;

import lombok.Getter;
import lombok.Setter;

/**
 * 用户数据
 */
@Getter
@Setter
public class GrantUserOrgInput {

    /**
     * 用户Id
     */
    private Long userId;

    /**
     * 用户数据
     */
    private Long[] grantOrgIds;
}
