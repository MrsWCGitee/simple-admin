package simpleadmin.web.entity.dto.email;

import lombok.Getter;
import lombok.Setter;

/**
 * 邮件实体
 */
@Getter
@Setter
public class EmailHtmlInput extends EmailBaseInput {

}
