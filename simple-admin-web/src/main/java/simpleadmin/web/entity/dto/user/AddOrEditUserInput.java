package simpleadmin.web.entity.dto.user;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import simpleadmin.web.common.constant.DateTimeFormatConstant;

import java.time.LocalDate;

/**
 * 用户添加修改实体
 */
@Getter
@Setter
public class AddOrEditUserInput {

    private Long id;

    private String account;

    private String password;

    private String nickName;

    private String name;

    private String avatar;

    @JsonFormat(pattern = DateTimeFormatConstant.DATE)
    private LocalDate birthday;

    private Integer sex;

    private String email;

    private String phone;

    private String tel;

    private Integer adminType;

    private Integer status;

    private Boolean isDeleted;

    private String orgName;

    private String orgCode;

    private Long orgId;

    private Long UserOrgId;

}
