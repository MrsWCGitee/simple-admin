package simpleadmin.web.entity.dto.machine;


import cn.hutool.core.date.BetweenFormatter;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.system.oshi.OshiUtil;
import oshi.software.os.OSProcess;
import simpleadmin.web.common.constant.DateTimeFormatConstant;

import java.lang.management.ManagementFactory;

/**
 * JVM相关信息
 *
 * @author huasheng
 */
public class Jvm {
    /**
     * 当前JVM占用的内存总数(M)
     */
    private double total;

    /**
     * JVM最大可用内存总数(M)
     */
    private double max;

    /**
     * JVM空闲内存(M)
     */
    private double free;

    /**
     * JDK版本
     */
    private String version;

    /**
     * JDK版本
     */
    private String jvmVersion;

    /**
     * JDK路径
     */
    private String home;

    public double getTotal() {
        return NumberUtil.div(total, (1024 * 1024), 2);
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getMax() {
        return NumberUtil.div(max, (1024 * 1024), 2);
    }

    public void setMax(double max) {
        this.max = max;
    }

    public double getFree() {
        return NumberUtil.div(free, (1024 * 1024), 2);
    }

    public void setFree(double free) {
        this.free = free;
    }

    public double getUsed() {
        return NumberUtil.div(total - free, (1024 * 1024), 2);
    }

    public double getUsage() {
        return NumberUtil.mul(NumberUtil.div(total - free, total, 4), 100);
    }

    /**
     * 获取JDK名称
     */
    public String getName() {
        return ManagementFactory.getRuntimeMXBean().getVmName();
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getJvmVersion() {
        return jvmVersion;
    }

    public void setJvmVersion(String jvmVersion) {
        this.jvmVersion = jvmVersion;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    /**
     * JDK启动时间
     */
    public String getStartTime() {
        OSProcess currentProcess = OshiUtil.getCurrentProcess();
        return DateUtil.format(DateUtil.date(currentProcess.getStartTime()), DateTimeFormatConstant.DATETIME);
    }

    /**
     * JDK运行时间
     */
    public String getRunTime() {
        DateTime nowTime = DateUtil.date();
        OSProcess currentProcess = OshiUtil.getCurrentProcess();
        DateTime startTime = DateUtil.date(currentProcess.getStartTime());
        return DateUtil.formatBetween(startTime, nowTime, BetweenFormatter.Level.SECOND);
    }
}



