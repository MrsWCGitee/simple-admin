package simpleadmin.web.entity.dto.machine;

import cn.hutool.core.util.NumberUtil;

/**
 * CPU相关信息
 *
 * @author huasheng
 */
public class Cpu {
    /**
     * CPU总的使用率
     */
    private double total;

    /**
     * CPU系统使用率
     */
    private double sys;

    /**
     * CPU用户使用率
     */
    private double used;

    /**
     * CPU当前等待率
     */
    private double wait;

    /**
     * CPU当前空闲率
     */
    private double free;

    /**
     * CPU名称
     */
    private String cpuName;

    /**
     * CPU数量
     */
    private int cpuNum;

    /**
     * CPU物理核心数
     */
    private int physicalCpuNum;

    /**
     * CPU逻辑核心数
     */
    private int logicalCpuNum;

    public String getCpuName() {
        return cpuName;
    }

    public void setCpuName(String cpuName) {
        this.cpuName = cpuName;
    }

    public int getCpuNum() {
        return cpuNum;
    }

    public void setCpuNum(int cpuNum) {
        this.cpuNum = cpuNum;
    }

    public int getPhysicalCpuNum() {
        return physicalCpuNum;
    }

    public void setPhysicalCpuNum(int physicalCpuNum) {
        this.physicalCpuNum = physicalCpuNum;
    }

    public int getLogicalCpuNum() {
        return logicalCpuNum;
    }

    public void setLogicalCpuNum(int logicalCpuNum) {
        this.logicalCpuNum = logicalCpuNum;
    }

    public double getTotal() {
        return NumberUtil.round(NumberUtil.mul((sys + used) / total, 100), 2).doubleValue();
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getSys() {
        return NumberUtil.round(NumberUtil.mul(sys / total, 100), 2).doubleValue();
    }

    public void setSys(double sys) {
        this.sys = sys;
    }

    public double getUsed() {
        return NumberUtil.round(NumberUtil.mul(used / total, 100), 2).doubleValue();
    }

    public void setUsed(double used) {
        this.used = used;
    }

    public double getWait() {
        return NumberUtil.round(NumberUtil.mul(wait / total, 100), 2).doubleValue();
    }

    public void setWait(double wait) {
        this.wait = wait;
    }

    public double getFree() {
        return NumberUtil.round(NumberUtil.mul(free / total, 100), 2).doubleValue();
    }

    public void setFree(double free) {
        this.free = free;
    }
}



