package simpleadmin.web.entity.dto.auth;

import lombok.Getter;
import lombok.Setter;

/**
 * 刷新 token 输出实体
 */
@Getter
@Setter
public class RefreshTokenOutput {

    /**
     * token
     */
    private String token;

    /**
     * 刷新 token
     */
    private String refreshToken;

    public RefreshTokenOutput(String token, String refreshToken) {
        this.token = token;
        this.refreshToken = refreshToken;
    }
}
