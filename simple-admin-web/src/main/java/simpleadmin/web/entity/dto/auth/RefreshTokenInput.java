package simpleadmin.web.entity.dto.auth;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * 刷新 token 输入实体
 */
@Getter
@Setter
public class RefreshTokenInput {

    /**
     * token
     */
    @NotNull(message = "token 不能为空")
    private String token;

    /**
     * 刷新 token
     */
    @NotNull(message = "refreshToken 不能为空")
    private String refreshToken;
}
