package simpleadmin.web.entity.dto.auth;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import simpleadmin.web.common.constant.DateTimeFormatConstant;
import simpleadmin.web.entity.dto.menu.MaterialTreeNode;
import simpleadmin.web.entity.dto.role.RoleOutput;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class LoginOutput {

    /**
     * Id
     */
    private Long id;

    /**
     * 账号
     */
    private String account;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 姓名
     */
    private String name;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 生日
     */
    @JsonFormat(pattern = DateTimeFormatConstant.DATE)
    private LocalDate birthday;

    /**
     * 性别(字典 1男 2女)
     */
    private Integer sex;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 手机
     */
    private String phone;

    /**
     * 电话
     */
    private String tel;

    /**
     * 管理员类型（0超级管理员 1非管理员）
     */
    private String adminType;

    /**
     * 最后登陆IP
     */
    private String lastLoginIp;

    /**
     * 最后登陆时间
     */
    @JsonFormat(pattern = DateTimeFormatConstant.DATETIME)
    private LocalDateTime lastLoginTime;

    /**
     * 最后登陆地址
     */
    private String lastLoginAddress;

    /**
     * 最后登陆所用浏览器
     */
    private String lastLoginBrowser;

    /**
     * 最后登陆所用系统
     */
    private String lastLoginOs;

    /**
     * 角色信息
     */
    private List<RoleOutput> roles = new ArrayList<RoleOutput>();

    /**
     * 菜单信息
     */
    private List<MaterialTreeNode> menus = new ArrayList<MaterialTreeNode>();

}
