package simpleadmin.web.entity.map;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DictDataVO {
    private String code;
    private String value;
}
