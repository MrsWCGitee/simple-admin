package simpleadmin.web.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import simpleadmin.web.common.constant.DateTimeFormatConstant;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 已收通知公告实体
 * </p>
 *
 * @author wangcheng
 * @since 2022-08-15
 */
@Getter
@Setter
@TableName("sys_view_notice_receive")
@ApiModel(value = "已收通知公告实体", description = "已收通知公告实体")
public class ViewNoticeReceive implements Serializable {

    private static final long serialVersionUID = 1L;

    private String title;

    private String content;

    private Integer type;

    private Long publicUserId;

    private String publicUserName;

    private Long publicOrgId;

    private String publicOrgName;

    @JsonFormat(shape = Shape.STRING, pattern = DateTimeFormatConstant.DATETIME)
    private LocalDateTime publicTime;

    @JsonFormat(shape = Shape.STRING, pattern = DateTimeFormatConstant.DATETIME)
    private LocalDateTime cancelTime;

    private Integer status;

    private Long id;

    private Long noticeId;

    private Long userId;

    @JsonFormat(shape = Shape.STRING, pattern = DateTimeFormatConstant.DATETIME)
    private LocalDateTime readTime;

    private Integer readStatus;

}
