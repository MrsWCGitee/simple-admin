package simpleadmin.web.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import simpleadmin.web.common.constant.DateTimeFormatConstant;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
@Getter
@Setter
@TableName("sys_log_audit")
@ApiModel(value = "LogAudit对象")
public class LogAudit implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "Id", type = IdType.AUTO)
    private Integer id;

    private String tableName;

    private String columnName;

    private String newValue;

    private String oldValue;

    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = DateTimeFormatConstant.DATETIME)
    private LocalDateTime createdTime;

    private Long userId;

    private String userName;

    private Integer operate;

}
