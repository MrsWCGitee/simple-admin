package simpleadmin.web.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import simpleadmin.web.common.constant.DateTimeFormatConstant;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * users表所对应的实体类
 *
 * @Author:wangcheng
 * @Date: 2022/03/06
 * @Time 19:25
 */
@Getter
@Setter
@TableName("sys_user")
@ApiModel(value = "Role对象")
public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    // value与数据库主键列名一致，若实体类属性名与表主键列名一致可省略value
    @TableId(value = "Id", type = IdType.AUTO) // 设置主键，并指定自增策略
    private Long id;

    private String account;

    private String password;

    private String nickName;

    private String name;

    private String avatar;

    @JsonFormat(shape = Shape.STRING, pattern = DateTimeFormatConstant.DATE)
    private LocalDate birthday;

    private Integer sex;

    private String email;

    private String phone;

    private String tel;

    private String lastLoginIp;

    @JsonFormat(shape = Shape.STRING, pattern = DateTimeFormatConstant.DATETIME)
    private LocalDateTime lastLoginTime;

    private Integer adminType;

    private Integer status;

    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(shape = Shape.STRING, pattern = DateTimeFormatConstant.DATETIME)
    private LocalDateTime createdTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(shape = Shape.STRING, pattern = DateTimeFormatConstant.DATETIME)
    private LocalDateTime updatedTime;

    @TableField(fill = FieldFill.INSERT)
    private Long createdUserId;

    @TableField(fill = FieldFill.INSERT)
    private String createdUserName;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updatedUserId;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updatedUserName;

    private Boolean isDeleted;

}
