package simpleadmin.web.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import simpleadmin.web.common.constant.DateTimeFormatConstant;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 异常日志 实体
 * </p>
 *
 * @author wangcheng
 * @since 2022-06-08
 */
@Getter
@Setter
@TableName("sys_log_ex")
@ApiModel(value = "LogEx对象")
public class LogEx implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "Id", type = IdType.AUTO)
    private Integer id;

    private String account;

    private String name;

    private String className;

    private String methodName;

    private String exceptionName;

    private String exceptionMsg;

    private String exceptionSource;

    private String stackTrace;

    private String paramsObj;

    @JsonFormat(pattern = DateTimeFormatConstant.DATETIME)
    private LocalDateTime exceptionTime;

}
