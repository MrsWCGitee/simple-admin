package simpleadmin.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import simpleadmin.web.common.webScoket.WebSocketServer;

/**
 * @author wangcheng
 * @since 2022-06-03
 */
@SpringBootApplication
public class SimpleApplication {

    /**
     * 主函数入口
     *
     * @param args
     */
    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(SimpleApplication.class, args);
        WebSocketServer.setApplicationContext(context);
    }

}
